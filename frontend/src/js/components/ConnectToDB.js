import React from "react";
import InputField from './InputField';
import * as shortid from 'shortid';
import Dropdown from 'react-dropdown';
import 'react-dropdown/style.css';
import Help from "./Help";
import ReactDOM from "react-dom";
import AboutMe from "./AboutMe";
import * as Utils from "../utils/Utils";
import '../../css/ConnectToDB.css';

const ConnectToDB = props => {
    return (
        <div className="connect">
            {fields.map(field => <InputField name={field.name} type={field.type} pattern={field.pattern}
                                             key={shortid.generate()}
                                             ourCallback={onGetDataFromChild}
            />)}
            <div className="connect-footer">
                <Dropdown options={databaseType.valueOptions} onChange={dropDownValue}
                          placeholder="Select a DB"/>
                <div id="connectButton" className="connect-button button" onClick={() => onClickBtn(props)}> Connect</div>
            </div>

            <div className="field">
                <div className="button" onClick={() => showInfo("help")}>Help</div>
                <div className="button" onClick={() => showInfo("aboutMe")}>About me</div>
            </div>
        </div>
    );
};
const showInfo = (typeInfo) => {
    switch (typeInfo) {
        case "help":
            return ReactDOM.render(<Help/>, document.getElementById('root'));
        case "aboutMe":
            return ReactDOM.render(<AboutMe/>, document.getElementById('root'));
        default:
            alert("Selection error!");
    }
};

const fields = [
    {name: 'Hostname:', type: 'text'},
    {name: 'Port:', pattern:"[0-9]{1,6}", type: 'text'},
    {name: 'Username:', type: 'text'},
    {name: 'Password:', type: 'password'},
    {name: 'Database Name:', type: 'text'},
];

const databaseType = {
    valueOptions: ['MySQL', 'PostgreSQL']
};

const validate = () => {
    for (const field of fields) {
        if (!field.value && field.name !== 'Password:') {
            alert(`field ${field.name.replace(':', '')} not set!`);
            return false;
        }

        if (field.value && field.name === 'Port:') {
            if (!(/^[0-9]{1,6}$/.test(field.value))) {
                alert("Port is incorrect!");
                return false;
            }
        }
    }

    if (!databaseType.value) {
        alert(`DB type not set!`);
        return false;
    }
    return true;
};

const onClickBtn = async props => {
    try {
        if (!validate()) return;
        let mapping = "connect";
        let body = JSON.stringify({
            hostname: fields.find(field => field.name === 'Hostname:').value + ':',
            port: fields.find(field => field.name === 'Port:').value,
            username: fields.find(field => field.name === 'Username:').value,
            password: fields.find(field => field.name === 'Password:').value,
            dbName: fields.find(field => field.name === 'Database Name:').value,
            dbType: databaseType.value,
        });

        const tree = await Utils.fetchPostBody(mapping, body);

        if (tree.status === 201) {
            localStorage.setItem('tree', await tree.text());
            onClearDataFromChild();
            props.update('ses');
        } else {
            alert('Login data is incorrect!');
        }
    } catch (err) {
        alert('Server unavailable!');
    }
};

const onClearDataFromChild = () => {
    fields.forEach(field => {
        field.value = "";
    });
}

const onGetDataFromChild = (value, name) => {
    fields.forEach(field => {
        if (field.name === name) {
            field.value = value;
        }
    });
    return value;
};

const dropDownValue = (e) => {
    databaseType.value = e.value;
};

export default ConnectToDB;
