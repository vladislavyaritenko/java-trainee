import React, {Component} from "react";
import FileSaver from "file-saver";
import '../../css/ShowDDL.css';

class ShowDDL extends Component {

    saveDDL = () => {
        let ddl = localStorage.getItem("DDL") === null ? "" : localStorage.getItem("DDL");
        let currentTagURL = localStorage.getItem("currentTagURL") === null ? "empty" : localStorage.getItem("currentTagURL");
        let fileName = currentTagURL + '_DDL.txt';
        let blob = new Blob([ddl], {type: "text/plain;charset=utf-8"});
        FileSaver.saveAs(blob, fileName);
    }

    showDDL = (props) => {
        let ddl = props.ddl;
        if ((ddl === "") && (localStorage.getItem("DDL") !== null)) {
            ddl = localStorage.getItem("DDL");
        }
        return ddl;

    }

    render() {
        return (
            <React.Fragment>
                <textarea placeholder="no ddl" value={this.showDDL(this.props)} readOnly/>
                <div className="button" onClick={() => this.saveDDL()}>Save DDL</div>
                <hr/>
            </React.Fragment>
        );
    }
}

export default ShowDDL;
