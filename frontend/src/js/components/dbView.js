import React, {Component} from "react";
import * as CheckboxTree from 'react-checkbox-tree';
import Attributes from './Attributes';
import ShowDDL from "./ShowDDL";
import Search from "./Search";
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'
import {fas} from '@fortawesome/free-solid-svg-icons'
import {library} from '@fortawesome/fontawesome-svg-core'
import 'react-checkbox-tree/lib/react-checkbox-tree.css';
import '../../css/treeStyle.css'
import * as Utils from "../utils/Utils";
import '../../css/dbView.css';

library.add(fas);

class DBView extends Component {
    constructor(props) {
        super(props);
        this.state = {
            tree: [],
            expanded: [],
            currentTagURL: "",
            urlTagForLoad: "",
            ddl: "",
            loading: false,
            nameAttr: "",
            valueAttr: "",
        }
    }

    componentDidMount() {
        if (localStorage.getItem("tree")) {
            this.setState({tree: [JSON.parse(localStorage.getItem('tree'))]});
        }
        if (localStorage.getItem("expanded")) {
            this.setState({expanded: JSON.parse(localStorage.getItem('expanded'))});
        }
        if (localStorage.getItem("currentTagURL")) {
            this.setState({currentTagURL: localStorage.getItem('currentTagURL')});
        }
    }

    componentWillUpdate(nextProps, nextState, nextContext) {
        localStorage.setItem("expanded", JSON.stringify(nextState.expanded));
    }

    detailLoad = () => {
        this.onClickTag("detail");
    };
    lazyLoad = () => {
        this.onClickTag("lazy");
    };
    fullLoad = () => {
        this.onClickTag("full");
    };

    disconnect = (props) => {
        let mapping = "disconnect";
        Utils.fetchPost(mapping).then(props.update);
    };

    onClickTag = async (typeLoad) => {
        const isConnection = await Utils.fetchPost("isConnection").then(response => response.json()).catch(() => alert('Server unavailable!'));

        if (isConnection) {
            if (this.state.urlTagForLoad !== "") {
                this.setLoading();

                let mappingLoad = typeLoad + "/" + this.state.urlTagForLoad;
                const tag = await Utils.fetchGet(mappingLoad);

                this.setLoading();

                if (tag.status === 200) {
                    let tagJson = await tag.json();
                    this.setState({tree: [tagJson]});
                    localStorage.setItem("tree", JSON.stringify(tagJson));

                    await this.print(this.state.urlTagForLoad);

                    this.setState({urlTagForLoad: ""});
                }
                else if (tag.status === 500) {
                    let arrChecked = this.state.urlTagForLoad.split('.');
                    alert(`Tag "${arrChecked[arrChecked.length - 1]}" is not found`);
                }
            }
            else {
                alert("You need chose tag for loading!");
            }
        } else {
            localStorage.clear();
            this.props.update();
        }
    };

    print = async (tagURL) => {
        const isConnection = await Utils.fetchPost("isConnection").then(response => response.json()).catch(() => alert('Server unavailable!'));

        if (isConnection) {
            if (tagURL !== undefined) {
                this.setState({currentTagURL: tagURL});
                localStorage.setItem('currentTagURL', tagURL);

                this.setState({urlTagForLoad: tagURL});

                let mapping = "print/" + tagURL;

                const DDL = await Utils.fetchGet(mapping);

                if (DDL.status === 200) {
                    let ddlText = await DDL.text();
                    this.setState({ddl: ddlText});
                    localStorage.setItem("DDL", ddlText);
                } else if (DDL.status === 500) {
                    this.setState({ddl: "print is impossible!"})
                }
            }
        } else {
            localStorage.clear();
            this.props.update();
        }
    };


    searchTag = async (key, value) => {
        const isConnection = await Utils.fetchPost("isConnection").then(response => response.json()).catch(() => alert('Server unavailable!'));

        if (isConnection) {
            if ((!this.isEmpty(key)) && (!this.isEmpty(value))) {
                let paramsForSearch = {key: key.trim(), value: value.trim()};
                let mapping = "search";
                let body = JSON.stringify(paramsForSearch);

                this.setLoading();
                const searchResult = await Utils.fetchPostBody(mapping, body);
                this.setLoading();

                if (searchResult.status === 200) {
                    let searchTag = await searchResult.json();
                    this.setState({tree: [searchTag]});
                } else {
                    alert(`Tag with the such name - "${key}" and value - "${value}" is not found!`);
                }
            }
        } else {
            localStorage.clear();
            this.props.update();
        }
    };

    resetSearch = () => {
        if (localStorage.getItem("tree")) {
            this.setState({tree: [JSON.parse(localStorage.getItem('tree'))]});
        }
    }

    isEmpty = (str) => {
        return (str.length === 0 || !str.trim());
    };

    //for showing fa-spinner
    setLoading = () => {
        this.setState(prevState => ({
            loading: !prevState.loading
        }));
    };

    //when do double click by table with attributes
    onDoubleClickNameAttr = (key) => {
        this.setState({nameAttr: key});
    };
    onDoubleClickValueAttr = (value) => {
        this.setState({valueAttr: value});
    };

    //when do click on the search input field, for manual input
    resetNameAttr = () => {
        this.setState({nameAttr: ""});
    };

    resetValueAttr = () => {
        this.setState({valueAttr: ""});
    };

    render() {
        const {loading} = this.state;

        return (
            <React.Fragment>
                {loading && <div className="preloader">
                    <FontAwesomeIcon className="fa fa-spinner fa-spin fa-4x fa-fw" icon="spinner"/>
                </div>}

                <div className="button-group">
                    <div className="loads-button">
                        <div className="button" onClick={() => this.detailLoad()}> detailLoad</div>
                        <div className="button" onClick={() => this.lazyLoad()}> lazyLoad</div>
                        <div className="button" onClick={() => this.fullLoad()}> fullLoad</div>
                    </div>
                    <div className="button" onClick={() => this.disconnect(this.props)}> Disconnect</div>
                </div>

                <div className="db-view">
                    <div className="db-container">
                        <CheckboxTree
                            nodes={this.state.tree}
                            onClick={tagURL => this.print(tagURL.value)}
                            expanded={this.state.expanded}
                            onExpand={expanded => {
                                this.setState({expanded});
                            }}
                            showNodeIcon={false}
                            icons={{
                                expandClose: <FontAwesomeIcon className="rct-icon rct-icon-expand-close"
                                                              icon="chevron-right"/>,
                                expandOpen: <FontAwesomeIcon className="rct-icon rct-icon-expand-open"
                                                             icon="chevron-down"/>,
                            }}
                        />
                    </div>
                    <div className="db-container">
                        <Attributes currentTagURL={this.state.currentTagURL} tree={this.state.tree}
                                    keyCallBack={this.onDoubleClickNameAttr}
                                    valueCallBack={this.onDoubleClickValueAttr}/>
                    </div>
                    <div className="db-container">
                        <ShowDDL ddl={this.state.ddl}/>
                        <Search name={this.state.nameAttr} resetName={this.resetNameAttr} value={this.state.valueAttr}
                                resetValue={this.resetValueAttr} searchCallBack={this.searchTag}
                                resetCallBack={this.resetSearch}/>
                    </div>
                </div>
            </React.Fragment>
        );
    }
}

export default DBView;
