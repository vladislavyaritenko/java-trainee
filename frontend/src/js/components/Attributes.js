import React, {Component} from "react";
import * as shortid from 'shortid';
import '../../css/Attributes.css';

class Attributes extends Component {
    constructor(props) {
        super(props);
        this.state = {
            currentTag: this.props.currentTagURL,
            tree: this.props.tree,
        }
    }

    componentDidMount() {
        this.getAttributes(this.state.currentTag.split('.'), this.state.tree);
    }

    getAttributes = (urlTag, tree) => {
        if ((tree.length === 0) || (urlTag.length === 0)) {
            return {}
        }

        var nameForEquals = urlTag[0];
        for (var tag in tree) {
            if (tree[tag].label !== undefined) {
                if (tree[tag].label.toLowerCase() === nameForEquals && urlTag.length === 1) {
                    return tree[tag].attributes;
                }
                if (tree[tag].label.toLowerCase() === nameForEquals) {
                    return this.getAttributes(urlTag.slice(1), tree[tag].children);
                }
            }
        }
        return {};
    }


    createTableAttributes(props) {
        let row = [];
        let column = [];

        column.push(<th key={shortid.generate()}>Name attribute</th>);
        column.push(<th key={shortid.generate()}>Value attribute</th>);

        row.push(<thead key={shortid.generate()}><tr>{column}</tr></thead>);

        if (this.props.currentTagURL !== undefined) {
            var attributes = this.getAttributes(this.props.currentTagURL.split('.'), this.props.tree);

            if (attributes !== undefined) {
                Object.keys(attributes).forEach(value => {
                    let children = [];

                    children.push(
                        <td onDoubleClick={()=>props.keyCallBack(value.toLowerCase())} key={shortid.generate()}>{value.toLowerCase()}</td>
                    );

                    let attributeValue = attributes[value] === "" ? "null" : attributes[value];
                    children.push(
                        <td onDoubleClick={()=>props.valueCallBack(attributeValue)} key={shortid.generate()}>{attributeValue}</td>
                    );

                    row.push(<tbody key={shortid.generate()}><tr>{children}</tr></tbody>);
                });
            }
        }
        return row;
    }

    render() {
        return (
            <table>
                {this.createTableAttributes(this.props)}
            </table>
        );
    }
}

export default Attributes;
