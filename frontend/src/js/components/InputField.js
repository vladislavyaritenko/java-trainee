import React from "react";

const InputField = props => {
    return (
        <div className="field">
            <span>{props.name}</span>
            <input type={props.type} name={props.name} pattern={props.pattern}
                   onChange={(e) => {
                       props.ourCallback(e.target.value, props.name);
                   }}/>


        </div>

    );
};

export default InputField;
