import React from "react";
import ReactDOM from "react-dom";
import App from "../../App.js";
import img from '../../pic/IMG_8101.jpg'
import '../../css/AboutMe.css';

const AboutMe = () => {
    return (
        <div className="about-me">
            <img className="photo-me" src={img} alt="me"/>
            <p className="contact">Yaritenko Vladislav</p>
            <p className="contact">+380996681048</p>
            <p className="contact">vladislavyaritenko@gmail.com</p>
            <div className="button" onClick={() => goBack()}>back</div>
        </div>
    );
};

const goBack = () => {
    return ReactDOM.render(<App/>, document.getElementById('root'));
}

export default AboutMe;