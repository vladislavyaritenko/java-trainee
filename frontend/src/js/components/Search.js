import React, {Component} from "react";

class Search extends Component {
    constructor(props) {
        super(props);
        this.key = "";
        this.value = "";
    }

    setKey = (key) => {
        this.key = key;
    };

    setValue = (value) => {
        this.value = value;
    };

    inputFieldNameWithValue = (props) => {
        this.key =  props.name;
        return <input id="key" className="search" type="text" placeholder="name"
                      value={this.key} onFocus={() => {
            props.resetName();
            document.getElementById("key").value = "";
        }}/>;
    };

    inputFieldNameWithoutValue = () => {
        return <input className="search" type="text" placeholder="name"
                      onChange={(e) => this.setKey(e.target.value)}
        />;
    };

    inputFieldValueWithValue = (props) => {
        this.value = props.value;
        return <input id="value" className="search" type="text" placeholder="value"
                      value={this.value} onFocus={() => {
            props.resetValue();
            document.getElementById("value").value = "";
        }}/>;
    };

    inputFieldValueWithoutValue = () => {
        return <input className="search" type="text" placeholder="value"
                      onChange={(e) => this.setValue(e.target.value)}/>;
    };

    render() {
        return (
            <React.Fragment>
                <div className="field">
                    {this.props.name !== "" ? this.inputFieldNameWithValue(this.props) : this.inputFieldNameWithoutValue()}

                    {this.props.value !== "" ? this.inputFieldValueWithValue(this.props) : this.inputFieldValueWithoutValue()}
                </div>


                <div className="field">
                    <div className="button" onClick={() => {
                        this.props.searchCallBack(this.key, this.value);
                    }}>
                        Search
                    </div>
                    <div className="button" onClick={() => this.props.resetCallBack()}>Reset</div>
                </div>
            </React.Fragment>
        );
    }
}

export default Search;

