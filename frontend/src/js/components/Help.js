import React from "react";
import ReactDOM from "react-dom";
import App from "../../App.js";
import '../../css/Help.css';

const Help = () => {
    return (
        <div className="help">
            <p>For log in to enter: <b>hostname</b>, <b>port</b>, <b>username</b>, <b>password</b>, <b>database
                name</b> and select the <b>type of database</b>.
            </p>
            <br/>
            <p>
                To load, select the required tag in the checkbox field and click on one of the
                buttons: <b>detailLoad</b>, <b>lazyLoad</b>, <b>fullLoad</b>.
            </p>
            <br/>
            <p>
                To load the DDL and display the attributes of this tag, you must click on the tag itself.
            </p>
            <br/>
            <p>
                To download a text file containing the DDL tag, you must click on the <b>Save DDL</b> button.
            </p>
            <br/>
            <p>
                To search for an attribute you need in the field <b>name</b> enter attribute name, and in the
                field <b>value</b> - its value and push the button <b>Search</b>.
                As a result of the search, the found attributes are displayed as a subtree, only with those nodes
                in which there is a given attribute.
                To show the full tree, click on the <b>Reset</b> button.
            </p>
            <br/>
            <div className="button" onClick={() => goBack()}>back</div>
        </div>
    );
};

const goBack = () => {
    return ReactDOM.render(<App/>, document.getElementById('root'));
}

export default Help;