import React from 'react'
import ReactDOM from 'react-dom'
import "./css/index.css";
import * as serviceWorker from './serviceWorker';

import App from './App'

const routing = (
    <App/>
);

ReactDOM.render(routing, document.getElementById('root'));

serviceWorker.unregister();

