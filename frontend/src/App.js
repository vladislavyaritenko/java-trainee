import React, {Component} from 'react';
import "./css/App.css";
import ConnectToDB from "./js/components/ConnectToDB";
import DBView from "./js/components/dbView";


class App extends Component {

    constructor(props) {
        super(props);
        this.state = {
            connected: !!localStorage.getItem('sessionKey'),
            help: null,
        };
    }

    render() {
        return (
            <React.Fragment>
                {
                    this.state.connected ? <DBView update={this.connect}/> : <ConnectToDB update={this.connect}/>
                }
            </React.Fragment>
        );
    }

    connect = (sessionKey) => {
        localStorage.setItem('sessionKey', this.state.connected ? '' : sessionKey);
        this.setState(prevState => ({
            connected: !prevState.connected
        }));

        if (!this.state.connected) {
            localStorage.clear();
        }
    };
}

export default App;
