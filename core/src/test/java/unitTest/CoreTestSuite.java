package unitTest;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import unitTest.service.ServiceTest;

@RunWith(Suite.class)
@Suite.SuiteClasses( {ServiceTest.class, StructureTest.class})
public class CoreTestSuite {
}
