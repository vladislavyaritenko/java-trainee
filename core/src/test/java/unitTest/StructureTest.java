package unitTest;

import core.exceptions.ChildrenListException;
import core.exceptions.ParserException;
import core.service.parsing.ParsingClient;
import core.service.parsing.impl.ParseFromXML;
import core.structure.ChildrenList;
import core.structure.Tag;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.StringJoiner;
import java.util.function.Predicate;

public class StructureTest {
    private static final String PATH_TO_INPUT_VALID_FILE = "src/test/resources/testInputValidFile.xml";
    private static final String EOL = System.lineSeparator();

    @Test(expected = ChildrenListException.class)
    public void shouldAddChildTagThenThrowChildrenListException() throws ChildrenListException {
        ChildrenList childrenList = new ChildrenList(null);
        childrenList.add(null);
    }

    @Test
    public void shouldExecuteDepthSearch() throws ParserException {
        ParsingClient parsingClient = new ParsingClient();
        ParseFromXML ParseFromXML = new ParseFromXML();

        parsingClient.setStrategy(ParseFromXML);
        Tag tag = parsingClient.executeStrategy(PATH_TO_INPUT_VALID_FILE);

        List<String> searchParam = new ArrayList<>();
        searchParam.add("size");

        Predicate<Tag> tagPredicate = tag1 -> tag1.getName().equals(searchParam.get(0));

        List<Tag> actualSearch = new ArrayList<>();
        actualSearch = tag.depthSearch(tagPredicate, tag, actualSearch);

        String expectedExecuteSearch = "[<size>15</size>]";
        String actualExecuteSearch = actualSearch.toString();

        Assert.assertEquals(expectedExecuteSearch, actualExecuteSearch);
    }

    @Test
    public void shouldExecuteBreadthSearch() throws ParserException {
        ParsingClient parsingClient = new ParsingClient();
        ParseFromXML ParseFromXML = new ParseFromXML();

        parsingClient.setStrategy(ParseFromXML);
        Tag tag = parsingClient.executeStrategy(PATH_TO_INPUT_VALID_FILE);

        List<String> searchParam = new ArrayList<>();
        searchParam.add("type");
        searchParam.add("newspaper");

        Predicate<Tag> tagPredicate = tag1 -> {
            if (!tag1.getAttributes().isEmpty()) {
                return tag1.getAttributes().get(searchParam.get(0)).equals(searchParam.get(1));
            }
            return false;
        };

        List<Tag> actualSearch = new ArrayList<>();
        actualSearch = tag.breadthSearch(tagPredicate, tag, actualSearch);

        StringJoiner joiner = new StringJoiner(EOL);
        joiner.add("[<paper type=\"newspaper\">").add("<titel>Simon > 5</titel>")
            .add("<monthly>true</monthly>").add("<color>false</color>").add("<size>15</size></paper>]");

        String expectedExecuteSearch = joiner.toString();
        String actualExecuteSearch = actualSearch.toString();

        Assert.assertEquals(expectedExecuteSearch, actualExecuteSearch);
    }
}
