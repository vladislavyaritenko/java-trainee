package unitTest.db;

import core.db.connection.ConnectionManager;
import core.db.constants.DBType;
import core.exceptions.DBException;
import org.junit.Assert;
import org.junit.Test;

import java.sql.Connection;

import static core.db.constants.DBType.MySQL;
import static core.db.constants.DBType.PostgreSQL;

public class ConnectionManagerTest {
    private static final String URL = "localhost:3306/sakila";
    private static final String USER = "root";
    private static final String PASSWORD = "pass";
    private DBType dbType = MySQL;
    private DBType dbTypeInvalid = PostgreSQL;

    @Test
    public void shouldSelectDatabaseThenGetConnectionAndConnectionNotNull() throws DBException {
        ConnectionManager connectionManager = new ConnectionManager();
        Connection connection = connectionManager.initConnection(dbType, URL, USER, PASSWORD);
        Assert.assertNotNull(connection);
    }

    @Test(expected = DBException.class)
    public void shouldSelectDatabaseThenGetConnectionThenThrowDBException() throws DBException {
        ConnectionManager connectionManager = new ConnectionManager();
        connectionManager.initConnection(dbTypeInvalid, URL, USER, PASSWORD);
    }
}
