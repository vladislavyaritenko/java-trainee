package unitTest.db.loaders;

import core.db.connection.impl.MySQLConnection;
import core.db.constants.CommonConst;
import core.db.constants.DBType;
import core.db.loaders.LoaderManager;
import core.exceptions.DBException;
import core.exceptions.ReflectionException;
import core.structure.Tag;
import java.sql.Connection;
import java.sql.SQLException;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import static core.db.constants.DBType.MySQL;

public class LoadTest {
    private static final String EOL = System.lineSeparator();
    private static final String URL = "localhost:3306/sakila";
    private static final String USER = "root";
    private static final String PASSWORD = "pass";
    private static final String TAG_NAME = "TABLE";
    private static final String TAG_NAME_ATTRIBUTE = "actor";
    private static final DBType DB_TYPE = MySQL;

    private Connection connection;
    private Tag tag;

    @Before
    public void setUp() throws DBException {
        MySQLConnection mySQLConnection = new MySQLConnection(URL, USER, PASSWORD);
        connection = mySQLConnection.getConnection();

        tag = new Tag(TAG_NAME);
        tag.setAttribute(CommonConst.NAME, TAG_NAME_ATTRIBUTE);
    }

    @After
    public void closeConnection() throws SQLException {
        if(connection != null){
            connection.close();
        }
    }

    @Test
    public void shouldGetTagByDetailLoad() throws ReflectionException, DBException {
        String expectedTag = "<TABLE TABLE_CATALOG=\"def\" TABLE_COMMENT=\"\" CHECKSUM=\"\" CHECK_TIME=\"\" ENGINE=\"InnoDB\" DB_NAME=\"sakila\" TABLE_TYPE=\"BASE TABLE\" TABLE_ROWS=\"200\" AVG_ROW_LENGTH=\"81\" UPDATE_TIME=\"\" DATA_LENGTH=\"16384\" DATA_FREE=\"0\" NAME=\"actor\" INDEX_LENGTH=\"16384\" ROW_FORMAT=\"Dynamic\" AUTO_INCREMENT=\"201\" VERSION=\"10\" CREATE_OPTIONS=\"\" CREATE_TIME=\"2019-04-14 23:09:22.0\" MAX_DATA_LENGTH=\"0\" TABLE_COLLATION=\"utf8_general_ci\">" + EOL +
            "</TABLE>";
        LoaderManager loaderManager = new LoaderManager(connection, DB_TYPE);
        String actualTag = loaderManager.detailLoad(tag).toString();

        Assert.assertEquals(expectedTag, actualTag);
    }

    @Test
    public void shouldGetTagByLazyLoad() throws ReflectionException, DBException {
        String expectedTag = "<TABLE TABLE_CATALOG=\"def\" TABLE_COMMENT=\"\" CHECKSUM=\"\" CHECK_TIME=\"\" ENGINE=\"InnoDB\" DB_NAME=\"sakila\" TABLE_TYPE=\"BASE TABLE\" TABLE_ROWS=\"200\" AVG_ROW_LENGTH=\"81\" UPDATE_TIME=\"\" DATA_LENGTH=\"16384\" DATA_FREE=\"0\" NAME=\"actor\" INDEX_LENGTH=\"16384\" ROW_FORMAT=\"Dynamic\" AUTO_INCREMENT=\"201\" VERSION=\"10\" CREATE_OPTIONS=\"\" CREATE_TIME=\"2019-04-14 23:09:22.0\" MAX_DATA_LENGTH=\"0\" TABLE_COLLATION=\"utf8_general_ci\">" + EOL +
            "<CATEGORY NAME=\"COLUMNS\" CHILD_TYPE=\"COLUMN\" DB_NAME=\"sakila\" TABLE_NAME=\"actor\">" + EOL +
            "<COLUMN NAME=\"actor_id\"></COLUMN>" + EOL +
            "<COLUMN NAME=\"first_name\"></COLUMN>" + EOL +
            "<COLUMN NAME=\"last_name\"></COLUMN>" + EOL +
            "<COLUMN NAME=\"last_update\"></COLUMN></CATEGORY>" + EOL +
            "<CATEGORY NAME=\"INDEXES\" CHILD_TYPE=\"INDEX\" DB_NAME=\"sakila\" TABLE_NAME=\"actor\">" + EOL +
            "<INDEX NAME=\"idx_actor_last_name\"></INDEX></CATEGORY>" + EOL +
            "<CATEGORY NAME=\"FOREIGN_KEYS\" CHILD_TYPE=\"FOREIGN_KEY\" DB_NAME=\"sakila\" TABLE_NAME=\"actor\"></CATEGORY>" + EOL +
            "<CATEGORY NAME=\"PRIMARY_KEYS\" CHILD_TYPE=\"PRIMARY_KEY\" DB_NAME=\"sakila\" TABLE_NAME=\"actor\">" + EOL +
            "<PRIMARY_KEY NAME=\"actor_id\"></PRIMARY_KEY></CATEGORY>" + EOL +
            "<CATEGORY NAME=\"TRIGGERS\" CHILD_TYPE=\"TRIGGER\" DB_NAME=\"sakila\" TABLE_NAME=\"actor\"></CATEGORY>" + EOL +
            "</TABLE>";
        LoaderManager loaderManager = new LoaderManager(connection, DB_TYPE);
        String actualTag = loaderManager.lazyLoad(tag).toString();

        Assert.assertEquals(expectedTag, actualTag);
    }

    @Test
    public void shouldGetTagByFullLoad() throws ReflectionException, DBException {
        String expectedTag = "<TABLE TABLE_CATALOG=\"def\" TABLE_COMMENT=\"\" CHECKSUM=\"\" CHECK_TIME=\"\" ENGINE=\"InnoDB\" DB_NAME=\"sakila\" TABLE_TYPE=\"BASE TABLE\" TABLE_ROWS=\"200\" AVG_ROW_LENGTH=\"81\" UPDATE_TIME=\"\" DATA_LENGTH=\"16384\" DATA_FREE=\"0\" NAME=\"actor\" INDEX_LENGTH=\"16384\" ROW_FORMAT=\"Dynamic\" AUTO_INCREMENT=\"201\" VERSION=\"10\" CREATE_OPTIONS=\"\" CREATE_TIME=\"2019-04-14 23:09:22.0\" MAX_DATA_LENGTH=\"0\" TABLE_COLLATION=\"utf8_general_ci\">" + EOL +
            "<CATEGORY NAME=\"COLUMNS\" CHILD_TYPE=\"COLUMN\" DB_NAME=\"sakila\" TABLE_NAME=\"actor\">" + EOL +
            "<COLUMN TABLE_CATALOG=\"def\" IS_NULLABLE=\"NO\" TABLE_NAME=\"actor\" EXTRA=\"auto_increment\" COLUMN_KEY=\"PRI\" CHARACTER_OCTET_LENGTH=\"\" DB_NAME=\"sakila\" NAME=\"actor_id\" NUMERIC_PRECISION=\"5\" PRIVILEGES=\"select,insert,update,references\" COLUMN_COMMENT=\"\" DATETIME_PRECISION=\"\" COLLATION_NAME=\"\" NUMERIC_SCALE=\"0\" COLUMN_TYPE=\"smallint(5) unsigned\" GENERATION_EXPRESSION=\"\" ORDINAL_POSITION=\"1\" CHARACTER_MAXIMUM_LENGTH=\"\" DATA_TYPE=\"smallint\" CHARACTER_SET_NAME=\"\" COLUMN_DEFAULT=\"\"></COLUMN>" + EOL +
            "<COLUMN TABLE_CATALOG=\"def\" IS_NULLABLE=\"NO\" TABLE_NAME=\"actor\" EXTRA=\"\" COLUMN_KEY=\"\" CHARACTER_OCTET_LENGTH=\"135\" DB_NAME=\"sakila\" NAME=\"first_name\" NUMERIC_PRECISION=\"\" PRIVILEGES=\"select,insert,update,references\" COLUMN_COMMENT=\"\" DATETIME_PRECISION=\"\" COLLATION_NAME=\"utf8_general_ci\" NUMERIC_SCALE=\"\" COLUMN_TYPE=\"varchar(45)\" GENERATION_EXPRESSION=\"\" ORDINAL_POSITION=\"2\" CHARACTER_MAXIMUM_LENGTH=\"45\" DATA_TYPE=\"varchar\" CHARACTER_SET_NAME=\"utf8\" COLUMN_DEFAULT=\"\"></COLUMN>" + EOL +
            "<COLUMN TABLE_CATALOG=\"def\" IS_NULLABLE=\"NO\" TABLE_NAME=\"actor\" EXTRA=\"\" COLUMN_KEY=\"MUL\" CHARACTER_OCTET_LENGTH=\"135\" DB_NAME=\"sakila\" NAME=\"last_name\" NUMERIC_PRECISION=\"\" PRIVILEGES=\"select,insert,update,references\" COLUMN_COMMENT=\"\" DATETIME_PRECISION=\"\" COLLATION_NAME=\"utf8_general_ci\" NUMERIC_SCALE=\"\" COLUMN_TYPE=\"varchar(45)\" GENERATION_EXPRESSION=\"\" ORDINAL_POSITION=\"3\" CHARACTER_MAXIMUM_LENGTH=\"45\" DATA_TYPE=\"varchar\" CHARACTER_SET_NAME=\"utf8\" COLUMN_DEFAULT=\"\"></COLUMN>" + EOL +
            "<COLUMN TABLE_CATALOG=\"def\" IS_NULLABLE=\"NO\" TABLE_NAME=\"actor\" EXTRA=\"on update CURRENT_TIMESTAMP\" COLUMN_KEY=\"\" CHARACTER_OCTET_LENGTH=\"\" DB_NAME=\"sakila\" NAME=\"last_update\" NUMERIC_PRECISION=\"\" PRIVILEGES=\"select,insert,update,references\" COLUMN_COMMENT=\"\" DATETIME_PRECISION=\"0\" COLLATION_NAME=\"\" NUMERIC_SCALE=\"\" COLUMN_TYPE=\"timestamp\" GENERATION_EXPRESSION=\"\" ORDINAL_POSITION=\"4\" CHARACTER_MAXIMUM_LENGTH=\"\" DATA_TYPE=\"timestamp\" CHARACTER_SET_NAME=\"\" COLUMN_DEFAULT=\"CURRENT_TIMESTAMP\"></COLUMN></CATEGORY>" + EOL +
            "<CATEGORY NAME=\"INDEXES\" CHILD_TYPE=\"INDEX\" DB_NAME=\"sakila\" TABLE_NAME=\"actor\">" + EOL +
            "<INDEX NON_UNIQUE=\"1\" TABLE_CATALOG=\"def\" SUB_PART=\"\" TABLE_NAME=\"actor\" COLLATION=\"A\" COLUMN_NAME=\"last_name\" DB_NAME=\"sakila\" NULLABLE=\"\" COMMENT=\"\" NAME=\"idx_actor_last_name\" PACKED=\"\" INDEX_TYPE=\"BTREE\" SEQ_IN_INDEX=\"1\" CARDINALITY=\"121\" INDEX_COMMENT=\"\"></INDEX></CATEGORY>" + EOL +
            "<CATEGORY NAME=\"FOREIGN_KEYS\" CHILD_TYPE=\"FOREIGN_KEY\" DB_NAME=\"sakila\" TABLE_NAME=\"actor\"></CATEGORY>" + EOL +
            "<CATEGORY NAME=\"PRIMARY_KEYS\" CHILD_TYPE=\"PRIMARY_KEY\" DB_NAME=\"sakila\" TABLE_NAME=\"actor\">" + EOL +
            "<PRIMARY_KEY TABLE_CATALOG=\"def\" REFERENCED_TABLE_NAME=\"\" TABLE_NAME=\"actor\" TABLE_SCHEMA=\"sakila\" CONSTRAINT_TYPE=\"PRIMARY KEY\" POSITION_IN_UNIQUE_CONSTRAINT=\"\" CONSTRAINT_NAME=\"PRIMARY\" ORDINAL_POSITION=\"1\" REFERENCED_TABLE_SCHEMA=\"\" REFERENCED_COLUMN_NAME=\"\" NAME=\"actor_id\"></PRIMARY_KEY></CATEGORY>" + EOL +
            "<CATEGORY NAME=\"TRIGGERS\" CHILD_TYPE=\"TRIGGER\" DB_NAME=\"sakila\" TABLE_NAME=\"actor\"></CATEGORY>" + EOL +
            "</TABLE>";
        LoaderManager loaderManager = new LoaderManager(connection, DB_TYPE);
        String actualTag = loaderManager.fullLoad(tag).toString();

        Assert.assertEquals(expectedTag, actualTag);
    }
}
