package unitTest.db.loaders;

import core.db.constants.DBType;
import core.db.loaders.Loader;
import core.db.loaders.LoaderManager;
import core.exceptions.ReflectionException;
import org.junit.Assert;
import org.junit.Test;

import java.util.*;
import java.util.stream.Collectors;

import static core.db.constants.DBType.MySQL;
import static core.db.constants.DBType.PostgreSQL;
import static org.hamcrest.CoreMatchers.is;

public class LoaderManagerTest {
    private final String[] LOADERS_ARRAY = {"ViewLoader", "ForeignKeyLoader", "PrimaryKeyLoader", "ProcedureLoader", "SchemaLoader", "TriggerLoader", "ColumnLoader",
        "IndexLoader", "ParamsLoader", "FunctionLoader", "TableLoader"};
    private DBType dbType = MySQL;
    private DBType dbTypeInvalid = PostgreSQL;

    @Test
    public void shouldGetMapOfLoaders() throws ReflectionException {
        List<String> expectedLoadersNameList = new ArrayList<>(Arrays.asList(LOADERS_ARRAY));

        LoaderManager loaderManager = new LoaderManager(null, dbType);
        List<String> loaderList = loaderManager.getLoadersMap().values().stream().map(loader -> loader.getClass().getSimpleName()).collect(Collectors.toList());

        Assert.assertEquals(expectedLoadersNameList.size(), loaderList.size());

        Collections.sort(loaderList);
        Collections.sort(expectedLoadersNameList);

        Assert.assertEquals(expectedLoadersNameList, loaderList);
    }

    @Test
    public void shouldGetMapOfLoadersWhenDbTypeIsInvalidAndLoaderMapIsEmpty() throws ReflectionException {
        LoaderManager loaderManager = new LoaderManager(null, dbTypeInvalid);
        Map<String, Loader> loaderMap = loaderManager.getLoadersMap();

        Assert.assertThat(loaderMap.size(), is(0));
        Assert.assertTrue(loaderMap.isEmpty());
    }

    @Test
    public void shouldGetMapOfLoadersWhenDbTypeIsNullAndLoaderMapIsEmpty() throws ReflectionException {
        LoaderManager loaderManager = new LoaderManager(null, null);
        Map<String, Loader> loaderMap = loaderManager.getLoadersMap();

        Assert.assertThat(loaderMap.size(), is(0));
        Assert.assertTrue(loaderMap.isEmpty());
    }
}
