package unitTest.db.printers;

import core.db.constants.DBType;
import core.db.loaders.Loader;
import core.db.loaders.LoaderManager;
import core.db.printers.Printer;
import core.db.printers.PrinterManager;
import core.exceptions.ReflectionException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.junit.Assert;
import org.junit.Test;

import static core.db.constants.DBType.MySQL;
import static core.db.constants.DBType.PostgreSQL;
import static org.hamcrest.CoreMatchers.is;

public class PrinterManagerTest {
    private final String[] PRINTERS_ARRAY = {"ForeignKeyPrinter", "TablePrinter", "ProcedurePrinter", "SchemaPrinter", "TriggerPrinter",
        "IndexPrinter", "FunctionPrinter", "ViewPrinter"};
    private DBType dbType = MySQL;
    private DBType dbTypeInvalid = PostgreSQL;

    @Test
    public void shouldGetMapOfPrinters() throws ReflectionException {
        List<String> expectedPrintersNameList = new ArrayList<>(Arrays.asList(PRINTERS_ARRAY));

        PrinterManager printerManager = new PrinterManager(dbType);
        List<String> printerList = printerManager.getPrintersMap().values().stream().map(printer -> printer.getClass().getSimpleName()).collect(Collectors.toList());

        Assert.assertEquals(expectedPrintersNameList.size(), printerList.size());

        Collections.sort(printerList);
        Collections.sort(expectedPrintersNameList);

        Assert.assertEquals(expectedPrintersNameList, printerList);
    }

    @Test
    public void shouldGetMapOfPrintersWhenDbTypeIsInvalidAndPrinterMapIsEmpty() throws ReflectionException {
        PrinterManager printerManager = new PrinterManager(dbTypeInvalid);
        Map<String, Printer> printerMap = printerManager.getPrintersMap();

        Assert.assertThat(printerMap.size(), is(0));
        Assert.assertTrue(printerMap.isEmpty());
    }

    @Test
    public void shouldGetMapOfPrintersWhenDbTypeIsNullAndPrinterMapIsEmpty() throws ReflectionException {
        PrinterManager printerManager = new PrinterManager(null);
        Map<String, Printer> printerMap = printerManager.getPrintersMap();

        Assert.assertThat(printerMap.size(), is(0));
        Assert.assertTrue(printerMap.isEmpty());
    }
}
