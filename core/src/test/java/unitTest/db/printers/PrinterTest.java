package unitTest.db.printers;

import core.db.connection.impl.MySQLConnection;
import core.db.constants.CommonConst;
import core.db.constants.DBType;
import core.db.loaders.LoaderManager;
import core.db.printers.Printer;
import core.db.printers.PrinterManager;
import core.exceptions.DBException;
import core.exceptions.PrintCannotException;
import core.exceptions.ReflectionException;
import core.structure.Tag;
import java.sql.Connection;
import java.sql.SQLException;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import static core.db.constants.DBType.MySQL;

public class PrinterTest {
    private static final String EOL = System.lineSeparator();
    private static final String URL = "localhost:3306/sakila";
    private static final String USER = "root";
    private static final String PASSWORD = "pass";
    private static final String TAG_NAME = "TABLE";
    private static final String TAG_NAME_INVALID = "COLUMN";
    private static final String TAG_NAME_ATTRIBUTE = "actor";
    private static final DBType DB_TYPE = MySQL;

    private Connection connection;
    private Tag tag;

    @Before
    public void setUp() throws DBException, ReflectionException {
        MySQLConnection mySQLConnection = new MySQLConnection(URL, USER, PASSWORD);
        connection = mySQLConnection.getConnection();

        tag = new Tag(TAG_NAME);
        tag.setAttribute(CommonConst.NAME, TAG_NAME_ATTRIBUTE);

        LoaderManager loaderManager = new LoaderManager(connection, DB_TYPE);
        loaderManager.fullLoad(tag);
    }

    @After
    public void closeConnection() throws SQLException {
        if (connection != null) {
            connection.close();
        }
    }

    @Test
    public void shouldPrintTag() throws ReflectionException, PrintCannotException {
        String expectedDdl = "CREATE TABLE actor(" + EOL +
            "\tactor_id smallint(5) unsigned NOT NULL DEFAULT  auto_increment," + EOL +
            "\tfirst_name varchar(45) NOT NULL DEFAULT ''," + EOL +
            "\tlast_name varchar(45) NOT NULL DEFAULT ''," + EOL +
            "\tlast_update timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP," + EOL +
            "\tPRIMARY KEY (actor_id)" + EOL +
            ") ENGINE=InnoDB AUTO_INCREMENT=201 DEFAULT COLLATE=utf8_general_ci;" + EOL +
            EOL +
            EOL +
            "CREATE INDEX idx_actor_last_name ON actor (last_name);" + EOL;

        PrinterManager printerManager = new PrinterManager(DB_TYPE);
        Printer printer = printerManager.getPrintersMap().get(tag.getName());

        String actualDdl = printer.print(tag);

        Assert.assertEquals(expectedDdl, actualDdl);
    }

    @Test
    public void shouldPrintTagByTagNameIsInvalidAndPrinterNull() throws ReflectionException {
        PrinterManager printerManager = new PrinterManager(DB_TYPE);
        Printer printer = printerManager.getPrintersMap().get(TAG_NAME_INVALID);

        Assert.assertNull(printer);
    }

    @Test(expected = PrintCannotException.class)
    public void shouldPrintTagByNotFullLoadThenThrowPrintCannotException() throws ReflectionException, PrintCannotException {
        PrinterManager printerManager = new PrinterManager(DB_TYPE);
        Printer printer = printerManager.getPrintersMap().get(tag.getName());
        
        printer.print(tag.removeChildren());
    }
}
