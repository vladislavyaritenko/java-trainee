package unitTest.db.conenction;

import core.db.connection.impl.MySQLConnection;
import core.exceptions.DBException;
import java.sql.SQLException;
import org.junit.Assert;
import org.junit.Test;

import java.sql.Connection;

public class MySQLConnectionTest {
    private static final String URL = "localhost:3306/sakila";
    private static final String USER = "root";
    private static final String PASSWORD = "pass";
    private static final String PASSWORD_INVALID = "asdf";

    @Test
    public void shouldGetConnectionAndConnectionNotNull() throws DBException, SQLException {
        MySQLConnection mySQLConnection = new MySQLConnection(URL, USER, PASSWORD);
        Connection connection = mySQLConnection.getConnection();

        Assert.assertNotNull(connection);

        if(connection != null){
            connection.close();
        }
    }

    @Test(expected = DBException.class)
    public void shouldGetConnectionThenThrowDBException() throws DBException {
        MySQLConnection mySQLConnection = new MySQLConnection(URL, USER, PASSWORD_INVALID);
        mySQLConnection.getConnection();
    }
}
