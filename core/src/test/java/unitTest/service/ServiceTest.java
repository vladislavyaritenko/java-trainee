package unitTest.service;

import core.constants.TypeFileConst;
import core.exceptions.ParserException;
import core.exceptions.SetStrategyException;
import core.service.parsing.ParsingClient;
import core.service.parsing.ParsingStrategy;
import core.service.parsing.impl.ParseFromXML;
import core.service.saving.SavingClient;
import core.service.saving.SavingStrategy;
import core.service.saving.impl.SaveToXML;
import core.structure.Tag;
import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.StringJoiner;

public class ServiceTest {
    private static final String PATH_TO_INPUT_VALID_FILE = "src/test/resources/testInputValidFile.xml";
    private static final String PATH_TO_INPUT_INVALID_FILE = "src/test/resources/testInputInvalidFile.xml";
    private static final String PATH_TO_OUTPUT_FILE = "src/test/resources/testOutputFile.xml";
    private static final String INVALID_PATH_TO_OUTPUT_FILE = "src/test/resources/rrr/testOutputFile.xml";
    private static final TypeFileConst VALID_TYPE_FILE = TypeFileConst.XML;
    private static final TypeFileConst INVALID_TYPE_FILE = TypeFileConst.TXT;
    private static final String EOL = System.lineSeparator();

    @Test
    public void shouldReadXmlFile() throws ParserException {
        StringJoiner joiner = new StringJoiner(EOL);
        joiner.add("<papers>").add("<paper type=\"newspaper\">").add("<titel>Simon > 5</titel>")
            .add("<monthly>true</monthly>").add("<color>false</color>").add("<size>15</size></paper>").add("</papers>");
        String tagExpected = joiner.toString();

        ParseFromXML parsingStrategy = new ParseFromXML();
        Tag tag = parsingStrategy.parse(PATH_TO_INPUT_VALID_FILE);
        String tagActual = tag.toString();

        Assert.assertEquals(tagExpected, tagActual);
    }

    @Test(expected = ParserException.class)
    public void shouldReadXmlFileAndThrowParserException() throws ParserException {
        ParseFromXML parsingStrategy = new ParseFromXML();
        parsingStrategy.parse(PATH_TO_INPUT_INVALID_FILE);
    }

    @Test
    public void shouldReadAndThenWriteToXmlFile() throws ParserException, IOException {
        ParseFromXML parsingStrategy = new ParseFromXML();
        Tag tag = parsingStrategy.parse(PATH_TO_INPUT_VALID_FILE);

        SaveToXML savingStrategy = new SaveToXML();
        savingStrategy.save(tag, PATH_TO_OUTPUT_FILE);

        List<String> strings1 = Files.readAllLines(Paths.get(PATH_TO_INPUT_VALID_FILE), StandardCharsets.UTF_8);
        List<String> strings2 = Files.readAllLines(Paths.get(PATH_TO_OUTPUT_FILE), StandardCharsets.UTF_8);

        int size = strings1.size() > strings2.size() ? strings1.size() : strings2.size();

        for (int i = 1; i < size; i++) {
            Assert.assertEquals(strings2.get(i), strings1.get(i));
        }
    }

    @Test(expected = ParserException.class)
    public void shouldReadAndWriteToXmlFileByInvalidPathAndThenThrowParserException() throws ParserException {
        ParseFromXML parsingStrategy = new ParseFromXML();
        Tag tag = parsingStrategy.parse(PATH_TO_INPUT_VALID_FILE);

        SaveToXML savingStrategy = new SaveToXML();
        savingStrategy.save(tag, INVALID_PATH_TO_OUTPUT_FILE);
    }

    @Test(expected = ParserException.class)
    public void shouldWriteToXmlFileAndThenThrowParserException() throws ParserException {
        Tag tag = null;

        SaveToXML savingStrategy = new SaveToXML();
        savingStrategy.save(tag, INVALID_PATH_TO_OUTPUT_FILE);
    }

    @Test(expected = ParserException.class)
    public void shouldWriteToXmlFileAndThenThrowParserExceptionDueToTagEqualsNull() throws ParserException {
        Tag tag = null;

        SaveToXML savingStrategy = new SaveToXML();
        savingStrategy.save(tag, PATH_TO_OUTPUT_FILE);
    }

    @Test
    public void shouldSetStrategyForParsingClient() {
        ParsingClient parsingClient = new ParsingClient();
        ParseFromXML parsingStrategy = new ParseFromXML();
        parsingClient.setStrategy(parsingStrategy);

        ParsingStrategy strategy = parsingClient.getStrategy();

        Assert.assertNotNull(strategy);
    }

    @Test
    public void shouldExecuteParsingStrategy() throws ParserException {
        ParsingClient parsingClient = new ParsingClient();
        ParseFromXML implParsingStrategy = new ParseFromXML();

        parsingClient.setStrategy(implParsingStrategy);
        Tag tag = parsingClient.executeStrategy(PATH_TO_INPUT_VALID_FILE);

        StringJoiner joiner = new StringJoiner(EOL);
        joiner.add("<papers>").add("<paper type=\"newspaper\">").add("<titel>Simon > 5</titel>")
            .add("<monthly>true</monthly>").add("<color>false</color>").add("<size>15</size></paper>").add("</papers>");

        String expectedString = joiner.toString();
        String actualString = tag.toString();

        Assert.assertEquals(expectedString, actualString);
    }

    @Test
    public void shouldSetStrategyForSavingClient() throws SetStrategyException {
        SavingClient savingClient = new SavingClient();
        savingClient.setStrategy(VALID_TYPE_FILE);

        SavingStrategy savingStrategy = savingClient.getStrategy();
        Assert.assertNotNull(savingStrategy);

        String expectedClass = "SaveToXML";
        String actualClass = savingStrategy.getClass().getSimpleName();

        Assert.assertEquals(expectedClass, actualClass);
    }

    @Test(expected = SetStrategyException.class)
    public void shouldSetStrategyForSavingClientAndThrowParserException() throws SetStrategyException {
        SavingClient savingClient = new SavingClient();
        savingClient.setStrategy(INVALID_TYPE_FILE);
    }

    @Test
    public void shouldExecuteSavingStrategy() throws ParserException {
        ParsingClient parsingClient = new ParsingClient();
        ParseFromXML implParsingStrategy = new ParseFromXML();

        parsingClient.setStrategy(implParsingStrategy);
        Tag tag = parsingClient.executeStrategy(PATH_TO_INPUT_VALID_FILE);

        SavingClient savingClient = new SavingClient();
        savingClient.setStrategy(VALID_TYPE_FILE);
        savingClient.executeStrategy(tag, PATH_TO_OUTPUT_FILE);

        Assert.assertTrue(Files.exists(Paths.get(PATH_TO_OUTPUT_FILE)));
    }
}
