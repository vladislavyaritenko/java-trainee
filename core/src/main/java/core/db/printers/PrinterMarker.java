package core.db.printers;


import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Annotation for Printers.
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface PrinterMarker {
    /**
     * Method using for setting the value of the database type.
     *
     * @return database type value.
     */
    String value();
}

