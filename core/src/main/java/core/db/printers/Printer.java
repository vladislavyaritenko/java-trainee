package core.db.printers;

import core.exceptions.PrintCannotException;
import core.structure.Tag;

/**
 * Interface for all printers.
 */
public interface Printer {
    /**
     * Method using for printing text that contains a sql query for creating an entity.
     *
     * @param tag java object.
     * @return text that contains a sql query for creating an entity.
     * @throws PrintCannotException when tag was loaded by detail or lazy load.
     */
    String print(Tag tag) throws PrintCannotException;
}
