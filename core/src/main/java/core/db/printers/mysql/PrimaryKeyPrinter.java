package core.db.printers.mysql;

import core.db.constants.CommonConst;
import core.db.printers.Printer;
import core.exceptions.PrintCannotException;
import core.structure.Tag;

import java.util.Map;

/**
 * Class using for printing the mysql Information about primary keys from Java Objects.
 */
public class PrimaryKeyPrinter implements Printer {
    @Override
    public String print(Tag key) throws PrintCannotException {
        if(key.getAttributes().size()>1) {
            StringBuilder result = new StringBuilder();
            result.append("PRIMARY KEY").append(" (");
            Map<String, String> attributes = key.getAttributes();
            result.append(attributes.get(CommonConst.NAME));
            result.append("),");
            return result.toString();
        }

        throw new PrintCannotException("Tag " + key.getName() + " was loaded by detail or lazy load. Please, load the tag by full load!");

    }
}
