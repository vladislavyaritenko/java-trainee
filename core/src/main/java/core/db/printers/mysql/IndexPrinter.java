package core.db.printers.mysql;

import core.db.constants.CommonConst;
import core.db.constants.mysql.MySQLConst;
import core.db.printers.Printer;
import core.db.printers.PrinterMarker;
import core.exceptions.PrintCannotException;
import core.structure.Tag;

/**
 * Class using for printing the mysql Information about indexes from Java Objects.
 */
@PrinterMarker(MySQLConst.INDEX)
public class IndexPrinter implements Printer {
    private static final String EOL = System.lineSeparator();

    @Override
    public String print(Tag index) throws PrintCannotException {
        if (index.getAttributes().size() > 1) {
            StringBuilder result = new StringBuilder();
            result.append("CREATE ");
            if ("0".equals(index.getAttributeByKey("NON_UNIQUE"))) {
                result.append("UNIQUE KEY (").append(index.getAttributeByKey("COLUMNS_NAME")).append("),").append(EOL);
            }

            String indexType = index.getAttributeByKey("INDEX_TYPE");
            result.append("BTREE".equals(indexType) ? "" : indexType + " ");

            result.append("INDEX ")
                .append(index.getAttributeByKey(CommonConst.NAME))
                .append(" ON ")
                .append(index.getAttributeByKey("TABLE_NAME"))
                .append(" (")
                .append(index.getAttributeByKey("COLUMN_NAME"))
                .append(");");

            return result.toString();
        }

        throw new PrintCannotException("Tag " + index.getName() + " was loaded by detail or lazy load. Please, load the tag by full load!");

    }
}
