package core.db.printers.mysql;

import core.db.constants.CommonConst;
import core.db.constants.mysql.MySQLConst;
import core.db.printers.Printer;
import core.db.printers.PrinterMarker;
import core.exceptions.PrintCannotException;
import core.structure.Tag;

/**
 * Class using for printing the mysql Information about triggers from Java Objects.
 */
@PrinterMarker(MySQLConst.TRIGGER)
public class TriggerPrinter implements Printer {
    private static final String EOL = System.lineSeparator();
    private static final StringBuilder EOLT = new StringBuilder(System.lineSeparator()).append("\t");

    @Override
    public String print(Tag trigger) throws PrintCannotException {
        if (trigger.getAttributes().size() > 1) {
            StringBuilder result = new StringBuilder();
            result.append("DELIMITER |");
            result.append(System.lineSeparator())
                .append("CREATE TRIGGER ")
                .append(trigger.getAttributeByKey(CommonConst.NAME)).append(" ")
                .append(trigger.getAttributeByKey("ACTION_TIMING")).append(" ")
                .append(trigger.getAttributeByKey("EVENT_MANIPULATION")).append(" ON ")
                .append(trigger.getAttributeByKey("EVENT_OBJECT_TABLE")).append(EOLT)
                .append("FOR EACH ")
                .append(trigger.getAttributeByKey("ACTION_ORIENTATION")).append(EOLT)
                .append(trigger.getAttributeByKey("ACTION_STATEMENT")).append("; ").append(EOL).append("|");
            result.append(EOL).append("DELIMITER ;");
            return result.toString();
        }

        throw new PrintCannotException("Tag " + trigger.getName() + " was loaded by detail or lazy load. Please, load the tag by full load!");

    }
}
