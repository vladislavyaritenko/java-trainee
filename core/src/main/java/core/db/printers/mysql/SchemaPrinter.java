package core.db.printers.mysql;

import core.db.constants.CommonConst;
import core.db.constants.mysql.MySQLConst;
import core.db.printers.Printer;
import core.db.printers.PrinterMarker;
import core.exceptions.PrintCannotException;
import core.structure.Tag;

import java.util.Map;

/**
 * Class using for printing the mysql Information about schema from Java Objects.
 */
@PrinterMarker(MySQLConst.SCHEMA)
public class SchemaPrinter implements Printer {
    private static final String EOL = System.lineSeparator();

    @Override
    public String print(Tag tag) throws PrintCannotException {
        if (tag.getAttributes().size() > 1) {
            StringBuilder result = new StringBuilder();
            Map<String, String> attributes = tag.getAttributes();

            result.append("DROP SCHEMA IF EXISTS ")
                .append(attributes.get(CommonConst.DB_NAME)).append(";").append(EOL)
                .append("CREATE SCHEMA ")
                .append(attributes.get(CommonConst.DB_NAME)).append(";").append(EOL)
                .append("DEFAULT CHARACTER SET ")
                .append(attributes.get("DEFAULT_CHARACTER_SET_NAME")).append(";").append(EOL)
                .append("DEFAULT COLLATE ")
                .append(attributes.get("DEFAULT_COLLATION_NAME")).append(";").append(EOL)
                .append("USE ")
                .append(attributes.get(CommonConst.DB_NAME))
                .append(";");
            return result.toString();
        }

        throw new PrintCannotException("Tag " + tag.getName() + " was loaded by detail or lazy load. Please, load the tag by full load!");
    }
}
