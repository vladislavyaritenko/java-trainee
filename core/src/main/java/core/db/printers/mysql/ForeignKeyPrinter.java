package core.db.printers.mysql;

import core.db.constants.CommonConst;
import core.db.constants.mysql.MySQLConst;
import core.db.printers.Printer;
import core.db.printers.PrinterMarker;
import core.exceptions.PrintCannotException;
import core.structure.Tag;

/**
 * Class using for printing the mysql Information about foreign keys from Java Objects.
 */
@PrinterMarker(MySQLConst.FOREIGN_KEY)
public class ForeignKeyPrinter implements Printer {

    @Override
    public String print(Tag key) throws PrintCannotException {
        if (key.getAttributes().size() > 1) {
            StringBuilder result = new StringBuilder();
            result.append("CONSTRAINT ")
                .append(key.getAttributeByKey(CommonConst.NAME)).append(" ")
                .append("FOREIGN KEY (").append(key.getAttributeByKey("COLUMN_NAME")).append(") REFERENCES ")
                .append(key.getAttributeByKey("REFERENCED_TABLE_NAME"))
                .append(" (").append(key.getAttributeByKey("REFERENCED_COLUMN_NAME"))
                .append(") ON DELETE ").append(key.getAttributeByKey("DELETE_RULE"))
                .append(" ON UPDATE ").append(key.getAttributeByKey("UPDATE_RULE"))
                .append(";");
            return result.toString();
        }

        throw new PrintCannotException("Tag " + key.getName() + " was loaded by detail or lazy load. Please, load the tag by full load!");

    }
}
