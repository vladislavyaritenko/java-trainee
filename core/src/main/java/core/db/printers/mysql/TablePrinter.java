package core.db.printers.mysql;

import core.db.constants.CommonConst;
import core.db.constants.mysql.MySQLConst;
import core.db.printers.Printer;
import core.db.printers.PrinterMarker;
import core.exceptions.PrintCannotException;
import core.structure.Tag;

import java.util.List;

/**
 * Class using for printing the mysql Information about tables from Java Objects.
 */
@PrinterMarker(MySQLConst.TABLE)
public class TablePrinter implements Printer {
    private static final String EOL = System.lineSeparator();
    private static final StringBuilder EOLT = new StringBuilder(System.lineSeparator()).append("\t");

    @Override
    public String print(Tag tag) throws PrintCannotException {
        if ((tag.getChildrenList().size() == 5) && (tag.getAttributes().size() > 1)) {
            StringBuilder result = new StringBuilder();

            result.append("CREATE TABLE ").append(tag.getAttributeByKey(CommonConst.NAME)).append("(");

            List<Tag> columns = tag.getChildrenList().get(0).getChildrenList();
            for (Tag column : columns) {
                result.append(EOLT).append(new ColumnsPrinter().print(column));
            }

            List<Tag> keysPrimary = tag.getChildrenList().get(3).getChildrenList();
            for (Tag key : keysPrimary) {
                result.append(EOLT).append(new PrimaryKeyPrinter().print(key));
            }

            result.deleteCharAt(result.length() - 1);

            result.append(EOL)
                .append(") ENGINE=")
                .append(tag.getAttributeByKey("ENGINE"))
                .append(" AUTO_INCREMENT=")
                .append(tag.getAttributeByKey("AUTO_INCREMENT"))
                .append(" DEFAULT COLLATE=")
                .append(tag.getAttributeByKey("TABLE_COLLATION")).append(";").append(EOL);

            List<Tag> keysForeign = tag.getChildrenList().get(2).getChildrenList();
            if (!keysForeign.isEmpty()) {
                for (Tag key : keysForeign) {
                    result.append(EOL).append(new ForeignKeyPrinter().print(key));
                }
            }

            result.append(EOL);

            List<Tag> indexes = tag.getChildrenList().get(1).getChildrenList();
            for (Tag index : indexes) {
                result.append(EOL).append(new IndexPrinter().print(index));
            }

            result.append(EOL);

            List<Tag> triggers = tag.getChildrenList().get(4).getChildrenList();
            if (triggers.size() > 0) {
                for (Tag trigger : triggers) {
                    result.append(EOL).append(new TriggerPrinter().print(trigger));
                }
            }

            return result.toString();
        }

        throw new PrintCannotException("Tag " + tag.getName() + " was loaded by detail or lazy load. Please, load the tag by full load!");
    }
}
