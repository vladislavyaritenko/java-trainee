package core.db.printers.mysql;


import core.db.constants.CommonConst;
import core.db.constants.mysql.MySQLConst;
import core.db.printers.Printer;
import core.db.printers.PrinterMarker;
import core.exceptions.PrintCannotException;
import core.structure.Tag;

import java.util.List;
import java.util.Map;

/**
 * Class using for printing the mysql Information about functions from Java Objects.
 */
@PrinterMarker(MySQLConst.FUNCTION)
public class FunctionPrinter implements Printer {
    private static final String EOL = System.lineSeparator();

    @Override
    public String print(Tag tag) throws PrintCannotException {
        if ((tag.getChildrenList().size() == 1) && (tag.getAttributes().size() > 1)) {
            StringBuilder result = new StringBuilder();

            Map<String, String> attributes = tag.getAttributes();
            List<Tag> params = tag.getChildrenList().get(0).getChildrenList();

            result.append("CREATE FUNCTION ").append(attributes.get(CommonConst.NAME)).append("(");

            for (int i = 1; i < params.size(); i++) {
                result.append(params.get(i).getAttributes().get("PARAMETER_NAME")).append(" ")
                    .append(params.get(i).getAttributes().get("DTD_IDENTIFIER"));
            }

            result.append(") RETURNS ")
                .append(params.get(0).getAttributes().get("DTD_IDENTIFIER"))
                .append(EOL);

            if ("YES".equals(attributes.get("IS_DETERMINISTIC"))) {
                result.append("DETERMINISTIC").append(EOL);
            }

            result.append(attributes.get("SQL_DATA_ACCESS")).append(EOL).append(attributes.get("ROUTINE_DEFINITION")).append(";");
            return result.toString();
        }

        throw new PrintCannotException("Tag " + tag.getName() + " was loaded by detail or lazy load. Please, load the tag by full load!");

    }
}
