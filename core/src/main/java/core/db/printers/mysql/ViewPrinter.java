package core.db.printers.mysql;

import core.db.constants.CommonConst;
import core.db.constants.mysql.MySQLConst;
import core.db.printers.Printer;
import core.db.printers.PrinterMarker;
import core.exceptions.PrintCannotException;
import core.structure.Tag;

import java.util.Map;

/**
 * Class using for printing the mysql Information about views from Java Objects.
 */
@PrinterMarker(MySQLConst.VIEW)
public class ViewPrinter implements Printer {
    private static final String EOL = System.lineSeparator();

    @Override
    public String print(Tag tag) throws PrintCannotException {
        if (tag.getAttributes().size() > 1) {
            StringBuilder result = new StringBuilder();
            Map<String, String> attrs = tag.getAttributes();

            result.append("CREATE ");
            if ("INVOKER".equals(attrs.get("SECURITY_TYPE"))) {
                result.append("DEFINER=CURRENT_USER SQL SECURITY INVOKER ");
            }
            result.append("VIEW ")
                .append(attrs.get(CommonConst.NAME)).append(EOL)
                .append("AS").append(EOL)
                .append(attrs.get("VIEW_DEFINITION")).append(";");
            return result.toString();
        }

        throw new PrintCannotException("Tag " + tag.getName() + " was loaded by detail or lazy load. Please, load the tag by full load!");
    }
}
