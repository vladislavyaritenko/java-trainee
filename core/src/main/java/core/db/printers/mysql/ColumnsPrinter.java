package core.db.printers.mysql;

import core.db.constants.CommonConst;
import core.db.printers.Printer;
import core.exceptions.PrintCannotException;
import core.structure.Tag;

/**
 * Class using for printing the mysql Information about columns from Java Objects.
 */
public class ColumnsPrinter implements Printer {

    @Override
    public String print(Tag column) throws PrintCannotException {
        if(column.getAttributes().size() > 1) {
            StringBuilder result = new StringBuilder();
            result.append(column.getAttributeByKey(CommonConst.NAME)).append(" ").append(column.getAttributeByKey("COLUMN_TYPE"));
            if ("NO".equals(column.getAttributeByKey("IS_NULLABLE"))) {
                result.append(" NOT NULL");
            }
            String columnDefault = column.getAttributeByKey("COLUMN_DEFAULT");
            if (columnDefault != null) {
                String dataType = column.getAttributeByKey("DATA_TYPE");
                if ("varchar".equals(dataType) || "text".equals(dataType) || "enum".equals(dataType) || "set".equals(dataType)) {
                    columnDefault = "'" + columnDefault + "'";
                }
                result.append(" DEFAULT ").append(columnDefault);
            }
            if (!column.getAttributeByKey("EXTRA").isEmpty()) {
                result.append(" ").append(column.getAttributeByKey("EXTRA"));
            }
            result.append(",");
            return result.toString();
        }

        throw new PrintCannotException("Tag " + column.getName() + " was loaded by detail or lazy load. Please, load the tag by full load!");

    }
}
