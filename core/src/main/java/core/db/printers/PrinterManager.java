package core.db.printers;

import core.db.ReflectionUtil;
import core.db.constants.DBType;
import core.exceptions.ReflectionException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.HashMap;
import java.util.Map;

/**
 * Class using for managing all loaders.
 */
public class PrinterManager {
    private static final Logger LOGGER = LogManager.getLogger(PrinterManager.class);

    private Map<String, Printer> printersMap;
    private ReflectionUtil reflection;

    /**
     * PrinterManager class constructor.
     *
     * @param dbType database type.
     * @throws ReflectionException
     */
    public PrinterManager(DBType dbType) throws ReflectionException {
        printersMap = new HashMap<>();
        reflection = new ReflectionUtil(dbType, PrinterMarker.class);
        getPrintersMap();
    }

    /**
     * Method using for filling printers map.
     *
     * @return printers map.
     */
    public Map<String, Printer> getPrintersMap() {
        LOGGER.info("Get the map of printers from reflection util...");
        for (Map.Entry<String, Object> entry : reflection.getMap().entrySet()) {
            printersMap.put(entry.getKey(), (Printer) entry.getValue());
        }
        return printersMap;
    }
}
