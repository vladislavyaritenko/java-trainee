package core.db;

import core.db.constants.DBType;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Annotation for Packages.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.PACKAGE)
public @interface PackageMarker {
    /**
     * Database type.
     */
    DBType database();

    /**
     * Object type of Class for mechanisms Loader and Printer.
     */
    Class clazz();
}
