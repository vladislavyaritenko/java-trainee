package core.db;

import core.db.constants.DBType;
import core.exceptions.ReflectionException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.*;
import java.util.jar.JarInputStream;
import java.util.zip.ZipEntry;

/**
 * Class using for filling the map of loaders/printers.
 * Search params must be:
 * 1) Database type
 * 2) Loaders or printers
 */
public class ReflectionUtil {
    private static final Logger LOGGER = LogManager.getLogger(ReflectionUtil.class);

    private static final String PACKAGE_INFO = "package-info";
    private static final String ROOT_PACKAGE_NAME = "core";
    private static final String FILE_PREFIX = "file:";
    private static final String JAR_PATH_SEPARATOR = "!\\";

    private Map<String, Object> map;
    private DBType dbType;
    private Class<? extends Annotation> annotation;

    /**
     * ReflectionUtil class constructor.
     *
     * @param dbType     database type.
     * @param annotation annotation for defining the component: loaders or printers.
     * @throws ReflectionException
     */
    public ReflectionUtil(DBType dbType, Class<? extends Annotation> annotation) throws ReflectionException {
        this.map = new HashMap<>();

        this.dbType = dbType;
        this.annotation = annotation;

        List<Class> classList = findClassesInDirectory();
        fillMap(classList);
    }

    private static boolean isJar(File file) {
        String directory = file.getPath();
        return directory.startsWith(FILE_PREFIX) && directory.contains(JAR_PATH_SEPARATOR);
    }

    private static List<Class> findClassesInDirectory(File directory, String packageName) throws ReflectionException {
        List<Class> classes = new ArrayList<>();
        if (!directory.exists()) {
            return classes;
        }

        File[] files = directory.listFiles();
        if (files != null) {
            for (File file : files) {
                if (file.isDirectory()) {
                    classes.addAll(findClassesInDirectory(file, packageName + "." + file.getName()));
                } else if (file.getName().endsWith(".class")) {
                    String className = packageName + '.' + file.getName().substring(0, file.getName().length() - 6);
                    try {
                        classes.add(Class.forName(className));
                    } catch (ClassNotFoundException e) {
                        LOGGER.error("ClassNotFoundException loading " + className);
                        throw new ReflectionException("ClassNotFoundException loading " + className);
                    }
                }
            }
        }
        return classes;
    }

    private static List<Class> findClassesInJar(File resourcePath, String rootPack) throws ReflectionException {
        List<Class> classes = new ArrayList<>();
        String path = resourcePath.getPath();
        String[] split = path.split("!\\\\");
        try {
            URL jarUrl = new URL(split[0]);
            JarInputStream zip = new JarInputStream(jarUrl.openStream());
            ZipEntry zipEntry;
            while ((zipEntry = zip.getNextEntry()) != null) {
                String entryName = zipEntry.getName();
                if (!entryName.endsWith(".class") || !entryName.startsWith(rootPack) || entryName.contains("$")) {
                    continue;
                }
                String className = entryName.substring(0, entryName.length() - 6).replace('/', '.');
                try {
                    classes.add(Class.forName(className));
                } catch (ClassNotFoundException e) {
                    LOGGER.error("ClassNotFoundException loading " + className);
                    throw new ReflectionException("ClassNotFoundException loading " + className);
                }
            }
        } catch (MalformedURLException e) {
            LOGGER.error(e);
            throw new ReflectionException(e);
        } catch (IOException e) {
            LOGGER.error(e);
            throw new ReflectionException(e);
        }
        return classes;
    }

    public Map<String, Object> getMap() {
        return map;
    }

    /**
     * Method using for finding loader and printer classes.
     *
     * @param classes classes in root package.
     * @return map where key is class name value is class object.
     * @throws ReflectionException
     */
    public Map<String, Object> fillMap(List<Class> classes) throws ReflectionException {
        String pack = ".package-info";
        String path = "";
        for (Class<?> klass : classes) {
            if (klass.getName().endsWith(pack)) {
                StringBuilder set = new StringBuilder();
                for (int i = 0; i < klass.getName().length() - pack.length(); i++) {
                    char chan = klass.getName().charAt(i);
                    set.append(chan);
                }
                path = set.toString();
            }

            if (PACKAGE_INFO.equals(klass.getSimpleName())) {
                PackageMarker reflectionAnnotation = klass.getAnnotation(PackageMarker.class);
                if ((reflectionAnnotation.database() == dbType) & (reflectionAnnotation.clazz() == annotation)) {
                    List<Class> packClasses = new ArrayList<>();
                    for (Class<?> clazz : classes) {
                        String className = clazz.getName();
                        if (className.startsWith(path)) {
                            packClasses.add(clazz);
                        }
                    }

                    for (Class<?> clazz : packClasses) {
                        try {
                            Annotation annotationMarker = clazz.getAnnotation(annotation);
                            if (annotationMarker != null) {
                                Class<?> type1 = annotationMarker.annotationType();
                                try {
                                    Method method = type1.getMethod("value");
                                    try {
                                        map.put(method.invoke(annotationMarker).toString(), clazz.newInstance());
                                    } catch (InvocationTargetException e) {
                                        LOGGER.error(e);
                                        throw new ReflectionException(e);
                                    }
                                } catch (NoSuchMethodException e) {
                                    LOGGER.error(e);
                                    throw new ReflectionException(e);
                                }
                            }
                        } catch (InstantiationException | IllegalAccessException e) {
                            LOGGER.error(e);
                            throw new ReflectionException(e);
                        }
                    }
                }
            }
        }
        return map;
    }

    private String findRootElement() {
        String pack = this.getClass().getPackage().getName();
        String[] directories = pack.split("\\.");

        return directories[0];
    }

    /**
     * Method using for finding all classes in the root package.
     *
     * @return list of classes in root package.
     * @throws ReflectionException
     */
    public ArrayList<Class> findClassesInDirectory() throws ReflectionException {
        String path = findRootElement();
        ArrayList<Class> classes = new ArrayList<>();
        try {
            Enumeration<URL> resources = Thread.currentThread().getContextClassLoader().getResources(path);
            List<File> dirs = new ArrayList<>();
            while (resources.hasMoreElements()) {
                URL resource = resources.nextElement();
                dirs.add(new File(resource.getFile()));
            }

            for (File directory : dirs) {
                if (isJar(directory)) {
                    classes.addAll(findClassesInJar(directory, ROOT_PACKAGE_NAME));
                } else {
                    classes.addAll(findClassesInDirectory(directory, ROOT_PACKAGE_NAME));
                }
            }
        } catch (IOException e) {
            LOGGER.error(e);
            throw new ReflectionException(e);
        }
        return classes;
    }
}
