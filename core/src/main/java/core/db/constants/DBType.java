package core.db.constants;

/**
 * Constants for identifying core.db.
 */
public enum DBType {
    MySQL("mysql"), PostgreSQL("postgresql");
    private String value;

    DBType(String value) {
        this.value = value;
    }

    /**
     * Method using for getting the object value of the console.command by its value.
     *
     * @param type object console.command value.
     * @return console.command object value.
     */
    public static DBType getDBType(String type) {
        for (DBType typeDb : DBType.values()) {
            if (type.toLowerCase().equals(typeDb.value)) {
                return typeDb;
            }
        }
        return null;
    }
}
