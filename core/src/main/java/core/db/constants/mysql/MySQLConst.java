package core.db.constants.mysql;

/**
 * mysql Constants for Loaders and Printers.
 */
public final class MySQLConst {
    private MySQLConst() {
    }

    public static final String COLUMNS = "COLUMNS";
    public static final String VIEWS = "VIEWS";
    public static final String TABLES = "TABLES";
    public static final String FUNCTIONS = "FUNCTIONS";
    public static final String PROCEDURES = "PROCEDURES";

    public static final String INDEXES = "INDEXES";
    public static final String FOREIGN_KEYS = "FOREIGN_KEYS";
    public static final String PRIMARY_KEYS = "PRIMARY_KEYS";
    public static final String TRIGGERS = "TRIGGERS";
    public static final String PARAMETERS = "PARAMETERS";

    public static final String COLUMN = "COLUMN";
    public static final String VIEW = "VIEW";
    public static final String TABLE = "TABLE";
    public static final String SCHEMA = "SCHEMA";
    public static final String FUNCTION = "FUNCTION";
    public static final String PROCEDURE = "PROCEDURE";

    public static final String INDEX = "INDEX";
    public static final String FOREIGN_KEY = "FOREIGN_KEY";
    public static final String PRIMARY_KEY = "PRIMARY_KEY";
    public static final String TRIGGER = "TRIGGER";
    public static final String PARAMETER = "PARAMETER";
}
