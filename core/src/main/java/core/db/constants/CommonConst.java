package core.db.constants;

/**
 * Common core.constants.
 */
public final class CommonConst {
    private CommonConst() {
    }

    public static final String CATEGORY = "CATEGORY";
    public static final String CHILD_TYPE = "CHILD_TYPE";

    public static final String LAZY = "lazy";
    public static final String FULL = "full";
    public static final String DETAIL = "detail";

    public static final String SCHEMA = "SCHEMA";
    public static final String NAME = "NAME";
    public static final String DB_NAME = "DB_NAME";
}
