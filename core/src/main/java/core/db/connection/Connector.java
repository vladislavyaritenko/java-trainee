package core.db.connection;

import core.exceptions.DBException;

import java.sql.Connection;

/**
 * Interface contains a method for getting a core.db connection.
 */
public interface Connector {
    /**
     * Method using for getting connection to the database.
     *
     * @return java.sql.Connector.
     * @throws DBException when the connection is not established.
     */
    Connection getConnection() throws DBException;
}
