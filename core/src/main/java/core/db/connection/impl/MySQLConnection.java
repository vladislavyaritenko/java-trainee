package core.db.connection.impl;


import core.db.connection.Connector;
import core.exceptions.DBException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Class using for obtaining connection to core.db.
 */
public class MySQLConnection implements Connector {
    private static final Logger LOGGER = LogManager.getLogger(MySQLConnection.class);

    private String url;
    private String user;
    private String password;

    /**
     * Input class constructor.
     *
     * @param url      database url.
     * @param user     database user.
     * @param password database password.
     */
    public MySQLConnection(String url, String user, String password) {
        this.url = url;
        this.user = user;
        this.password = password;
    }

    @Override
    public Connection getConnection() throws DBException {
        LOGGER.info("Connecting to database...");

        Connection connection;
        StringBuilder stringBuilderUrl = new StringBuilder();
        stringBuilderUrl.append("jdbc:mysql://").append(url).append("?serverTimezone=UTC");
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            connection = DriverManager.getConnection(stringBuilderUrl.toString(), user, password);

            if (connection == null) {
                LOGGER.info("Connection not established.");
                throw new DBException("Connection not established.");
            }

            LOGGER.info("Connection successful.");
        } catch (ClassNotFoundException | SQLException e) {
            LOGGER.error("ClassNotFoundException inside Class.forName()", e);
            throw new DBException("ClassNotFoundException inside Class.forName()", e);
        }
        LOGGER.info("Connected to DB.");
        return connection;
    }
}
