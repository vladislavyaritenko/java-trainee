package core.db.connection;

import core.db.connection.impl.MySQLConnection;
import core.db.constants.DBType;
import core.exceptions.DBException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;

/**
 * Class using for managing connection.
 */
public class ConnectionManager {
    private static final Logger LOGGER = LogManager.getLogger(ConnectionManager.class);

    /**
     * Method using for selecting the class containing the method to getValue the connection to the selected core.db.
     *
     * @param dbType database type.
     * @return The class to getValue connections to the selected core.db.
     * @throws DBException when the required database is not found.
     */
    public Connection initConnection(DBType dbType, String url, String user, String password) throws DBException {
        Connection result;
        switch (dbType) {
            case MySQL:
                result = new MySQLConnection(url, user, password).getConnection();
                break;
            default:
                throw new DBException("There is no such database.");
        }
        LOGGER.info("The connection is established and chosen database is" + dbType);
        return result;
    }
}
