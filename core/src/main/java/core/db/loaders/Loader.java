package core.db.loaders;

import core.exceptions.DBException;
import core.structure.Tag;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

/**
 * Interface for all loaders.
 */
public interface Loader {

    /**
     * Method using for filling the empty tag by lazy loading way.
     *
     * @param tag current tag.
     * @return root tag.
//     * @throws SQLException when an error occurs in a core.db request.
     * @throws  throws DBException  when an error occurs in a core.db request.
     */
    Tag lazy(Tag tag) throws DBException  /*throws SQLException*/;

    /**
     * Method using for filling the empty tag by full loading way.
     *
     * @param tag current tag.
     * @return root tag.
//     * @throws SQLException when an error occurs in a core.db request.
     * @throws  throws DBException  when an error occurs in a core.db request.
     */
    Tag full(Tag tag) throws DBException  /*throws SQLException*/;

    /**
     * Method using for filling the empty tag by detail loading way.
     *
     * @param tag current tag.
     * @return root tag.
//     * @throws SQLException when an error occurs in a core.db request.
     * @throws  throws DBException  when an error occurs in a core.db request.
     */
    Tag detail(Tag tag) throws DBException /*throws SQLException*/;

    /**
     * Method using for filling the empty tags by full loading way.
     * This List then can be added as a childrenMap list to the Category Tag.
     *
     * @param tag current tag.
     * @return root tag.
//     * @throws SQLException when an error occurs in a core.db request.
     * @throws  throws DBException  when an error occurs in a core.db request.
     */
    List<Tag> fullLoad(Tag tag) throws DBException  /*throws SQLException*/;

    /**
     * Method using for filling the empty tags by lazy loading way.
     * This List then can be added as a childrenMap list to the Category Tag.
     *
     * @param tag current tag.
     * @return root tag.
//     * @throws SQLException when an error occurs in a core.db request.
     * @throws  throws DBException  when an error occurs in a core.db request.
     */
    List<Tag> lazyLoad(Tag tag) throws DBException  /*throws SQLException*/;

    /**
     * Method using for filling the empty tags by detail loading way.
     * This List then can be added as a childrenMap list to the Category Tag.
     *
     * @param tag current tag.
     * @return root tag.
//     * @throws SQLException when an error occurs in a core.db request.
     * @throws  throws DBException  when an error occurs in a core.db request.
     */
    List<Tag> detailLoad(Tag tag) throws DBException  /*throws SQLException*/;

    /**
     * Sets the connection for the loader.
     *
     * @param connection database connection.
     */
    void setConnection(Connection connection);
}
