package core.db.loaders.mysql;

import core.db.constants.CommonConst;
import core.db.constants.mysql.MySQLConst;
import core.db.constants.mysql.Queries;
import core.db.loaders.Loader;
import core.db.loaders.LoaderMarker;
import core.exceptions.DBException;
import org.apache.logging.log4j.LogManager; import org.apache.logging.log4j.Logger;
import core.structure.Tag;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Class using for obtaining the mysql Schema Information from Database.
 */
@LoaderMarker(MySQLConst.SCHEMA)
public class SchemaLoader extends MySqlLoader {
    private static final Logger LOGGER = LogManager.getLogger(SchemaLoader.class);

    /**
     * SchemaLoader class constructor.
     */
    public SchemaLoader() {
        childrenMap = new LinkedHashMap<>();

        childrenMap.put(new TableLoader(), createCategory(MySQLConst.TABLE, MySQLConst.TABLES));
        childrenMap.put(new ViewLoader(), createCategory(MySQLConst.VIEW, MySQLConst.VIEWS));
        childrenMap.put(new ProcedureLoader(), createCategory(MySQLConst.PROCEDURE, MySQLConst.PROCEDURES));
        childrenMap.put(new FunctionLoader(), createCategory(MySQLConst.FUNCTION, MySQLConst.FUNCTIONS));
    }

    public SchemaLoader(Connection connection) {
        this.connection = connection;
    }

    @Override
    public void setConnection(Connection connection) {
        this.connection = connection;
    }


    @Override
    public Tag lazy(Tag tag) throws DBException {
        LOGGER.info("Start SchemaLoader detail...");
        fillAttributes(tag, Queries.FULL_LOAD_SCHEMA, connection);
        for (Map.Entry<Loader, Tag> entry : childrenMap.entrySet()) {
            entry.getValue().setAttribute(CommonConst.DB_NAME, tag.getAttributes().get(CommonConst.DB_NAME));
            tag.addChild(fillChild(entry.getKey(), entry.getValue(), CommonConst.LAZY, connection));
        }
        return tag;
    }

    @Override
    public Tag full(Tag tag) throws DBException {
        LOGGER.info("Start SchemaLoader detail...");
        fillAttributes(tag, Queries.FULL_LOAD_SCHEMA, connection);
        for (Map.Entry<Loader, Tag> entry : childrenMap.entrySet()) {
            entry.getValue().setAttribute(CommonConst.DB_NAME, tag.getAttributes().get(CommonConst.DB_NAME));
            tag.addChild(fillChild(entry.getKey(), entry.getValue(), CommonConst.FULL, connection));
        }
        return tag;
    }

    @Override
    public Tag detail(Tag tag) throws DBException {
        LOGGER.info("Start SchemaLoader detail...");
        fillAttributes(tag, Queries.FULL_LOAD_SCHEMA, connection);
        return tag;
    }

    @Override
    public List<Tag> fullLoad(Tag tag) {
        return null;
    }

    @Override
    public List<Tag> lazyLoad(Tag tag) {
        return null;
    }

    @Override
    public List<Tag> detailLoad(Tag tag) {
        return null;
    }

}
