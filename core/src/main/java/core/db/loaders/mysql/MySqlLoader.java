package core.db.loaders.mysql;

import core.db.constants.CommonConst;
import core.db.loaders.Loader;
import core.exceptions.DBException;
import core.structure.Tag;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.*;
import java.util.*;

/**
 * Abstract Class using for obtaining an Information from  mysql Database.
 */
public abstract class MySqlLoader implements Loader {
    private static final Logger LOGGER = LogManager.getLogger(MySqlLoader.class);

    protected Connection connection;
    protected Map<Loader, Tag> childrenMap;


    /**
     * Method using for filling entity.
     *
     * @param tag      current tag.
     * @param way      way loading.
     * @param query    query string.
     * @param children children of current tag.
     * @throws DBException when an error occurs in a core.db request.
     */
    void fillEntity(Tag tag, String way, String query, Map<Loader, Tag> children) throws DBException {
        String entityName = "";
        if (tag.getAttributeByKey(CommonConst.NAME) != null) {
            entityName = tag.getAttributeByKey(CommonConst.NAME);
        }
        LOGGER.info("Filling entity " + entityName);
        fillAttributes(tag, query, connection);

        for (Map.Entry<Loader, Tag> entry : children.entrySet()) {
            entry.getValue().setAttribute(CommonConst.DB_NAME, tag.getAttributes().get(CommonConst.DB_NAME));
            entry.getValue().setAttribute("TABLE_NAME", entityName);
            Tag tagChild = fillChild(entry.getKey(), entry.getValue(), way, connection);
            tag.addChild(tagChild);
        }
    }

    /**
     * Method using for filling current tag attributes. It uses query to core.db.
     * All information will be recorded to the attributes of current java object.
     *
     * @param tag        current java object.
     * @param query      sql query to DB.
     * @param connection core.db connection.
     * @throws DBException when an error occurs in a core.db request.
     */
    void fillAttributes(Tag tag, String query, Connection connection) throws DBException {
        trimAttributes(tag);
        try (PreparedStatement statement = connection.prepareStatement(query)) {
            int iteration = 0;
            for (Map.Entry<String, String> entry : tag.getAttributes().entrySet()) {
                if (entry.getValue() != null) {
                    statement.setString(++iteration, entry.getValue());
                }
            }
            LOGGER.info("Filling attributes: " + statement);
            try (ResultSet rs = statement.executeQuery()) {
                ResultSetMetaData rsMetaData = rs.getMetaData();
                while (rs.next()) {
                    Map<String, String> map = new HashMap<>();
                    checkNames(rs, rsMetaData, map);
                    tag.setAttributes(map);
                }
            }
        } catch (SQLException e) {
            LOGGER.error(e);
            throw new DBException(e);
        }
    }

    /**
     * Method using for checking names of DB or entities that comes after executing the query.
     *
     * @param rs         ResultSet object.
     * @param rsMetaData ResultSetMetaData object.
     * @param map        attribute map.
     * @throws SQLException when an error occurs in a core.db request.
     */
    private void checkNames(ResultSet rs, ResultSetMetaData rsMetaData, Map<String, String> map) throws SQLException {
        for (int counter = 1; counter <= rsMetaData.getColumnCount(); counter++) {

            String key = rsMetaData.getColumnLabel(counter).toUpperCase();
            String value = rs.getString(counter) == null ? "" : rs.getString(counter);

            map.put(key, value);
        }
    }

    /**
     * Method using for getting an information from core.db.
     *
     * @param connection core.db connection.
     * @param tag        current java object.
     * @param entityName entity value
     * @param query      sql query to DB.
     * @return tags list.
     * @throws DBException when an error occurs in a core.db request.
     */
    List<Tag> getDataFromDB(Connection connection, Tag tag, String entityName, String query) throws DBException {
        LOGGER.info("Getting data for " + tag.getAttributeByKey(CommonConst.NAME));
        List<Tag> children = new ArrayList<>();
        try (PreparedStatement statement = connection.prepareStatement(query)) {
            int iteration = 0;
            for (Map.Entry<String, String> entry : tag.getAttributes().entrySet()) {
                if (!((CommonConst.CHILD_TYPE.equals(entry.getKey())) || (CommonConst.NAME.equals(entry.getKey())))) {
                    statement.setString(++iteration, entry.getValue());
                }
            }

            try (ResultSet rs = statement.executeQuery()) {
                ResultSetMetaData rsMetaData = rs.getMetaData();

                while (rs.next()) {
                    Tag child = new Tag(entityName);
                    Map<String, String> map = new HashMap<>();
                    checkNames(rs, rsMetaData, map);
                    child.setAttributes(map);
                    children.add(child);
                }
            }
        } catch (SQLException e) {
            LOGGER.error(e);
            throw new DBException(e);
        }
        return children;
    }

    private void trimAttributes(Tag tag) {
        LOGGER.info("Trimming attributes for " + tag.getName());
        Map<String, String> attr = new LinkedHashMap<>();
        if ((tag.getName().equals(CommonConst.SCHEMA)) && (tag.getAttributeByKey(CommonConst.DB_NAME) != null)) {
            attr.put(CommonConst.DB_NAME, tag.getAttributeByKey(CommonConst.DB_NAME));
        }
        if (tag.getAttributeByKey(CommonConst.NAME) != null) {
            attr.put(CommonConst.NAME, tag.getAttributeByKey(CommonConst.NAME));
        }
        if (tag.getAttributeByKey(CommonConst.CHILD_TYPE) != null) {
            attr.put(CommonConst.CHILD_TYPE, tag.getAttributeByKey(CommonConst.CHILD_TYPE));
        }
        tag.setAttributes(attr);
    }

    /**
     * Returns a Tag object that can then be added to the parent.
     * The loadType argument must be lazy or full.
     *
     * @param loader     loader.
     * @param tag        child tag.
     * @param loadType   load type.
     * @param connection database connection.
     * @return current tag.
     */
    Tag fillChild(Loader loader, Tag tag, String loadType, Connection connection) throws DBException {
        loader.setConnection(connection);
        switch (loadType) {
            case CommonConst.LAZY:
                for (Tag child : loader.lazyLoad(tag)) {
                    tag.addChild(child);
                }
                break;
            case CommonConst.FULL:
                for (Tag child : loader.fullLoad(tag)) {
                    tag.addChild(child);
                }
                break;
            default:
                throw new DBException("Invalid load typa.");
        }

        return tag;
    }

    /**
     * Returns a Tag object that can then be used as a category.
     *
     * @param childType childrenMap type of current category.
     * @param name      category value.
     * @return category tag with attributes.
     */
    Tag createCategory(String childType, String name) {
        Tag tag = new Tag(CommonConst.CATEGORY);
        tag.setAttribute(CommonConst.NAME, name);
        tag.setAttribute(CommonConst.CHILD_TYPE, childType);
        return tag;
    }
}
