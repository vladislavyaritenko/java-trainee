package core.db.loaders.mysql;

import core.db.constants.CommonConst;
import core.db.constants.mysql.MySQLConst;
import core.db.constants.mysql.Queries;
import core.db.loaders.LoaderMarker;
import core.exceptions.DBException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import core.structure.Tag;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.LinkedHashMap;
import java.util.List;

/**
 * Class using for obtaining the mysql Views Information from Database.
 */
@LoaderMarker(MySQLConst.VIEW)
public class ViewLoader extends MySqlLoader {
    private static final Logger LOGGER = LogManager.getLogger(ViewLoader.class);

    public ViewLoader(Connection connection) {
        this.connection = connection;
    }

    public ViewLoader() {
        setChildren();
    }

    private void setChildren() {
        childrenMap = new LinkedHashMap<>();
        childrenMap.put(new ColumnLoader(), createCategory(MySQLConst.COLUMN, MySQLConst.COLUMNS));
    }

    @Override
    public void setConnection(Connection connection) {
        this.connection = connection;
    }

    @Override
    public Tag lazy(Tag tag) throws DBException {
        LOGGER.info("Start ViewLoader full...");
        if (CommonConst.CATEGORY.equals(tag.getName())) {
            for (Tag t : lazyLoad(tag)) {
                tag.addChild(t);
            }
        } else {
            fillEntity(tag, CommonConst.LAZY, Queries.DETAIL_VIEW_INFO, childrenMap);
        }
        return tag;
    }

    @Override
    public Tag full(Tag tag) throws DBException {
        LOGGER.info("Start ViewLoader full...");
        if (CommonConst.CATEGORY.equals(tag.getName())) {
            for (Tag t : fullLoad(tag)) {
                tag.addChild(t);
            }
        } else {
            setChildren();
            fillEntity(tag, CommonConst.FULL, Queries.DETAIL_VIEW_INFO, childrenMap);
        }
        return tag;
    }

    @Override
    public Tag detail(Tag tag) throws DBException {
        LOGGER.info("Start ViewLoader full...");
        if (CommonConst.CATEGORY.equals(tag.getName())) {
            for (Tag t : detailLoad(tag)) {
                tag.addChild(t);
            }
        } else {
            fillAttributes(tag, Queries.DETAIL_VIEW_INFO, connection);
        }
        return tag;
    }

    @Override
    public List<Tag> lazyLoad(Tag tag) throws DBException {
        return getDataFromDB(connection, tag, MySQLConst.VIEW, Queries.VIEW_NAME);
    }

    @Override
    public List<Tag> fullLoad(Tag tag) throws DBException {
        List<Tag> tables = getDataFromDB(connection, tag, MySQLConst.VIEW, Queries.ALL_VIEW_INFO);
        for (Tag table : tables) {
            full(table);
        }
        return tables;
    }

    public List<Tag> detailLoad(Tag tag) throws DBException {
        return getDataFromDB(connection, tag, MySQLConst.VIEW, Queries.ALL_VIEW_INFO);
    }
}
