package core.db.loaders.mysql;

import core.db.constants.CommonConst;
import core.db.constants.mysql.MySQLConst;
import core.db.constants.mysql.Queries;
import core.db.loaders.LoaderMarker;
import core.exceptions.DBException;
import core.structure.Tag;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.LinkedHashMap;
import java.util.List;

/**
 * Class using for obtaining the mysql Information about functions from Database.
 */
@LoaderMarker(value = MySQLConst.FUNCTION)
public class FunctionLoader extends MySqlLoader {
    private static final Logger LOGGER = LogManager.getLogger(FunctionLoader.class);

    public FunctionLoader(Connection connection) {
        this.connection = connection;
    }

    public FunctionLoader() {
        setChildren();
    }

    private void setChildren() {
        childrenMap = new LinkedHashMap<>();
        childrenMap.put(new ParamsLoader(), createCategory(MySQLConst.PARAMETER, MySQLConst.PARAMETERS));
    }

    @Override
    public void setConnection(Connection connection) {
        this.connection = connection;
    }

    @Override
    public Tag lazy(Tag tag) throws DBException {
        LOGGER.info("Start FunctionLoader lazy...");
        if (CommonConst.CATEGORY.equals(tag.getName())) {
            for (Tag t : lazyLoad(tag)) {
                tag.addChild(t);
            }
        } else {
            setChildren();
            fillEntity(tag, CommonConst.LAZY, Queries.DETAIL_FUNCTION_INFO, childrenMap);
        }
        return tag;
    }

    @Override
    public Tag full(Tag tag) throws DBException {
        LOGGER.info("Start FunctionLoader lazy...");
        if (CommonConst.CATEGORY.equals(tag.getName())) {
            for (Tag t : fullLoad(tag)) {
                tag.addChild(t);
            }
        } else {
            setChildren();
            fillEntity(tag, CommonConst.FULL, Queries.DETAIL_FUNCTION_INFO, childrenMap);
        }
        return tag;
    }

    @Override
    public Tag detail(Tag tag) throws DBException {
        LOGGER.info("Start FunctionLoader lazy...");
        if (CommonConst.CATEGORY.equals(tag.getName())) {
            for (Tag t : detailLoad(tag)) {
                tag.addChild(t);
            }
        } else {
            fillAttributes(tag, Queries.DETAIL_FUNCTION_INFO, connection);
        }
        return tag;
    }

    @Override
    public List<Tag> detailLoad(Tag tag) throws DBException {
        return getDataFromDB(connection, tag, MySQLConst.FUNCTION, Queries.ALL_FUNCTION_INFO);
    }

    @Override
    public List<Tag> lazyLoad(Tag tag) throws DBException {
        return getDataFromDB(connection, tag, MySQLConst.FUNCTION, Queries.FUNCTION_NAME);
    }

    @Override
    public List<Tag> fullLoad(Tag tag) throws DBException {
        List<Tag> functions = getDataFromDB(connection, tag, MySQLConst.FUNCTION, Queries.ALL_FUNCTION_INFO);
        for (Tag function : functions) {
            full(function);
        }
        return functions;
    }
}
