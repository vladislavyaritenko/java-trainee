package core.db.loaders.mysql;

import core.db.constants.CommonConst;
import core.db.constants.mysql.MySQLConst;
import core.db.constants.mysql.Queries;
import core.db.loaders.LoaderMarker;
import core.exceptions.DBException;
import core.structure.Tag;
import org.apache.logging.log4j.LogManager; import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.LinkedHashMap;
import java.util.List;

/**
 * Class using for obtaining the mysql Triggers Information from Database.
 */
@LoaderMarker(MySQLConst.TRIGGER)
public class TriggerLoader extends MySqlLoader {
    private static final Logger LOGGER = LogManager.getLogger(TriggerLoader.class);

    public TriggerLoader() {
    }

    public TriggerLoader(Connection connection) {
        this.connection = connection;
    }

    @Override
    public void setConnection(Connection connection) {
        this.connection = connection;
    }

    private void setChildren() {
        childrenMap = new LinkedHashMap<>();
    }

    @Override
    public Tag lazy(Tag tag) throws DBException {
        LOGGER.info("Start TriggerLoader lazy...");
        if (CommonConst.CATEGORY.equals(tag.getName())) {
            for (Tag t : lazyLoad(tag)) {
                tag.addChild(t);
            }
        } else {
            setChildren();
            fillEntity(tag, CommonConst.LAZY, Queries.DETAIL_TRIGGER_INFO, childrenMap);
        }
        return tag;
    }

    @Override
    public Tag full(Tag tag) throws DBException {
        LOGGER.info("Start TriggerLoader full...");
        if (CommonConst.CATEGORY.equals(tag.getName())) {
            for (Tag t : fullLoad(tag)) {
                tag.addChild(t);
            }
        } else {
            setChildren();
            fillEntity(tag, CommonConst.FULL, Queries.DETAIL_TRIGGER_INFO, childrenMap);
        }
        return tag;
    }

    @Override
    public Tag detail(Tag tag) throws DBException {
        LOGGER.info("Start TriggerLoader detail...");
        if (CommonConst.CATEGORY.equals(tag.getName())) {
            for (Tag t : detailLoad(tag)) {
                tag.addChild(t);
            }
        } else {
            fillAttributes(tag, Queries.DETAIL_TRIGGER_INFO, connection);
        }
        return tag;
    }

    @Override
    public List<Tag> fullLoad(Tag tag) throws DBException {
        List<Tag> triggers = getDataFromDB(connection, tag, MySQLConst.TRIGGER, Queries.ALL_TRIGGERS_INFO);
        for (Tag trigger : triggers) {
            full(trigger);
        }
        return triggers;
    }

    @Override
    public List<Tag> lazyLoad(Tag tag) throws DBException {
        return getDataFromDB(connection, tag, MySQLConst.TRIGGER, Queries.TRIGGERS_LAZY_LOAD);
    }

    @Override
    public List<Tag> detailLoad(Tag tag) throws DBException {
        return getDataFromDB(connection, tag, MySQLConst.TRIGGER, Queries.ALL_TRIGGERS_INFO);
    }
}
