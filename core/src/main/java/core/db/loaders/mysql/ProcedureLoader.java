package core.db.loaders.mysql;

import core.db.constants.CommonConst;
import core.db.constants.mysql.MySQLConst;
import core.db.constants.mysql.Queries;
import core.db.loaders.LoaderMarker;
import core.exceptions.DBException;
import core.structure.Tag;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.LinkedHashMap;
import java.util.List;

/**
 * Class using for obtaining the mysql Procedures Information from Database.
 */
@LoaderMarker(MySQLConst.PROCEDURE)
public class ProcedureLoader extends MySqlLoader {
    private static final Logger LOGGER = LogManager.getLogger(ProcedureLoader.class);

    public ProcedureLoader(Connection connection) {
        this.connection = connection;
    }

    private void setChildren() {
        childrenMap = new LinkedHashMap<>();
        childrenMap.put(new ParamsLoader(), createCategory(MySQLConst.PARAMETER, MySQLConst.PARAMETERS));
    }

    public ProcedureLoader() {
        setChildren();

    }

    @Override
    public void setConnection(Connection connection) {
        this.connection = connection;
    }

    @Override
    public Tag lazy(Tag tag) throws DBException {
        LOGGER.info("Start ProcedureLoader lazy...");
        if (CommonConst.CATEGORY.equals(tag.getName())) {
            for (Tag t : lazyLoad(tag)) {
                tag.addChild(t);
            }
        } else {
            fillEntity(tag, CommonConst.LAZY, Queries.DETAIL_PROCEDURE_INFO, childrenMap);
        }
        return tag;
    }

    @Override
    public Tag full(Tag tag) throws DBException {
        LOGGER.info("Start ProcedureLoader lazy...");
        if (CommonConst.CATEGORY.equals(tag.getName())) {
            for (Tag t : fullLoad(tag)) {
                tag.addChild(t);
            }
        } else {
            setChildren();
            fillEntity(tag, CommonConst.FULL, Queries.DETAIL_PROCEDURE_INFO, childrenMap);
        }
        return tag;
    }


    @Override
    public Tag detail(Tag tag) throws DBException {
        LOGGER.info("Start ProcedureLoader lazy...");
        if (CommonConst.CATEGORY.equals(tag.getName())) {
            for (Tag t : detailLoad(tag)) {
                tag.addChild(t);
            }
        } else {
            fillAttributes(tag, Queries.DETAIL_PROCEDURE_INFO, connection);
        }
        return tag;
    }

    @Override
    public List<Tag> detailLoad(Tag tag) throws DBException {
        return getDataFromDB(connection, tag, MySQLConst.PROCEDURE, Queries.ALL_PROCEDURE_INFO);
    }

    @Override
    public List<Tag> lazyLoad(Tag tag) throws DBException {
        return getDataFromDB(connection, tag, MySQLConst.PROCEDURE, Queries.PROCEDURE_NAME);
    }

    @Override
    public List<Tag> fullLoad(Tag tag) throws DBException {
        List<Tag> procedures = getDataFromDB(connection, tag, MySQLConst.PROCEDURE, Queries.ALL_PROCEDURE_INFO);
        for (Tag procedure : procedures) {
            full(procedure);
        }
        return procedures;
    }

}
