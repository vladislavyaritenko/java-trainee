package core.db.loaders.mysql;

import core.db.constants.mysql.MySQLConst;
import core.db.constants.mysql.Queries;
import core.db.loaders.LoaderMarker;
import core.exceptions.DBException;
import core.structure.Tag;

import java.sql.Connection;
import java.util.List;

/**
 * Class using for obtaining the mysql Primary keys Information from Database.
 */
@LoaderMarker(MySQLConst.PRIMARY_KEY)
public class PrimaryKeyLoader extends MySqlLoader {
    public PrimaryKeyLoader() {
    }

    public PrimaryKeyLoader(Connection connection) {
        this.connection = connection;
    }

    @Override
    public void setConnection(Connection connection) {
        this.connection = connection;
    }

    @Override
    public Tag lazy(Tag tag) {
        return null;
    }

    @Override
    public Tag full(Tag tag) {
        return null;
    }

    @Override
    public Tag detail(Tag tag) {
        return null;
    }

    @Override
    public List<Tag> fullLoad(Tag tag) throws DBException {
        return getDataFromDB(connection, tag, MySQLConst.PRIMARY_KEY, Queries.ALL_PRIMARY_KEY_INFO);
    }

    @Override
    public List<Tag> lazyLoad(Tag tag) throws DBException {
        return getDataFromDB(connection, tag, MySQLConst.PRIMARY_KEY, Queries.PRIMARY_KEY_LAZY_LOAD);

    }

    @Override
    public List<Tag> detailLoad(Tag tag) {
        return null;
    }
}
