package core.db.loaders.mysql;

import core.db.constants.CommonConst;
import core.db.constants.mysql.MySQLConst;
import core.db.constants.mysql.Queries;
import core.db.loaders.LoaderMarker;
import core.exceptions.DBException;
import core.structure.Tag;
import org.apache.logging.log4j.LogManager; import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.LinkedHashMap;
import java.util.List;

/**
 * Class using for obtaining the mysql Information about indexes from Database.
 */
@LoaderMarker(value = MySQLConst.INDEX)
public class IndexLoader extends MySqlLoader {
    private static final Logger LOGGER = LogManager.getLogger(IndexLoader.class);

    public IndexLoader() {
    }

    public IndexLoader(Connection connection) {
        this.connection = connection;
    }

    private void setChildren() {
        childrenMap = new LinkedHashMap<>();
    }

    @Override
    public Tag lazy(Tag tag) throws DBException {
        LOGGER.info("Start IndexLoader lazy...");
        if (CommonConst.CATEGORY.equals(tag.getName())) {
            for (Tag t : lazyLoad(tag)) {
                tag.addChild(t);
            }
        } else {
            setChildren();
            fillEntity(tag, CommonConst.LAZY, Queries.DETAIL_INDEX_INFO, childrenMap);
        }
        return tag;
    }

    @Override
    public Tag full(Tag tag) throws DBException {
        LOGGER.info("Start IndexLoader full...");
        if (CommonConst.CATEGORY.equals(tag.getName())) {
            for (Tag t : fullLoad(tag)) {
                tag.addChild(t);
            }
        } else {
            setChildren();
            fillEntity(tag, CommonConst.FULL, Queries.DETAIL_INDEX_INFO, childrenMap);
        }
        return tag;
    }

    @Override
    public Tag detail(Tag tag) throws DBException {
        LOGGER.info("Start IndexLoader detail...");
        if (CommonConst.CATEGORY.equals(tag.getName())) {
            for (Tag t : detailLoad(tag)) {
                tag.addChild(t);
            }
        } else {
            fillAttributes(tag, Queries.DETAIL_INDEX_INFO, connection);
        }
        return tag;
    }

    @Override
    public void setConnection(Connection connection) {
        this.connection = connection;
    }

    @Override
    public List<Tag> fullLoad(Tag tag) throws DBException {
        List<Tag> indexes = getDataFromDB(connection, tag, MySQLConst.INDEX, Queries.ALL_INDEX_INFO);
        for (Tag index : indexes) {
            full(index);
        }
        return indexes;
    }

    @Override
    public List<Tag> lazyLoad(Tag tag) throws DBException {
        return getDataFromDB(connection, tag, MySQLConst.INDEX, Queries.INDEX_LAZY_LOAD);
    }

    @Override
    public List<Tag> detailLoad(Tag tag) throws DBException {
        return getDataFromDB(connection, tag, MySQLConst.INDEX, Queries.ALL_INDEX_INFO);
    }
}
