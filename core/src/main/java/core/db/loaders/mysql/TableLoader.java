package core.db.loaders.mysql;

import core.db.constants.CommonConst;
import core.db.constants.mysql.MySQLConst;
import core.db.constants.mysql.Queries;
import core.db.loaders.LoaderMarker;
import core.exceptions.DBException;
import core.structure.Tag;
import org.apache.logging.log4j.LogManager; import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.LinkedHashMap;
import java.util.List;

/**
 * Class using for obtaining the mysql Tables Information from Database.
 */
@LoaderMarker(MySQLConst.TABLE)
public class TableLoader extends MySqlLoader {
    private static final Logger LOGGER = LogManager.getLogger(TableLoader.class);

    /**
     * TableLoader class constructor.
     *
     * @param connection database connection.
     */
    public TableLoader(Connection connection) {
        setConnection(connection);
    }

    public TableLoader() {
    }

    @Override
    public void setConnection(Connection connection) {
        this.connection = connection;
    }

    private void setChildren() {
        childrenMap = new LinkedHashMap<>();
        childrenMap.put(new ColumnLoader(), createCategory(MySQLConst.COLUMN, MySQLConst.COLUMNS));
        childrenMap.put(new IndexLoader(), createCategory(MySQLConst.INDEX, MySQLConst.INDEXES));
        childrenMap.put(new ForeignKeyLoader(), createCategory(MySQLConst.FOREIGN_KEY, MySQLConst.FOREIGN_KEYS));
        childrenMap.put(new PrimaryKeyLoader(), createCategory(MySQLConst.PRIMARY_KEY, MySQLConst.PRIMARY_KEYS));
        childrenMap.put(new TriggerLoader(), createCategory(MySQLConst.TRIGGER, MySQLConst.TRIGGERS));
    }

    @Override
    public Tag lazy(Tag tag) throws DBException {
        LOGGER.info("Start TableLoader lazy...");
        if (CommonConst.CATEGORY.equals(tag.getName())) {
            for (Tag t : lazyLoad(tag)) {
                tag.addChild(t);
            }
        } else {
            setChildren();
            fillEntity(tag, CommonConst.LAZY, Queries.DETAIL_TABLE_INFO, childrenMap);
        }
        return tag;
    }

    @Override
    public Tag full(Tag tag) throws DBException {
        LOGGER.info("Start TableLoader full...");
        if (CommonConst.CATEGORY.equals(tag.getName())) {
            for (Tag t : fullLoad(tag)) {
                tag.addChild(t);
            }
        } else {
            setChildren();
            fillEntity(tag, CommonConst.FULL, Queries.DETAIL_TABLE_INFO, childrenMap);
        }
        return tag;
    }

    @Override
    public Tag detail(Tag tag) throws DBException {
        LOGGER.info("Start TableLoader detail...");
        if (CommonConst.CATEGORY.equals(tag.getName())) {
            for (Tag t : detailLoad(tag)) {
                tag.addChild(t);
            }
        } else {
            fillAttributes(tag, Queries.DETAIL_TABLE_INFO, connection);
        }
        return tag;
    }

    @Override
    public List<Tag> detailLoad(Tag tag) throws DBException {
        return getDataFromDB(connection, tag, MySQLConst.TABLE, Queries.ALL_TABLE_INFO);
    }

    @Override
    public List<Tag> lazyLoad(Tag tag) throws DBException {
        return getDataFromDB(connection, tag, MySQLConst.TABLE, Queries.TABLE_NAME);
    }

    @Override
    public List<Tag> fullLoad(Tag tag) throws DBException {
        List<Tag> tables = getDataFromDB(connection, tag, MySQLConst.TABLE, Queries.ALL_TABLE_INFO);
        for (Tag table : tables) {
            full(table);
        }
        return tables;
    }
}
