package core.db.loaders;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Annotation for Loaders.
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface LoaderMarker {
    /**
     * Method using for setting the value of the database type.
     *
     * @return database type value.
     */
    String value();
}
