package core.db.loaders;

import core.db.ReflectionUtil;
import core.db.constants.CommonConst;
import core.db.constants.DBType;
import core.exceptions.DBException;
import core.exceptions.ReflectionException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import core.structure.Tag;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

/**
 * Class using for managing all loaders.
 */
public class LoaderManager {
    private static final Logger LOGGER = LogManager.getLogger(LoaderManager.class);

    private Map<String, Loader> loadersMap;
    private ReflectionUtil reflection;
    private Connection connection;

    /**
     * LoaderManager class constructor.
     *
     * @param connection database connection.
     * @param dbType     database type.
     * @throws ReflectionException
     */
    public LoaderManager(Connection connection, DBType dbType) throws ReflectionException {
        this.connection = connection;
        reflection = new ReflectionUtil(dbType, LoaderMarker.class);
        loadersMap = new HashMap<>();
        getLoadersMap();
    }

    /**
     * Method using for filling the empty tag by lazy loading way.
     *
     * @param tag empty entity.
     * @return filled tag.
     * @throws DBException when an error occurs in a core.db request.
     */
    public Tag lazyLoad(Tag tag) throws DBException {
        LOGGER.info("Lazy loading...");
        Loader loader = loadersMap.get(identifyLoader(tag));
        loader.setConnection(connection);
        loader.lazy(tag);
        return tag;
    }

    /**
     * Method using for filling the empty tag by full loading way.
     *
     * @param tag empty entity.
     * @return filled tag.
     * @throws DBException when an error occurs in a core.db request.
     */
    public Tag fullLoad(Tag tag) throws DBException {
        LOGGER.info("Full loading...");
        Loader loader = loadersMap.get(identifyLoader(tag));
        loader.setConnection(connection);
        loader.full(tag);
        return tag;
    }

    /**
     * Method using for filling the empty tag by detail loading way.
     *
     * @param tag empty entity.
     * @return filled tag.
     * @throws DBException when an error occurs in a core.db request.
     */
    public Tag detailLoad(Tag tag) throws DBException {
        LOGGER.info("Detail loading...");
        Loader loader = loadersMap.get(identifyLoader(tag));
        loader.setConnection(connection);
        loader.detail(tag);
        return tag;
    }

    /**
     * Method using for identifying the required loader.
     *
     * @param tag entity.
     * @return loader's value.
     */
    private String identifyLoader(Tag tag) {
        LOGGER.info("Identifying the loader...");
        if (tag.getName().equals(CommonConst.CATEGORY)) {
            return tag.getAttributeByKey(CommonConst.CHILD_TYPE);
        }
        return tag.getName().toUpperCase();
    }

    /**
     * Method using for obtaining loaders map.
     *
     * @return loaders map.
     */
    public Map<String, Loader> getLoadersMap() {
        LOGGER.info("Get the map of loaders from reflection util...");
        for (Map.Entry<String, Object> entry : reflection.getMap().entrySet()) {
            loadersMap.put(entry.getKey(), (Loader) entry.getValue());
        }
        return loadersMap;
    }

    /**
     * Method using for obtaining database connection.
     *
     * @return database connection.
     */
    public Connection getConnection() {
        return connection;
    }

    /**
     * Method using for establishing database connection.
     */
    public void setConnection(Connection connection) {
        this.connection = connection;
    }
}
