package core.service.parsing;


import core.exceptions.ParserException;
import core.structure.Tag;
import org.apache.logging.log4j.LogManager; import org.apache.logging.log4j.Logger;

/**
 * Class using for setting end executing parsing strategy.
 */
public class ParsingClient {
    private static final Logger LOGGER = LogManager.getLogger(ParsingClient.class);
    private ParsingStrategy strategy;

    public ParsingStrategy getStrategy() {
        return strategy;
    }

    /**
     * Method using for setting strategy to parsing process.
     *
     * @param strategy selected strategy.
     */
    public void setStrategy(ParsingStrategy strategy) {
        this.strategy = strategy;
        LOGGER.info("Parsing strategy established.");
    }

    /**
     * Method using for executing parsing process strategy.
     *
     * @param nameFile file value.
     * @return node/root tag.
     * @throws ParserException if an error occurred while parsing the file.
     */
    public Tag executeStrategy(String nameFile) throws ParserException {
        LOGGER.info("Parsing strategy executing.");
        return strategy.parse(nameFile);
    }
}
