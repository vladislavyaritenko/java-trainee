package core.service.parsing;


import core.exceptions.ParserException;
import core.structure.Tag;

/**
 * Interface using for parsing file to java objects.
 */
public interface ParsingStrategy {


    /**
     * Method using for parsing file to java objects.
     *
     * @param nameFile file value.
     * @return node/root tag.
     * @throws ParserException if an error occurred while parsing the file.
     */
    Tag parse(String nameFile) throws ParserException;
}
