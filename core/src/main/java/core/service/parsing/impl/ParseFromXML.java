package core.service.parsing.impl;

import core.service.parsing.ParsingStrategy;
import core.exceptions.ParserException;
import core.structure.Tag;
import org.apache.logging.log4j.LogManager; import org.apache.logging.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Class using for parsing xml file to java objects.
 */
public class ParseFromXML implements ParsingStrategy {
    private static final Logger LOGGER = LogManager.getLogger(ParseFromXML.class);

    /**
     * Method using to parse input file.
     *
     * @param inputFile input file value.
     * @return root tag.
     */
    public Tag parse(String inputFile) throws ParserException {
        LOGGER.info("Start parsing XML...");
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder;
        Document document;
        Tag result;

        try {
            builder = factory.newDocumentBuilder();
            document = builder.parse(inputFile);
            Element element = document.getDocumentElement();
            result = buildTag(element);
        } catch (ParserConfigurationException | SAXException | IOException e) {
            LOGGER.error(e);
            throw new ParserException(e);
        }
        return result;
    }

    /**
     * Method using to build tree core.structure.
     *
     * @param element current XML element.
     * @return current tag.
     */
    private Tag buildTag(Element element) {
        Tag result = new Tag(element.getTagName());
        if (element.hasAttributes()) {
            result.setAttributes(getAttributes(element));
        }
        if (hasTextContent(element)) {
            result.setContent(element.getTextContent().trim());
        }
        if (hasCdata(element)) {
            result.setContent(element.getTextContent().trim());
        }
        if (element.hasChildNodes()) {
            NodeList children = element.getChildNodes();
            for (int i = 0; i < children.getLength(); i++) {
                if (children.item(i) instanceof Element) {
                    Element child = (Element) children.item(i);
                    Tag childTag = buildTag(child);
                    result.addChild(childTag);
                }
            }
        }
        return result;
    }

    /**
     * Method using to check text content inside the element.
     *
     * @param element current element.
     * @return result of checking.
     */
    private boolean hasTextContent(Element element) {
        NodeList nl = element.getChildNodes();
        for (int i = 0; i < nl.getLength(); i++) {
            Node node = nl.item(i);
            if (node.getNodeType() == Node.TEXT_NODE) {
                String temp = node.getNodeValue().trim();
                if (!temp.equals("")) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Method using to check CDATA inside the element.
     *
     * @param element current element.
     * @return result of checking.
     */
    private boolean hasCdata(Element element) {
        NodeList nl = element.getChildNodes();
        for (int i = 0; i < nl.getLength(); i++) {
            Node node = nl.item(i);
            if (node.getNodeType() == Node.CDATA_SECTION_NODE) {
                return true;
            }
        }
        return false;
    }

    /**
     * Method using to getValue attributes from the Node.
     *
     * @param child current node.
     * @return Map of attributes.
     */
    private Map<String, String> getAttributes(Node child) {
        NamedNodeMap tempAttrs = child.getAttributes();
        Map<String, String> attributes = new HashMap<>();
        for (int j = 0; j < tempAttrs.getLength(); j++) {
            attributes.put(tempAttrs.item(j).getNodeName(), tempAttrs.item(j).getNodeValue());
        }
        return attributes;
    }
}
