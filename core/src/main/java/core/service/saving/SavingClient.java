package core.service.saving;

import core.constants.TypeFileConst;
import core.exceptions.ParserException;
import core.exceptions.SetStrategyException;
import core.service.saving.impl.SaveToXML;
import core.structure.Tag;
import org.apache.logging.log4j.LogManager; import org.apache.logging.log4j.Logger;

/**
 * Class using for setting end executing saving strategy.
 */
public class SavingClient {
    private static final Logger LOGGER = LogManager.getLogger(SavingClient.class);
    private SavingStrategy strategy;

    public SavingStrategy getStrategy() {
        return strategy;
    }

    /**
     * Method using for setting parsing process strategy.
     *
     * @param typeFile type file for output file.
     * @throws SetStrategyException if an error occurred while choosing and setting a strategy.
     */
    public void setStrategy(TypeFileConst typeFile) throws SetStrategyException {
        switch (typeFile) {
            case XML:
                LOGGER.info("Chosen xml type for output file.");
                SaveToXML save = new SaveToXML();
                this.strategy = save;
                break;
            default:
                LOGGER.error("Incorrect file type.");
                throw new SetStrategyException("Incorrect file type.");
        }
        LOGGER.info("Saving strategy established.");
    }

    /**
     * Method using for executing saving process strategy.
     *
     * @param tag        root tag.
     * @param outputFile file value.
     */
    public void executeStrategy(Tag tag, String outputFile) throws ParserException {
        strategy.save(tag, outputFile);
        LOGGER.info("Saving strategy executed.");
    }
}
