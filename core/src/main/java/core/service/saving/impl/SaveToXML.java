package core.service.saving.impl;

import core.service.saving.SavingStrategy;
import core.exceptions.ParserException;
import core.structure.Tag;
import org.apache.logging.log4j.LogManager; import org.apache.logging.log4j.Logger;
import org.w3c.dom.Attr;
import org.w3c.dom.CDATASection;
import org.w3c.dom.DOMImplementation;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.ls.DOMImplementationLS;
import org.w3c.dom.ls.LSOutput;
import org.w3c.dom.ls.LSSerializer;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.InvalidPathException;
import java.nio.file.Paths;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Class using for saving java objects to xml file.
 */
public class SaveToXML implements SavingStrategy {
    private static final Logger LOGGER = LogManager.getLogger(SaveToXML.class);

    /**
     * Method using to save output file.
     *
     * @param tag        root tag.
     * @param outputFile file value.
     */
    @Override
    public void save(Tag tag, String outputFile) throws ParserException {
        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document document = builder.newDocument();

            Element root = buildElement(document, tag);
            document.appendChild(root);

            saveFile(document, outputFile);
        } catch (NullPointerException | ParserConfigurationException e) {
            LOGGER.error(e);
            throw new ParserException(e);
        }
    }

    /**
     * Method using to build element core.structure.
     *
     * @param document current document.
     * @param tag      root tag.
     * @return root xml element.
     */
    private Element buildElement(Document document, Tag tag) {
        Element element = document.createElement(tag.getName());

        if (!tag.getAttributes().isEmpty()) {
            Map<String, String> attributes = tag.getAttributes();
            for (Map.Entry<String, String> entry : attributes.entrySet()) {
                Attr attr = document.createAttribute(entry.getKey());
                attr.setValue(entry.getValue());
                element.setAttributeNode(attr);
            }
        }
        if (!tag.getChildrenList().isEmpty()) {
            List<Tag> children = tag.getChildrenList();
            for (Tag child : children) {
                Element el = buildElement(document, child);
                element.appendChild(el);
            }
        }
        if (!tag.getContent().isEmpty()) {
            if (hasCdata(tag.getContent())) {
                CDATASection cdataSection = document.createCDATASection(tag.getContent());
                element.appendChild(cdataSection);
            } else {
                element.setTextContent(tag.getContent());
            }
        }
        return element;
    }

    /**
     * Method using for saving document to file.
     *
     * @param document current document.
     * @param nameFile file value.
     */
    private void saveFile(Document document, String nameFile) throws ParserException {
        try {
            DOMImplementation impl = document.getImplementation();
            DOMImplementationLS implLS = (DOMImplementationLS) impl.getFeature("LS", "3.0");
            LSSerializer ser = implLS.createLSSerializer();
            ser.getDomConfig().setParameter("format-pretty-print", true);

            LSOutput out = implLS.createLSOutput();
//            System.out.println(ser.writeToString(document)); //output to Console
            out.setByteStream(Files.newOutputStream(Paths.get(nameFile)));
            out.setEncoding("UTF-8");
            ser.write(document, out);

            LOGGER.info("XML file saved");
        } catch (IOException | InvalidPathException e) {
            LOGGER.error(e);
            throw new ParserException(e);
        }
    }

    /**
     * Method using to check CDATA inside the element.
     *
     * @param data current console.context.
     * @return result of checking.
     */
    private boolean hasCdata(String data) {
        Pattern pattern = Pattern.compile("<+|>+|&+|'+|\"+");
        Matcher matcher = pattern.matcher(data);

        return matcher.find();
    }
}
