package core.service.saving;

import core.exceptions.ParserException;
import core.structure.Tag;

/**
 * Interface using for saving objects to file.
 */
public interface SavingStrategy {
    /**
     * Method using for saving java object to output file.
     *
     * @param tag      root tag.
     * @param nameFile file value.
     * @throws ParserException if an error occurred while parsing the file.
     */
    void save(Tag tag, String nameFile) throws ParserException;
}

