package core.constants;

/**
 * Command set for console package.
 */
public enum TypeFileConst {
    XML("xml"), TXT("txt");

    private String typeFile;

    TypeFileConst(String typeFile) {
        this.typeFile = typeFile;
    }

    public String getTypeFile() {
        return typeFile;
    }
}
