package core.structure;

import core.exceptions.ChildrenListException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Class using as a container for childrenMap tags.
 */
public class ChildrenList extends ArrayList<Tag> {
    private static final Logger LOGGER = LogManager.getLogger(ChildrenList.class);
    private Tag parent;

    public ChildrenList(Tag parent) {
        this.parent = parent;
    }

    /**
     * Method using for adding child to Children List.
     *
     * @param tag child tag.
     * @return true if child tag was added.
     * @throws ChildrenListException if error adding to the list of childrenMap.
     */
    public boolean add(Tag tag) {
        if (tag != null) {
            if (tag.getParent() == null) {
                tag.setParent(parent);
                return super.add(tag);
            }
        }
        LOGGER.error("Error adding to the list of childrenMap.");
        throw new ChildrenListException("Error adding to the list of childrenMap.");
    }


    /**
     * Method using for setting child to Children List.
     *
     * @param tag child tag.
     * @return child tag that was setting.
     * @throws ChildrenListException if error setting to the list of childrenMap.
     */
    public Tag set(int index, Tag tag) {
        if (tag != null) {
            tag.setParent(parent);
            return super.set(index, tag);
        }
        LOGGER.error("Error setting to the list of childrenMap.");
        throw new ChildrenListException("Error setting to the list of childrenMap.");
    }
}

