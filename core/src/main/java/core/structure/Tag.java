package core.structure;

import core.db.constants.CommonConst;
import java.util.*;
import java.util.function.Predicate;

/**
 * Using for storing tags(nodes) from file as java object.
 */
public class Tag {
    private static final String EOL = System.lineSeparator();
    private Tag parent;
    private String name;
    private Map<String, String> attributes;
    private List<Tag> childrenList;
    private String content;

    /**
     * Tag class constructor.
     *
     * @param name tag value.
     */
    public Tag(String name) {
        this.name = name;
        attributes = new LinkedHashMap<>();
        childrenList = new ChildrenList(this);
    }

    Tag getParent() {
        return parent;
    }

    void setParent(Tag parent) {
        this.parent = parent;
    }

    public String getName() {
        return name;
    }

    public String getContent() {
        return content == null ? "" : content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Map<String, String> getAttributes() {
        return attributes;
    }

    public void setAttributes(Map<String, String> attributes) {
        this.attributes = attributes;
    }

    public List<Tag> getChildrenList() {
        return childrenList;
    }

    public String getAttributeByKey(String key) {
        return attributes.get(key);
    }

    public void setAttribute(String key, String value) {
        this.attributes.put(key, value);
    }

    /**
     * Method using for adding children list to current tag.
     *
     * @param child tag child.
     */
    public void addChild(Tag child) {
        int index = -1;
        for (Tag curTag : this.childrenList) {
            if(curTag.getAttributeByKey(CommonConst.NAME) != null) {
                if (curTag.getAttributeByKey(CommonConst.NAME).equals(child.getAttributeByKey(CommonConst.NAME))) {
                    index = this.childrenList.indexOf(curTag);
                }
            }
        }

        if (index == -1) {
            this.childrenList.add(child);
        } else {
            this.childrenList.set(index, child);
        }

    }

    /**
     * Method using for removing children list from current tag.
     *
     * @return current tag.
     */
    public Tag removeChildren() {
        if (!childrenList.isEmpty()) {
            this.parent = null;
            this.childrenList.removeAll(this.childrenList);
        }
        return this;
    }

    /**
     * Method for depth search.
     *
     * @param tagPredicate search predicate.
     * @param tag          root tag.
     * @param results      list of search results.
     * @return list of search results.
     */
    public List<Tag> depthSearch(Predicate<Tag> tagPredicate, Tag tag, List<Tag> results) {
        if (tagPredicate.test(tag)) {
            results.add(tag);
        }

        tag.getChildrenList().forEach(tagChild -> depthSearch(tagPredicate, tagChild, results));

        return results;
    }

    /**
     * Method using for breadth search.
     *
     * @param tagPredicate search predicate.
     * @param tag          current tag.
     * @param results      list of search results.
     * @return list of search results.
     */
    public List<Tag> breadthSearch(Predicate<Tag> tagPredicate, Tag tag, List<Tag> results) {
        Queue<Tag> queue = new LinkedList<>();
        queue.add(tag);

        while (!queue.isEmpty()) {
            Tag temp = queue.remove();

            if (tagPredicate.test(temp)) {
                results.add(temp);
            }

            queue.addAll(temp.getChildrenList());
        }

        return results;
    }

    public String getURL() {
        String name = this.getName().equals(CommonConst.SCHEMA) ? this.getAttributeByKey(CommonConst.DB_NAME).toLowerCase() : this.getAttributeByKey(CommonConst.NAME).toLowerCase();
        if (this.getParent() != null) {
            return this.getParent().getURL() + "." + name;
        }

        return name;
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder();

        result.append("<").append(name.equals("") ? "UNNAMED" : name);

        if (!getAttributes().isEmpty()) {
            attributes.forEach((key, value) -> result.append(" ").append(key).append("=" + "\"").append(value).append("\""));
            result.append(">");
        } else {
            result.append(">");
        }

        if (!getChildrenList().isEmpty()) {
            getChildrenList().forEach((tag) -> result.append(EOL).append(tag.toString()));
        }

        if (!getContent().equals("")) {
            result.append(content);
        }

        if (parent == null) {
            result.append(EOL);
        }

        result.append("</").append(name.equals("") ? "UNNAMED" : name).append(">");

        return result.toString();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        Tag tag = (Tag) obj;
        return Objects.equals(name, tag.name) && Objects.equals(attributes, tag.attributes);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, attributes);
    }
}
