package core.exceptions;

/**
 * An core.exceptions that provides information when tag not found error.
 */
public class TagNotFoundException extends Exception {

    public TagNotFoundException(String message) {
        super(message);
    }

    public TagNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

    public TagNotFoundException(Throwable cause) {
        super(cause);
    }
}
