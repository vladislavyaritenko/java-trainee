package core.exceptions;

/**
 * An core.exceptions that provides information on an argument manager error.
 */
public class ArgumentException extends Exception {

    public ArgumentException(String message) {
        super(message);
    }

    public ArgumentException(String message, Throwable cause) {
        super(message, cause);
    }

    public ArgumentException(Throwable cause) {
        super(cause);
    }
}
