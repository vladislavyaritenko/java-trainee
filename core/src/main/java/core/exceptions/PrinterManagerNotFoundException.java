package core.exceptions;


/**
 * An core.exceptions that provides information when printer manager not found error.
 */
public class PrinterManagerNotFoundException extends Exception {
    public PrinterManagerNotFoundException(String message) {
        super(message);
    }

    public PrinterManagerNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

    public PrinterManagerNotFoundException(Throwable cause) {
        super(cause);
    }
}
