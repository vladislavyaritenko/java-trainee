package core.exceptions;

/**
 * An core.exceptions that provides information when printer not found error.
 */
public class PrinterNotFoundException extends Exception {
    public PrinterNotFoundException(String message) {
        super(message);
    }

    public PrinterNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

    public PrinterNotFoundException(Throwable cause) {
        super(cause);
    }
}
