package core.exceptions;

/**
 * An core.exceptions that provides information when tag was loaded by lazy or detail load and and it cannot be printed.
 */
public class PrintCannotException extends Exception {
    public PrintCannotException(String message) {
        super(message);
    }

    public PrintCannotException(String message, Throwable cause) {
        super(message, cause);
    }

    public PrintCannotException(Throwable cause) {
        super(cause);
    }
}

