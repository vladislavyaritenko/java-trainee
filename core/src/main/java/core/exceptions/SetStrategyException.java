package core.exceptions;

/**
 * An core.exceptions that provides information on an set strategy error.
 */
public class SetStrategyException extends RuntimeException {

    public SetStrategyException(String message) {
        super(message);
    }

    public SetStrategyException(String message, Throwable cause) {
        super(message, cause);
    }

    public SetStrategyException(Throwable cause) {
        super(cause);
    }
}
