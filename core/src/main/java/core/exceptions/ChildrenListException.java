package core.exceptions;

/**
 * An core.exceptions that provides information on add to Children list error.
 */
public class ChildrenListException extends RuntimeException {
    public ChildrenListException(String message) {
        super(message);
    }

    public ChildrenListException(String message, Throwable cause) {
        super(message, cause);
    }

    public ChildrenListException(Throwable cause) {
        super(cause);
    }
}
