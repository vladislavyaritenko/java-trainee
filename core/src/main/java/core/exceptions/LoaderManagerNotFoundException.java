package core.exceptions;

/**
 * An core.exceptions that provides information when loader manager not found error.
 */
public class LoaderManagerNotFoundException extends Exception {
    public LoaderManagerNotFoundException(String message) {
        super(message);
    }

    public LoaderManagerNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

    public LoaderManagerNotFoundException(Throwable cause) {
        super(cause);
    }
}
