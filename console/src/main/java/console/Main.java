package console;

import console.context.Context;
import core.exceptions.ArgumentException;
import core.exceptions.ParserException;
import core.exceptions.ReflectionException;
import org.apache.logging.log4j.LogManager; import org.apache.logging.log4j.Logger;

import java.util.StringJoiner;

/**
 * console.Main class - entry point in the program.
 */
public final class Main {

    private static final Logger LOGGER = LogManager.getLogger(Main.class);
    private static final String EOL = System.lineSeparator();

    private Main() {
    }

    /**
     * console.Main method of programm.
     *
     * @param args Arguments of console.command line.
     * @throws ArgumentException if an error occurred while reading the console.command line parameters.
     * @throws ParserException   if an error occurred while parsing the file.
     */
    public static void main(String[] args) throws ArgumentException, ParserException, ReflectionException {

        LOGGER.info(EOL + "======================================================================================================");
        LOGGER.info(getCommandInfo());

        ArgumentManager manager;

//        -connect -db mysql -url localhost:3306/sakila -u root -p pass -load full sakila -print schema.tabel.customer

        String[] tempArgs;
        if (args.length == 0) {
            String f1 = "-connect";
            String f2 = "-db";
            String f3 = "mysql";
            String f4 = "-url";
            String f5 = "localhost:3306/sakila";
            String f6 = "-u";
            String f7 = "root";
            String f8 = "-p";
            String f9 = "pass";
//            String f11 = "full";
//            String f11 = "detail";
            String f10 = "-load";
            String f11 = "lazy";
            String f12 = "sakila";

            String f17 = "-load";
            String f18 = "lazy";
            String f19 = "actor";
//
//            String f20 = "-load";
//            String f21 = "full";
//            String f22 = "film_list";

//            String f17 = "-load";
//            String f18 = "lazy";
//            String f19 = "tables";

//            String f20 = "-load";
//            String f21 = "lazy";
//            String f22 = "customer";

//            String f20 = "-load";
//            String f21 = "full";
//            String f22 = "film_in_stock";

//            String f23 = "-load";
//            String f24 = "lazy";
//            String f25 = "indexes";
//
//            String f26 = "-load";
//            String f27 = "full";
//            String f28 = "idx_fk_store_id";

//            String f12 = "sakila.tables";
//            String f12 = "sakila.tables.customer";
//            String f12 = "sakila.tables.customer.indexes";
//            String f12 = "sakila.tables.customer.indexes.idx_fk_store_id";
//            String f12 = "sakila.tables.customer.triggers";
//            String f12 = "sakila.tables.customer.triggers.customer_create_date";
//            String f12 = "sakila.tables.customer.foreign_keys";
//            String f12 = "sakila.tables.customer.foreign_keys.fk_customer_address";
//
//            String f12 = "sakila.views";
//            String f12 = "sakila.views.actor_info";
//
//            String f12 = "sakila.procedures";
//            String f12 = "sakila.procedures.film_in_stock";
//
//            String f12 = "sakila.functions";
//            String f12 = "sakila.functions.get_customer_balance";

            String f13 = "-print";
//            String f14 = "schema.foreign_key.fk_customer_address";
            String f14 = "schema.table.actor";

//            String f23 = "-print";
//            String f24 = "schema.table.customer";
//            String f14 = "schema.table.customer";
//            String f14 = "schema.index.idx_fk_city_id";
//            String f14 = "schema.index.idx_fk_address_id";
//            String f14 = "schema.foreign_key.fk_customer_address";
//            String f14 = "schema.function.get_customer_balance";
//            String f14 = "schema.index.idx_fk_city_id";
//            String f14 = "schema.trigger.customer_create_date";
//            String f14 = "schema.table.customer";
//            String f14 = "schema.function.get_customer_balance";
//            String f14 = "schema.procedure.film_in_stock";
//            String f14 = "schema.procedure.film_in_stock";
//            String f14 = "schema.procedure.film_in_stock";
//            String f14 = "schema.procedure.rewards_report";


            String f15 = "-output";
            String f16 = "dbOutput.xml";


//            tempArgs = new String[] {f1, f2, f3, f4, f5, f6, f7, f8, f9, f10, f11, f12, f13, f14,  f15, f16};
            tempArgs = new String[] {f1, f2, f3, f4, f5, f6, f7, f8, f9, f10, f11, f12,f17,f18,f19,/*f20,f21,f22,*//*f23,f24,*/ f13,f14, f15, f16};
//            tempArgs = new String[] {f1, f2, f3, f4, f5, f6, f7, f8, f9, f10, f11, f12, f17, f18, f19, f15, f16};
//            tempArgs = new String[] {f1, f2, f3, f4, f5, f6, f7, f8, f9, f10, f11, f12, f20, f21, f22, f15, f16};
//            tempArgs = new String[] {f1, f2, f3, f4, f5, f6, f7, f8, f9, f10, f11, f12, f17, f18, f19, f20, f21, f22, f15, f16};
//            tempArgs = new String[] {f1, f2, f3, f4, f5, f6, f7, f8, f9, f10, f11, f12, f20, f21, f22, f26, f27, f28, f15, f16};
//            tempArgs = new String[] {f1, f2, f3, f4, f5, f6, f7, f8, f9, f10, f11, f12, f20, f21, f22, f23, f24, f25, f15, f16};
//            tempArgs = new String[] {f1, f2, f3, f4, f5, f6, f7, f8, f9, f10, f11, f12, f17, f18, f19, f20, f21, f22,f23,f24,f25,f26,f27,f28, f15, f16};

            manager = new ArgumentManager(tempArgs);
            manager.parseArgs();
            manager.executeAllCommands();
        } else {
            for (String arg : args) {
                System.out.println(arg);
            }
            manager = new ArgumentManager(args);
            manager.parseArgs();
            manager.executeAllCommands();
        }
        LOGGER.info("App finished.");
    }

    /**
     * Get information about console.command.
     *
     * @return a string representation of the example of all commands.
     */
    public static String getCommandInfo() {

        StringJoiner joiner = new StringJoiner(EOL);

        joiner.add("EXAMPLES OF COMMANDS:");
        joiner.add("Task 1:").add("-input \"fileName.xml\"").add("-output \"fileName.xml\"");
        joiner.add("-search \"nameNode\" -ds").add("-search \"nameTag\" -bs").add("-search \" \"nameAttribute\" \"value\" \" -ds");
        joiner.add("-search \" \"nameAttribute\" \"value\" \" -bs").add("-bs or -ds is Breadth or Depth search.");
        joiner.add("");
        joiner.add("Task 2:").add("-connect").add("-db").add("\"typeDb\"").add("-url").add("\"URL\"").add("-u").add("\"userName\"");
        joiner.add("-p").add("\"password\"").add("-load").add("detail | lazy | full").add("\"sakila").add("").add("sakila.tables").add("sakila.tables.customer");
        joiner.add("sakila.tables.customer.indexes").add("sakila.tables.customer.indexes.idx_fk_address_id").add("sakila.tables.customer.triggers");
        joiner.add("sakila.tables.customer.triggers.customer_create_date").add("sakila.tables.customer.foreign_keys");
        joiner.add("sakila.tables.customer.foreign_keys.fk_customer_address").add("").add("sakila.views").add("sakila.views.actor_info").add("");
        joiner.add("sakila.procedures").add("sakila.procedures.film_in_stock").add("").add("sakila.functions").add("sakila.functions.get_customer_balance\"").add("");
        joiner.add("-print").add("\"schema").add("").add("schema.table.customer").add("schema.index.idx_fk_city_id").add("schema.trigger.customer_create_date");
        joiner.add("schema.foreign_key.fk_customer_address").add("").add("schema.view.actor_info").add("").add("schema.procedure.film_in_stock").add("");
        joiner.add("schema.function.get_customer_balance\"");

        return joiner.toString();
    }
}
