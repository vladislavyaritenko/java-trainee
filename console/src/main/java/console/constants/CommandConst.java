package console.constants;

/**
 * Command set for console package.
 */
public enum CommandConst {
    INPUT("-input"), OUTPUT("-output"), SEARCH("-search"), ERROR(), LOAD("-load"), PRINT("-print"), CONNECT("-connect");

    private String valueCommand;

    CommandConst() {
    }

    CommandConst(String nameCommand) {
        this.valueCommand = nameCommand;
    }

    /**
     * Method using for getting the object value of the console.command by its value.
     *
     * @param valueCommand object console.command value.
     * @return console.command object value.
     */
    public static CommandConst getConst(String valueCommand) {
        for (CommandConst constt : CommandConst.values()) {
            if (valueCommand.equals(constt.valueCommand)) {
                return constt;
            }
        }
        return ERROR;
    }
}
