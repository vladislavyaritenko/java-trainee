package console;

import console.command.Receiver;
import console.command.impl.*;
import console.constants.CommandConst;
import core.constants.TypeFileConst;
import console.context.Context;
import core.exceptions.ArgumentException;
import core.exceptions.ParserException;
import core.exceptions.ReflectionException;
import org.apache.logging.log4j.LogManager; import org.apache.logging.log4j.Logger;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Class for work with console.
 */
public class ArgumentManager {
    private static final Logger LOGGER = LogManager.getLogger(ArgumentManager.class);

    private List<String> args;
    private Receiver receiver;
    private Map<String, String> connArgs;

    private static final TypeFileConst DEFAULT_TYPE_FILE = TypeFileConst.XML;

    /**
     * console.ArgumentManager class constructor.
     *
     * @param args console.command line arguments.
     */
    public ArgumentManager(String[] args) throws ArgumentException {
        receiver = new Receiver();
        connArgs = new HashMap<>();
        this.args = new ArrayList<>();
        this.args.addAll(Arrays.asList(getArgsSearch(args)));

       LOGGER.info("Created ArrayList with correct console.command. " + Arrays.toString(args));
        LOGGER.info("Start parsing arguments.");
    }

    public Receiver getReceiver() {
        return receiver;
    }

    /**
     * Method executing all added commands to the receiver list.
     *
     * @throws ParserException if an error occurred while parsing the file.
     */
    public void executeAllCommands() throws ParserException {
        receiver.run();
    }

    /**
     * Method for parsing arguments.
     *
     * @throws ArgumentException if an error occurred while reading the console.command line parameters.
     */
    public void parseArgs() throws ArgumentException, ReflectionException {
        for (int i = 0; i < args.size(); i++) {
            String currentArg = args.get(i);

            switch (CommandConst.getConst(currentArg)) {
                case INPUT:
                    parseInputCommand(args.get(i + 1));
                    i++;
                    break;
                case OUTPUT:
                    parseOutputCommand(args.get(i + 1));
                    i++;
                    break;
                case SEARCH:
                    parseSearchCommand(args.get(i + 1), args.get(i + 2));
                    i = i + 2;
                    break;
                case LOAD:
                    List<String> loadArgs = new LinkedList<>();
                    for (int y = i + 1; y < args.size(); y++) {
                        if (!args.get(y).contains("-")) {
                            loadArgs.add(args.get(y));

                        } else {
                            break;
                        }
                    }
                    parseLoaderArgs(loadArgs);
                    break;
                case PRINT:
                    parsePrinterArgs(args.get(i + 1));
                    break;
                case CONNECT:
                    int quantityOfConnectionArgs = 8;
                    if (args.get(i + quantityOfConnectionArgs) != null) {
                        List<String> conArgs = new ArrayList<>();
                        for (int y = 1; y <= quantityOfConnectionArgs; y++) {
                            conArgs.add(args.get(i + y));
                        }
                        parseConnectionArgs(conArgs);
                    }

                    break;
                default:
                    break;
            }
        }
        LOGGER.info("Arguments parsed.");
    }

    /**
     * Method using for parsing connection arguments.
     *
     * @param conArgs list of parameters for connecting to the database.
     */
    private void parseConnectionArgs(List<String> conArgs) throws ArgumentException {
        for (int i = 0; i < conArgs.size(); i = i + 2) {
            String currentArg = conArgs.get(i);
            String nextArg = "";
            if (i < conArgs.size() - 1) {
                nextArg = conArgs.get(i + 1);
            }
            switch (currentArg) {
                case "-db":
                case "-url":
                case "-u":
                case "-p":
                    connArgs.put(currentArg.replace("-", ""), nextArg);
                    if (currentArg.equals("-url")) {
                        Context.getInstance().setDBName(nextArg.substring(nextArg.lastIndexOf("/") + 1));
                    }
                    if (connArgs.size() == 4) {
                        Connect connect = new Connect(connArgs.get("db"), connArgs.get("url"), connArgs.get("u"), connArgs.get("p"), 1);
                        receiver.add(connect);
                    }
                    break;
                default:
                    throw new ArgumentException("Not enough parameters to establish a connection.");
            }
        }
    }

    /**
     * Method using for parsing loader argument.
     *
     * @param loaderSequence loader arguments.
     */
    private void parseLoaderArgs(List<String> loaderSequence) {
        receiver.add(new Load(loaderSequence, 2));
    }

    /**
     * Method using for parsing printer argument.
     *
     * @param printArgs printer arguments.
     */
    private void parsePrinterArgs(String printArgs) throws ReflectionException {
        receiver.add(new Print(printArgs, 3));
    }

    /**
     * Method using for parsing <paramSearch> paramSearch </paramSearch> by value tag or by key and value.
     *
     * @param paramSearch search options.
     * @param typeSearch  type of search.
     */
    private void parseSearchCommand(String paramSearch, String typeSearch) {
        String[] tempParamSearch = paramSearch.split(" ");

        List<String> searchParams = new ArrayList<>(Arrays.asList(tempParamSearch));

        String searchType = typeSearch.replace("-", "");
        receiver.add(new Search(searchParams, searchType, 2));
    }

    /**
     * Validating output file value and parsing java objects inside.
     *
     * @param nameFile output file value.
     */
    private void parseOutputCommand(String nameFile) throws ArgumentException {
        if (validateFile(nameFile)) {
            receiver.add(new Output(nameFile, DEFAULT_TYPE_FILE, 2));
           LOGGER.info("Output arg parsed.");
        } else {
            LOGGER.error("Incorrect output file.");
            throw new ArgumentException("Incorrect output file.");
        }
    }

    /**
     * Method using for parsing input file argument.
     *
     * @param nameFile value input file.
     * @throws ArgumentException if an error occurred while reading the console.command line parameters.
     */
    private void parseInputCommand(String nameFile) throws ArgumentException {
        if (validateFile(nameFile)) {
            receiver.add(new Input(nameFile, 1));
           LOGGER.info("Input argument parsed.");
        } else {
            LOGGER.error("Incorrect input file.");
            throw new ArgumentException("Incorrect input file.");
        }
    }

    /**
     * Method using for checking correct value file.
     *
     * @param file value file.
     * @return true, if value file is correct.
     */
    private boolean validateFile(String file) {
        String outputFileRegex = "\\w+\\.[a-z]{2,5}$";

        Pattern pattern = Pattern.compile(outputFileRegex);
        Matcher matcher = pattern.matcher(file);

        return matcher.find();
    }

    /**
     * Method using for combining search query parameters into a single string.
     *
     * @param args console.command line arguments.
     * @return string representation of request parameters.
     * @throws ArgumentException if console.command line arguments invalid.
     */
    public static String[] getArgsSearch(String[] args) throws ArgumentException {
        if (args.length == 4) {
            String[] result = new String[3];
            StringJoiner joiner = new StringJoiner(" ");

            for (int i = 0; i < args.length; i++) {
                if (args[i].equals("-search")) {
                    result[0] = args[i];
                    for (int j = i + 1; j < args.length; j++) {
                        if ((args[j].equals("-bs")) || (args[j].equals("-ds"))) {
                            result[1] = joiner.toString();
                            result[2] = args[j];
                            return result;
                        }
                        joiner.add(args[j]);
                    }
                }
            }
            throw new ArgumentException("Incorrect console.command.");
        }
        return args;
    }
}
