package console.command;

import core.exceptions.*;

import java.util.Comparator;
import java.util.PriorityQueue;
import java.util.Queue;

/**
 * Controller that obtaining the list of commands and calling each of them.
 */
public class Receiver {
    private Queue<Command> commands = new PriorityQueue<>(10, Comparator.comparingInt(Command::getPriority));

    public Queue<Command> getCommands() {
        return commands;
    }

    /**
     * Method using for running all commands.
     *
     * @throws ParserException if an error occurred while parsing the file.
     */
    public void run() throws ParserException {
        for (Command c : commands) {
            c.execute();
        }
    }

    /**
     * Method using for adding the execution commands to the queue.
     *
     * @param command console.command for executing.
     */
    public void add(Command command) {
        commands.add(command);
    }
}
