package console.command;

import core.exceptions.ParserException;

/**
 * Interface that used for setting console.command priorities and executing them.
 */
public interface Command {
    /**
     * Method using for obtaining priority of console.command.
     *
     * @return priority of console.command.
     */
    Integer getPriority();

    /**
     * Method using for implementing pattern Command.
     *
     * @throws ParserException if an error occurred while parsing the file.
     */
    void execute() throws ParserException;
}
