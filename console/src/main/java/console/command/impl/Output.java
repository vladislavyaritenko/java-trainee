package console.command.impl;

import core.constants.TypeFileConst;
import console.command.Command;
import core.exceptions.ParserException;
import core.exceptions.SetStrategyException;
import core.service.saving.SavingClient;
import console.context.Context;
import core.structure.Tag;
import org.apache.logging.log4j.LogManager; import org.apache.logging.log4j.Logger;

/**
 * Class for implementation Output console.command.
 */
public class Output implements Command {
    private static final Logger LOGGER = LogManager.getLogger(Output.class);

    private String nameFile;
    private TypeFileConst typeFile;
    private Integer priority;

    /**
     * Output class constructor.
     *
     * @param nameFile value file.
     * @param priority output console.command execution priority.
     */
    public Output(String nameFile, TypeFileConst typeFile, Integer priority) {
        this.nameFile = nameFile;
        this.typeFile = typeFile;
        this.priority = priority;
    }

    @Override
    public Integer getPriority() {
        return priority;
    }

    @Override
    public void execute() throws ParserException {
        LOGGER.info("Start saving Java Objects to XML file...");
        if (Context.getInstance().getTag() == null) {
            LOGGER.warn("First of all you need to execute the Input console.command.");
            throw new ParserException("First of all you need to execute the Input console.command.");
        }
        try {
            SavingClient out = new SavingClient();
            out.setStrategy(typeFile);
            Tag tag = Context.getInstance().getTag();
            out.executeStrategy(tag, nameFile);
        } catch (SetStrategyException e) {
            LOGGER.error(e);
            throw new ParserException("Incorrect file type.", e);
        }
    }
}
