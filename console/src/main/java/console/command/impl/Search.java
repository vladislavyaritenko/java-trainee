package console.command.impl;

import console.command.Command;
import console.context.Context;
import core.exceptions.ParserException;
import core.structure.Tag;
import org.apache.logging.log4j.LogManager; import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;

/**
 * Class for implementation Search console.command.
 */
public class Search implements Command {
    private static final Logger LOGGER = LogManager.getLogger(Search.class);

    private List<String> searchParam;
    private String searchType;
    private Integer priority;

    private List<Tag> resultsSearch;

    public List<Tag> getResultsSearch() {
        return resultsSearch;
    }

    /**
     * Search class constructor.
     *
     * @param searchParam list of search parameters.
     * @param searchType  type of search.
     * @param priority    search console.command execution priority.
     */
    public Search(List<String> searchParam, String searchType, Integer priority) {
        this.searchParam = searchParam;
        this.searchType = searchType;
        this.priority = priority;
    }

    public Search(List<String> searchParam) {
        this.searchParam = searchParam;
        this.searchType = "bs";
    }

    @Override
    public Integer getPriority() {
        return priority;
    }

    @Override
    public void execute() throws ParserException {
        LOGGER.info("Start searching in XML file.");
        if (Context.getInstance().getTag() == null) {
            LOGGER.warn("First of all you need to execute the Input console.command.");
            throw new ParserException("First of all you need to execute the Input console.command.");
        }

        Predicate<Tag> tagPredicate = selectPredicate(searchParam);
        Tag tag = Context.getInstance().getTag();
        resultsSearch = executeSearch(searchType, tagPredicate, tag);
//        System.out.println("Search results: " + Arrays.toString(resultsSearch.toArray()));
        LOGGER.info("Search results: " + Arrays.toString(resultsSearch.toArray()));
        LOGGER.info("Search ended");
    }


    /**
     * Method using for executing search.
     *
     * @param searchType   type of search.
     * @param tagPredicate search predicate.
     * @param tag          root tag.
     * @return list of search results.
     */
    private List<Tag> executeSearch(String searchType, Predicate<Tag> tagPredicate, Tag tag) {
        List<Tag> results = new ArrayList<>();
        switch (searchType) {
            case "ds":
                LOGGER.info("Start depth searching");
                if (tagPredicate != null) {
                    results = tag.depthSearch(tagPredicate, tag, results);
                }
                break;
            case "bs":
                LOGGER.info("Start breadth searching");
                if (tagPredicate != null) {
                    results = tag.breadthSearch(tagPredicate, tag, results);
                }
                break;
            default:
                break;
        }
        return results;
    }

    /**
     * Method using for selecting of search predicate.
     *
     * @param searchParam list search parameters.
     * @return search predicate.
     */
    private Predicate<Tag> selectPredicate(List<String> searchParam) {
        Predicate<Tag> tagPredicate = null;
        if (searchParam.size() == 1) {
            tagPredicate = tag1 -> tag1.getName().equals(searchParam.get(0));
        }
        if (searchParam.size() == 2) {
            tagPredicate = tag1 -> {
                if (!tag1.getAttributes().isEmpty()) {
                    String key = tag1.getAttributes().get(searchParam.get(0));
                    return key != null && key.equals(searchParam.get(1));
                }
                return false;
            };
        }
        return tagPredicate;
    }
}

