package console.command.impl;

import console.command.Command;
import console.context.Context;
import core.db.constants.CommonConst;
import core.exceptions.DBException;
import core.exceptions.TagNotFoundException;
import core.structure.Tag;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Class for implementation Load console.command.
 */
public class Load implements Command {
    private static final Logger LOGGER = LogManager.getLogger(Load.class);

    private Integer priority;
    private List<String> loaderSequence;
    private String typeLoad;


    /**
     * Load class constructor.
     *
     * @param loaderSequence loader arguments.
     * @param priority       console.command execution priority.
     */
    public Load(List<String> loaderSequence, Integer priority) {
        this.priority = priority;
        typeLoad = loaderSequence.get(0).toLowerCase();
        this.loaderSequence = getCorrectListOfLoadParameters(loaderSequence);
    }

    @Override
    public Integer getPriority() {
        return this.priority;
    }

    @Override
    public void execute() {
        LOGGER.info("Start LoadCommand...");
        try {
            Tag tag = Context.getInstance().getTag();
            Tag foundTag = tag;
            if (!tag.getChildrenList().isEmpty()) {
                foundTag = findTag(tag);
            }

            switch (typeLoad) {
                case CommonConst.FULL:
                    Context.getInstance().getLoaderManager().fullLoad(foundTag);
                    break;
                case CommonConst.LAZY:
                    Context.getInstance().getLoaderManager().lazyLoad(foundTag);
                    break;
                case CommonConst.DETAIL:
                    Context.getInstance().getLoaderManager().detailLoad(foundTag);
                    break;
                default:
                    break;
            }

            Context.getInstance().setTag(tag);
        } catch (DBException e) {
            LOGGER.error(e);
            e.printStackTrace();
        } catch (TagNotFoundException e) {
            e.printStackTrace();
        }
    }

    /**
     * Method using for finding the required tag.
     *
     * @param root root tag.
     * @return found tag.
     * @throws TagNotFoundException if tag not found.
     */
    private Tag findTag(Tag root) throws TagNotFoundException {
        String entity = loaderSequence.get(0);
        for (Tag tag : root.getChildrenList()) {
            if (entity.equals(tag.getAttributeByKey(CommonConst.NAME).toLowerCase())) {
                return tag;
//                return tag.removeChildren();
            }
            for (Tag child : tag.getChildrenList()) {
                if (entity.equals(child.getAttributeByKey(CommonConst.NAME).toLowerCase())) {
                    return child;
//                    return child.removeChildren();
                }
            }
        }

        throw new TagNotFoundException("Tag \"" + entity + "\" not found");
    }

    /**
     * Method using for parsing Loader arguments.
     *
     * @param list list of Loader arguments.
     * @return correct string of load parameters.
     */
    private List<String> getCorrectListOfLoadParameters(List<String> list) {
        list.remove(0);
        StringBuilder sb = new StringBuilder();
        for (String s : list) {
            sb.append(s);
        }
        String[] sequence = sb.toString().split("\\.");

        return new ArrayList<>(Arrays.asList(sequence));
    }
}
