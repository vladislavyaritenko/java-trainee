package console.command.impl;

import console.command.Command;
import console.context.Context;
import core.db.constants.CommonConst;
import core.db.printers.Printer;
import core.db.printers.PrinterManager;
import core.exceptions.ParserException;
import core.exceptions.PrintCannotException;
import core.exceptions.ReflectionException;
import core.structure.Tag;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.List;

/**
 * Class for implementation Print console.command.
 */
public class Print implements Command {
    private static final Logger LOGGER = LogManager.getLogger(Print.class);

    private Integer priority;
    private List<String> sequence = new ArrayList<>();
    private List<String> searchParam = new ArrayList<>();
    private Tag result;
    private PrinterManager printerManager = null;

    /**
     * Print class constructor.
     *
     * @param sequence argument for printing.
     * @param priority input console.command execution priority.
     */
    public Print(String sequence, Integer priority) throws ReflectionException {
        this.priority = priority;
        getCorrectPrinterInputParams(sequence);
        Context.getInstance().setPrinterManager(new PrinterManager(Context.getInstance().getDbType()));
    }

    @Override
    public Integer getPriority() {
        return this.priority;
    }

    @Override
    public void execute() throws ParserException {
        LOGGER.info("Executing Print Command ...");
        parsingPrintArgumentsAndSetTagForPrinters();

        Search search = new Search(searchParam);
        search.execute();
        result = search.getResultsSearch().get(0);

        printerManager = Context.getInstance().getPrinterManager();

        try {
            Printer printer = printerManager.getPrintersMap().get(result.getName());
            String ddl = printer.print(result);
            LOGGER.info(ddl);
            System.out.println(ddl);
        } catch (NullPointerException e) {
            LOGGER.error("No printer " + result.getName(), e);
            throw new RuntimeException(e);
        } catch (PrintCannotException e) {
            LOGGER.error("Cannot printer " + result.getName(), e);
            throw new RuntimeException(e);
        }
    }

    private void getCorrectPrinterInputParams(String inputParams) {
        if (!inputParams.isEmpty() && inputParams.contains(".")) {
            int dotIndex = inputParams.indexOf(".");
            String param = inputParams.substring(0, dotIndex);
            sequence.add(param);
            String nextPart = inputParams.substring(dotIndex + 1);
            if (nextPart.contains(".")) {
                getCorrectPrinterInputParams(nextPart);
            } else {
                sequence.add(nextPart);
            }
        } else if (!inputParams.isEmpty()) {
            sequence.add(inputParams);
        }
    }

    private void parsingPrintArgumentsAndSetTagForPrinters() {
        if (sequence.size() == 1) {
            searchParam.add(sequence.get(0).toUpperCase());
        }
        if (sequence.size() == 3) {
            searchParam.add(CommonConst.NAME);
            searchParam.add(sequence.get(2));
        }
    }
}
