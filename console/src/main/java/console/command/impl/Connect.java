package console.command.impl;

import console.command.Command;
import console.context.Context;
import core.db.connection.ConnectionManager;
import core.db.constants.CommonConst;
import core.db.constants.DBType;
import core.db.loaders.LoaderManager;
import core.exceptions.DBException;
import core.exceptions.ReflectionException;
import org.apache.logging.log4j.LogManager; import org.apache.logging.log4j.Logger;
import core.structure.Tag;

import java.sql.Connection;

/**
 * Class for implementation Connect console.command.
 */
public class Connect implements Command {
    private static final Logger LOGGER = LogManager.getLogger(Connect.class);

    private ConnectionManager manager;
    private DBType dbType;
    private String url;
    private String user;
    private String password;
    private Integer priority;

    /**
     * Connect class constructor.
     *
     * @param dbType   database type.
     * @param url      database url.
     * @param user     database user.
     * @param password database password.
     * @param priority Connect console.command execution priority.
     */
    public Connect(String dbType, String url, String user, String password, Integer priority) {
        manager = new ConnectionManager();
        this.dbType = DBType.getDBType(dbType);
        this.url = url;
        this.user = user;
        this.password = password;
        this.priority = priority;
        Context.getInstance().setDbType(this.dbType);
    }

    public ConnectionManager getManager() {
        return manager;
    }

    @Override
    public Integer getPriority() {
        return this.priority;
    }

    @Override
    public void execute() {
        LOGGER.info("Command connectToDB starting...");
        Connection connection = null;
        try {
            connection = manager.initConnection(dbType, url, user, password);
        } catch (DBException e) {
            e.printStackTrace();
        }

        Tag tag = new Tag(CommonConst.SCHEMA);
        tag.setAttribute(CommonConst.DB_NAME, Context.getInstance().getDbName());
        Context.getInstance().setTag(tag);

        try {
            Context.getInstance().setLoaderManager(new LoaderManager(connection, dbType));
        } catch (ReflectionException e) {
            e.printStackTrace();
        }
        LOGGER.info("LoaderManager is created...");
    }
}
