package console.command.impl;

import console.command.Command;
import core.service.parsing.ParsingClient;
import core.service.parsing.impl.ParseFromXML;
import core.exceptions.ParserException;
import console.context.Context;
import core.structure.Tag;
import org.apache.logging.log4j.LogManager; import org.apache.logging.log4j.Logger;

/**
 * Class for implementation Input console.command.
 */
public class Input implements Command {
    private static final Logger LOGGER = LogManager.getLogger(Input.class);
    private String inputFile;
    private Integer priority;

    @Override
    public Integer getPriority() {
        return priority;
    }

    /**
     * Input class constructor.
     *
     * @param nameFile value file.
     * @param priority input console.command execution priority.
     */
    public Input(String nameFile, Integer priority) {
        this.inputFile = nameFile;
        this.priority = priority;
    }

    @Override
    public void execute() throws ParserException {
        LOGGER.info("Start parsing XML file to Java Objects...");
        ParsingClient in = new ParsingClient();
        ParseFromXML implParsingStrategy = new ParseFromXML();
        in.setStrategy(implParsingStrategy);
        Tag tag = in.executeStrategy(inputFile);

        Context context = Context.getInstance();
        context.setTag(tag);
        System.out.println(context.getTag().toString());
    }
}
