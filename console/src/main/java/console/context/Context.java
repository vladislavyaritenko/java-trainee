package console.context;

import core.db.constants.DBType;
import core.db.loaders.LoaderManager;
import core.db.printers.PrinterManager;
import core.structure.Tag;
import org.apache.logging.log4j.LogManager; import org.apache.logging.log4j.Logger;

/**
 * Class using as an application console.context.
 */
public final class Context {
    private static final Logger LOGGER = LogManager.getLogger(Context.class);

    private static Context instance = null;
    private Tag tag;
    private LoaderManager loaderManager = null;
    private PrinterManager printerManager = null;
    private String dbName = null;
    private DBType dbType = null;

    private Context() {
    }

    /**
     * Gets the core.structure. Context for the current backend.
     *
     * @return the backend console.context.
     */
    public static synchronized Context getInstance() {
        if (instance == null) {
            instance = new Context();
        }
       LOGGER.info("Get instance Context ->" + instance);
        return instance;
    }

    public Tag getTag() {
        return tag;
    }

    public void setTag(Tag tag) {
        this.tag = tag;
    }

    public void setDBName(String dbName) {
        this.dbName = dbName;
    }

    public String getDbName() {
        return dbName;
    }

    public LoaderManager getLoaderManager() {
        return loaderManager;
    }

    public PrinterManager getPrinterManager() {
        return printerManager;
    }

    public void setLoaderManager(LoaderManager loaderManager) {
        this.loaderManager = loaderManager;
    }

    public void setPrinterManager(PrinterManager printerManager) {
        this.printerManager = printerManager;
    }

    public DBType getDbType() {
        return dbType;
    }

    public void setDbType(DBType dbType) {
        this.dbType = dbType;
    }

    public static void clear() {
        instance = null;
    }
}

