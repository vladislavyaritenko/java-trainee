package integrationTest;

import console.Main;
import core.exceptions.ArgumentException;
import core.exceptions.ParserException;
import core.exceptions.ReflectionException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.List;

@RunWith(Parameterized.class)
public class IntegrationDbTest {
    private final String[] param;

    public IntegrationDbTest(String[] param) {
        this.param = param;
    }

    @Test
    public void main() throws ArgumentException, ParserException, ReflectionException {
        Main.main(param);
    }


    @Parameterized.Parameters
    public static List<Object> input() {
        return Arrays.asList(new Object[][] {

            {new String[] {"-connect", "-db", "mysql", "-url", "localhost:3306/sakila", "-u",
                "root", "-p", "pass", "-load", "lazy", "sakila",
                "-output", "db.xml"}},
            {new String[] {"-connect", "-db", "mysql", "-url", "localhost:3306/sakila", "-u",
                "root", "-p", "pass", "-load", "lazy", "sakila.tables.customer",
                "-output", "db.xml"}},
            {new String[] {"-connect", "-db", "mysql", "-url", "localhost:3306/sakila", "-u",
                "root", "-p", "pass", "-load", "lazy", "sakila.tables.customer.indexes",
                "-output", "db.xml"}},
            {new String[] {"-connect", "-db", "mysql", "-url", "localhost:3306/sakila", "-u",
                "root", "-p", "pass", "-load", "lazy", "sakila.tables.customer.triggers",
                "-output", "db.xml"}},
            {new String[] {"-connect", "-db", "mysql", "-url", "localhost:3306/sakila", "-u",
                "root", "-p", "pass", "-load", "lazy", "sakila.tables.customer.foreign_keys",
                "-output", "db.xml"}},
            {new String[] {"-connect", "-db", "mysql", "-url", "localhost:3306/sakila", "-u",
                "root", "-p", "pass", "-load", "lazy", "sakila.views",
                "-output", "db.xml"}},
            {new String[] {"-connect", "-db", "mysql", "-url", "localhost:3306/sakila", "-u",
                "root", "-p", "pass", "-load", "lazy", "sakila.views.actor_info",
                "-output", "db.xml"}},
            {new String[] {"-connect", "-db", "mysql", "-url", "localhost:3306/sakila", "-u",
                "root", "-p", "pass", "-load", "lazy", "sakila.functions",
                "-output", "db.xml"}},
            {new String[] {"-connect", "-db", "mysql", "-url", "localhost:3306/sakila", "-u",
                "root", "-p", "pass", "-load", "lazy", "sakila.functions.get_customer_balance",
                "-output", "db.xml"}},
            {new String[] {"-connect", "-db", "mysql", "-url", "localhost:3306/sakila", "-u",
                "root", "-p", "pass", "-load", "lazy", "sakila.procedures",
                "-output", "db.xml"}},
            {new String[] {"-connect", "-db", "mysql", "-url", "localhost:3306/sakila", "-u",
                "root", "-p", "pass", "-load", "lazy", "sakila.procedures.film_in_stock",
                "-output", "db.xml"}},


            {new String[] {"-connect", "-db", "mysql", "-url", "localhost:3306/sakila", "-u",
                "root", "-p", "pass", "-load", "full", "sakila",
                "-output", "db.xml"}},
            {new String[] {"-connect", "-db", "mysql", "-url", "localhost:3306/sakila", "-u",
                "root", "-p", "pass", "-load", "full", "sakila.tables",
                "-output", "db.xml"}},
            {new String[] {"-connect", "-db", "mysql", "-url", "localhost:3306/sakila", "-u",
                "root", "-p", "pass", "-load", "full", "sakila.tables.customer",
                "-output", "db.xml"}},
            {new String[] {"-connect", "-db", "mysql", "-url", "localhost:3306/sakila", "-u",
                "root", "-p", "pass", "-load", "full", "sakila.tables.customer.indexes",
                "-output", "db.xml"}},
            {new String[] {"-connect", "-db", "mysql", "-url", "localhost:3306/sakila", "-u",
                "root", "-p", "pass", "-load", "full", "sakila.tables.customer.triggers",
                "-output", "db.xml"}},
            {new String[] {"-connect", "-db", "mysql", "-url", "localhost:3306/sakila", "-u",
                "root", "-p", "pass", "-load", "full", "sakila.tables.customer.foreign_keys",
                "-output", "db.xml"}},
            {new String[] {"-connect", "-db", "mysql", "-url", "localhost:3306/sakila", "-u",
                "root", "-p", "pass", "-load", "full", "sakila.views",
                "-output", "db.xml"}},
            {new String[] {"-connect", "-db", "mysql", "-url", "localhost:3306/sakila", "-u",
                "root", "-p", "pass", "-load", "full", "sakila.views.actor_info",
                "-output", "db.xml"}},
            {new String[] {"-connect", "-db", "mysql", "-url", "localhost:3306/sakila", "-u",
                "root", "-p", "pass", "-load", "full", "sakila.functions",
                "-output", "db.xml"}},
            {new String[] {"-connect", "-db", "mysql", "-url", "localhost:3306/sakila", "-u",
                "root", "-p", "pass", "-load", "full", "sakila.functions.get_customer_balance",
                "-output", "db.xml"}},
            {new String[] {"-connect", "-db", "mysql", "-url", "localhost:3306/sakila", "-u",
                "root", "-p", "pass", "-load", "full", "sakila.procedures",
                "-output", "db.xml"}},
            {new String[] {"-connect", "-db", "mysql", "-url", "localhost:3306/sakila", "-u",
                "root", "-p", "pass", "-load", "full", "sakila.procedures.film_in_stock",
                "-output", "db.xml"}},


            {new String[] {"-connect", "-db", "mysql", "-url", "localhost:3306/sakila", "-u",
                "root", "-p", "pass", "-load", "detail", "sakila",
                "-output", "db.xml"}},
            {new String[] {"-connect", "-db", "mysql", "-url", "localhost:3306/sakila", "-u",
                "root", "-p", "pass", "-load", "detail", "sakila.tables",
                "-output", "db.xml"}},
            {new String[] {"-connect", "-db", "mysql", "-url", "localhost:3306/sakila", "-u",
                "root", "-p", "pass", "-load", "detail", "sakila.tables.customer",
                "-output", "db.xml"}},
            {new String[] {"-connect", "-db", "mysql", "-url", "localhost:3306/sakila", "-u",
                "root", "-p", "pass", "-load", "detail", "sakila.tables.customer.indexes",
                "-output", "db.xml"}},
            {new String[] {"-connect", "-db", "mysql", "-url", "localhost:3306/sakila", "-u",
                "root", "-p", "pass", "-load", "detail", "sakila.tables.customer.indexes.idx_fk_store_id",
                "-output", "db.xml"}},
            {new String[] {"-connect", "-db", "mysql", "-url", "localhost:3306/sakila", "-u",
                "root", "-p", "pass", "-load", "detail", "sakila.tables.customer.triggers",
                "-output", "db.xml"}},
            {new String[] {"-connect", "-db", "mysql", "-url", "localhost:3306/sakila", "-u",
                "root", "-p", "pass", "-load", "detail", "sakila.tables.customer.triggers.customer_create_date",
                "-output", "db.xml"}},
            {new String[] {"-connect", "-db", "mysql", "-url", "localhost:3306/sakila", "-u",
                "root", "-p", "pass", "-load", "detail", "sakila.tables.customer.foreign_keys",
                "-output", "db.xml"}},
            {new String[] {"-connect", "-db", "mysql", "-url", "localhost:3306/sakila", "-u",
                "root", "-p", "pass", "-load", "detail", "sakila.tables.customer.foreign_keys.fk_customer_address",
                "-output", "db.xml"}},
            {new String[] {"-connect", "-db", "mysql", "-url", "localhost:3306/sakila", "-u",
                "root", "-p", "pass", "-load", "detail", "sakila.views",
                "-output", "db.xml"}},
            {new String[] {"-connect", "-db", "mysql", "-url", "localhost:3306/sakila", "-u",
                "root", "-p", "pass", "-load", "detail", "sakila.views.actor_info",
                "-output", "db.xml"}},
            {new String[] {"-connect", "-db", "mysql", "-url", "localhost:3306/sakila", "-u",
                "root", "-p", "pass", "-load", "detail", "sakila.functions",
                "-output", "db.xml"}},
            {new String[] {"-connect", "-db", "mysql", "-url", "localhost:3306/sakila", "-u",
                "root", "-p", "pass", "-load", "detail", "sakila.functions.get_customer_balance",
                "-output", "db.xml"}},
            {new String[] {"-connect", "-db", "mysql", "-url", "localhost:3306/sakila", "-u",
                "root", "-p", "pass", "-load", "detail", "sakila.procedures",
                "-output", "db.xml"}},
            {new String[] {"-connect", "-db", "mysql", "-url", "localhost:3306/sakila", "-u",
                "root", "-p", "pass", "-load", "detail", "sakila.procedures.film_in_stock",
                "-output", "db.xml"}},


            {new String[] {"-connect", "-db", "mysql", "-url", "localhost:3306/sakila", "-u",
                "root", "-p", "pass", "-load", "full", "sakila", "-print", "schema"}},
            {new String[] {"-connect", "-db", "mysql", "-url", "localhost:3306/sakila", "-u",
                "root", "-p", "pass", "-load", "full", "sakila", "-print", "schema.tabel.customer"}},
            {new String[] {"-connect", "-db", "mysql", "-url", "localhost:3306/sakila", "-u",
                "root", "-p", "pass", "-load", "full", "sakila", "-print", "schema.index.idx_fk_address_id"}},
            {new String[] {"-connect", "-db", "mysql", "-url", "localhost:3306/sakila", "-u",
                "root", "-p", "pass", "-load", "full", "sakila", "-print", "schema.trigger.customer_create_date"}},
            {new String[] {"-connect", "-db", "mysql", "-url", "localhost:3306/sakila", "-u",
                "root", "-p", "pass", "-load", "full", "sakila", "-print", "schema.foreign_key.fk_customer_address"}},
            {new String[] {"-connect", "-db", "mysql", "-url", "localhost:3306/sakila", "-u",
                "root", "-p", "pass", "-load", "full", "sakila", "-print", "schema.view.actor_info"}},
            {new String[] {"-connect", "-db", "mysql", "-url", "localhost:3306/sakila", "-u",
                "root", "-p", "pass", "-load", "full", "sakila", "-print", "schema.function.get_customer_balance"}},
            {new String[] {"-connect", "-db", "mysql", "-url", "localhost:3306/sakila", "-u",
                "root", "-p", "pass", "-load", "full", "sakila", "-print", "schema.procedure.film_in_stock"}},
        });
    }
}
