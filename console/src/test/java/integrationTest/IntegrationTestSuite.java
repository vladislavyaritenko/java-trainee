package integrationTest;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({FileCompareTest.class, IntegrationTest.class})
public class IntegrationTestSuite {
}
