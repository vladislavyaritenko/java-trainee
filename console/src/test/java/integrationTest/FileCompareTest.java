package integrationTest;

import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

public class FileCompareTest {
    @Test
    public void fileTest() throws IOException {
        List<String> strings1 = Files.readAllLines(Paths.get("src/test/resources/referenceInput.xml"), StandardCharsets.UTF_8);
        List<String> strings2 = Files.readAllLines(Paths.get("src/test/resources/referenceOutput.xml"), StandardCharsets.UTF_8);

        int size = strings1.size() > strings2.size() ? strings1.size() : strings2.size();

        for (int i = 1; i < size; i++) {
            Assert.assertEquals(strings2.get(i), strings1.get(i));
        }
    }
}
