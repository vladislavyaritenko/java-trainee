package integrationTest;

import console.Main;
import core.exceptions.ArgumentException;
import core.exceptions.ParserException;
import core.exceptions.ReflectionException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.List;

@RunWith(Parameterized.class)
public class IntegrationTest {
    private final String[] param;

    public IntegrationTest(String[] param) {
        this.param = param;
    }

    @Test
    public void main() throws ArgumentException, ParserException, ReflectionException {
        Main.main(param);
    }


    @Parameterized.Parameters
    public static List<Object> input() {
        return Arrays.asList(new Object[][] {
            {new String[] {"-input", "src/test/resources/referenceInput.xml"}},
            {new String[] {"-output", "src/test/resources/referenceOutput.xml"}},
        });
    }
}
