package unitTest.command;

import console.command.Receiver;
import console.command.impl.Input;
import console.command.impl.Output;
import console.command.impl.Search;
import console.context.Context;
import core.constants.TypeFileConst;
import core.exceptions.ParserException;
import core.structure.Tag;
import org.junit.After;
import org.junit.Assert;
import org.junit.Test;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.StringJoiner;

import static org.hamcrest.core.Is.is;

public class CommandTest {
    private static final String PATH_TO_INPUT_VALID_FILE = "src/test/resources/testInputValidFile.xml";
    private static final String PATH_TO_INPUT_INVALID_FILE = "src/test/resources/testInputInvalidFile.xml";
    private static final String PATH_TO_VALID_OUTPUT_FILE = "src/test/resources/testOutputFile.xml";
    private static final String PATH_TO_INVALID_OUTPUT_FILE = "src/test/resources//*testInvalidOutputFile.xml";
    private static final TypeFileConst VALID_TYPE_FILE = TypeFileConst.XML;
    private static final TypeFileConst INVALID_TYPE_FILE = TypeFileConst.TXT;
    private static final String EOL = System.lineSeparator();

    @After
    public void setTagInNull() {
        Context.getInstance().setTag(null);
    }

    @Test
    public void shouldExecuteInputCommandAndSetPriority() throws ParserException {
        Input input = new Input(PATH_TO_INPUT_VALID_FILE, 1);
        input.execute();

        int actualPriority = input.getPriority();

        Tag tag = Context.getInstance().getTag();

        Assert.assertThat(actualPriority, is(1));
        Assert.assertNotNull(tag);
    }

    @Test(expected = ParserException.class)
    public void shouldExecuteInputCommandByInvalidFileAndThrowParserException() throws ParserException {
        Input input = new Input(PATH_TO_INPUT_INVALID_FILE, 1);
        input.execute();
    }

    @Test
    public void shouldExecuteInputAndThenOutputCommandAndSetPriority() throws ParserException {
        Input input = new Input(PATH_TO_INPUT_VALID_FILE, 1);
        input.execute();

        int actualInputPriority = input.getPriority();

        Tag tag = Context.getInstance().getTag();

        Assert.assertThat(actualInputPriority, is(1));
        Assert.assertNotNull(tag);

        Output output = new Output(PATH_TO_VALID_OUTPUT_FILE, VALID_TYPE_FILE, 2);
        output.execute();

        int actualOutputPriority = output.getPriority();

        Assert.assertThat(actualOutputPriority, is(2));
        Assert.assertTrue(Files.exists(Paths.get(PATH_TO_VALID_OUTPUT_FILE)));
    }

    @Test(expected = ParserException.class)
    public void shouldExecuteOutputCommandAndThrowParserException() throws ParserException {
        Output output = new Output(PATH_TO_VALID_OUTPUT_FILE, VALID_TYPE_FILE, 2);
        output.execute();
    }

    @Test(expected = ParserException.class)
    public void shouldExecuteInputByValidFileAndThenOutputCommandByInvalidTypeFileAndThrowParserException() throws ParserException {
        Input input = new Input(PATH_TO_INPUT_VALID_FILE, 1);
        input.execute();

        Output output = new Output(PATH_TO_VALID_OUTPUT_FILE, INVALID_TYPE_FILE, 2);
        output.execute();
    }

    @Test(expected = ParserException.class)
    public void shouldExecuteInputByValidFileAndThenOutputCommandByInvalidFileAndThrowParserException() throws ParserException {
        Input input = new Input(PATH_TO_INPUT_VALID_FILE, 1);
        input.execute();

        Output output = new Output(PATH_TO_INVALID_OUTPUT_FILE, VALID_TYPE_FILE, 2);
        output.execute();
    }

    @Test
    public void shouldExecuteInputAndThenSearchCommandByDepthAndSetPriority() throws ParserException {
        Input input = new Input(PATH_TO_INPUT_VALID_FILE, 1);
        input.execute();

        int actualInputPriority = input.getPriority();

        Tag tag = Context.getInstance().getTag();

        Assert.assertThat(actualInputPriority, is(1));
        Assert.assertNotNull(tag);

        List<String> paramSearch = new ArrayList<>();
        paramSearch.add("type");
        paramSearch.add("newspaper");

        String typeSearch = "ds";

        Search search = new Search(paramSearch, typeSearch, 2);
        search.execute();

        int actualSearchPriority = search.getPriority();

        List<Tag> resultsSearch = search.getResultsSearch();
        String actualSearch = Arrays.toString(resultsSearch.toArray());

        StringJoiner joiner = new StringJoiner(EOL);
        joiner.add("[<paper type=\"newspaper\">").add("<titel>Simon > 5</titel>")
            .add("<monthly>true</monthly>").add("<color>false</color>").add("<size>15</size></paper>]");
        String expectedSearch = joiner.toString();

        Assert.assertThat(actualSearchPriority, is(2));
        Assert.assertEquals(expectedSearch, actualSearch);
    }

    @Test
    public void shouldExecuteInputAndThenSearchCommandByBreadthAndSetPriority() throws ParserException {
        Input input = new Input(PATH_TO_INPUT_VALID_FILE, 1);
        input.execute();

        int actualInputPriority = input.getPriority();

        Tag tag = Context.getInstance().getTag();

        Assert.assertThat(actualInputPriority, is(1));
        Assert.assertNotNull(tag);

        List<String> paramSearch = new ArrayList<>();
        paramSearch.add("color");

        String typeSearch = "bs";

        Search search = new Search(paramSearch, typeSearch, 2);
        search.execute();

        int actualSearchPriority = search.getPriority();

        List<Tag> resultsSearch = search.getResultsSearch();
        String actualSearch = Arrays.toString(resultsSearch.toArray());
        String expectedSearch = "[<color>false</color>]";

        Assert.assertThat(actualSearchPriority, is(2));
        Assert.assertEquals(expectedSearch, actualSearch);
    }

    @Test(expected = ParserException.class)
    public void shouldExecuteSearchCommandAndThrowParserException() throws ParserException {
        List<String> paramSearch = new ArrayList<>();
        paramSearch.add("titel");

        String typeSearch = "bs";

        Search search = new Search(paramSearch, typeSearch, 2);
        search.execute();
    }


    @Test
    public void shouldAddCommandToQueue() {
        Receiver receiver = new Receiver();

        Input input = new Input(PATH_TO_INPUT_VALID_FILE, 1);
        Output output = new Output(PATH_TO_VALID_OUTPUT_FILE, VALID_TYPE_FILE, 2);

        List<String> paramSearch = new ArrayList<>();
        paramSearch.add("type");
        paramSearch.add("newspaper");

        String typeSearch = "ds";

        Search search = new Search(paramSearch, typeSearch, 2);

        receiver.add(input);
        receiver.add(output);
        receiver.add(search);

        int sizeQueue = receiver.getCommands().size();

        Assert.assertThat(sizeQueue, is(3));
    }

    @Test
    public void shouldRunAllCommandsThatInQueue() throws ParserException {
        Receiver receiver = new Receiver();

        Input input = new Input(PATH_TO_INPUT_VALID_FILE, 1);
        Output output = new Output(PATH_TO_VALID_OUTPUT_FILE, VALID_TYPE_FILE, 2);

        List<String> paramSearch = new ArrayList<>();
        paramSearch.add("type");
        paramSearch.add("newspaper");

        String typeSearch = "ds";

        Search search = new Search(paramSearch, typeSearch, 2);

        receiver.add(input);
        receiver.add(output);
        receiver.add(search);

        receiver.run();
    }

    @Test(expected = ParserException.class)
    public void shouldAddComandsWithWrongPrioritiesThenRunThemAndThrowParserException() throws ParserException {
        Receiver receiver = new Receiver();

        Input input = new Input(PATH_TO_INPUT_VALID_FILE, 2);
        Output output = new Output(PATH_TO_VALID_OUTPUT_FILE, VALID_TYPE_FILE, 1);

        List<String> paramSearch = new ArrayList<>();
        paramSearch.add("type");
        paramSearch.add("newspaper");

        String typeSearch = "ds";

        Search search = new Search(paramSearch, typeSearch, 3);

        receiver.add(input);
        receiver.add(output);
        receiver.add(search);

        receiver.run();
    }
}
