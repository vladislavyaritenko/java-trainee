package unitTest;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import unitTest.command.CommandTest;

@RunWith(Suite.class)
@Suite.SuiteClasses({CommandTest.class, ArgumentManagerTest.class, MainTest.class})
public class ConsoleTestSuite {
}
