package unitTest;

import console.Main;
import org.junit.Assert;
import org.junit.Test;

import java.util.StringJoiner;

public class MainTest {
    private static final String EOL = System.lineSeparator();

    @Test
    public void shouldGetStringRepresentationCommandInfo() {

        StringJoiner joiner = new StringJoiner(EOL);
        joiner.add("EXAMPLES OF COMMANDS:");
        joiner.add("Task 1:").add("-input \"fileName.xml\"").add("-output \"fileName.xml\"");
        joiner.add("-search \"nameNode\" -ds").add("-search \"nameTag\" -bs").add("-search \" \"nameAttribute\" \"value\" \" -ds");
        joiner.add("-search \" \"nameAttribute\" \"value\" \" -bs").add("-bs or -ds is Breadth or Depth search.");
        joiner.add("");
        joiner.add("Task 2:").add("-connect").add("-db").add("\"typeDb\"").add("-url").add("\"URL\"").add("-u").add("\"userName\"");
        joiner.add("-p").add("\"password\"").add("-load").add("detail | lazy | full").add("\"sakila").add("").add("sakila.tables").add("sakila.tables.customer");
        joiner.add("sakila.tables.customer.indexes").add("sakila.tables.customer.indexes.idx_fk_address_id").add("sakila.tables.customer.triggers");
        joiner.add("sakila.tables.customer.triggers.customer_create_date").add("sakila.tables.customer.foreign_keys");
        joiner.add("sakila.tables.customer.foreign_keys.fk_customer_address").add("").add("sakila.views").add("sakila.views.actor_info").add("");
        joiner.add("sakila.procedures").add("sakila.procedures.film_in_stock").add("").add("sakila.functions").add("sakila.functions.get_customer_balance\"").add("");
        joiner.add("-print").add("\"schema").add("").add("schema.table.customer").add("schema.index.idx_fk_city_id").add("schema.trigger.customer_create_date");
        joiner.add("schema.foreign_key.fk_customer_address").add("").add("schema.view.actor_info").add("").add("schema.procedure.film_in_stock").add("");
        joiner.add("schema.function.get_customer_balance\"");

        String expectedInfo = joiner.toString();
        String actualInfo = Main.getCommandInfo();

        Assert.assertEquals(expectedInfo, actualInfo);
    }
}
