package unitTest;

import console.ArgumentManager;
import console.command.Command;
import console.command.Receiver;
import core.exceptions.ArgumentException;
import core.exceptions.ParserException;
import core.exceptions.ReflectionException;
import org.junit.Assert;
import org.junit.Test;

import java.util.Queue;

import static console.ArgumentManager.getArgsSearch;
import static org.hamcrest.CoreMatchers.is;

public class ArgumentManagerTest {
    private static final String PATH_TO_INPUT_VALID_FILE = "src/test/resources/testInputValidFile.xml";

    @Test
    public void shouldGetCorrectArgumentSearch() throws ArgumentException {
        String[] ARGS = {"-search", "type", "newspaper", "-bs"};
        String[] ARGS2 = {"-search", "type", "magazine", "-ds"};
        String[] ARGS3 = {"-input", "src/test/resources/testInputValidFile.xml"};
        String[] ARGS4 = {"-search", "titel", "-ds"};

        String[] expectedString = {"-search", "type newspaper", "-bs"};
        String[] expectedString2 = {"-search", "type magazine", "-ds"};
        String[] expectedString3 = {"-input", "src/test/resources/testInputValidFile.xml"};
        String[] expectedString4 = {"-search", "titel", "-ds"};

        String[] actualString = getArgsSearch(ARGS);
        String[] actualString2 = getArgsSearch(ARGS2);
        String[] actualString3 = getArgsSearch(ARGS3);
        String[] actualString4 = getArgsSearch(ARGS4);

        Assert.assertArrayEquals(expectedString, actualString);
        Assert.assertArrayEquals(expectedString2, actualString2);
        Assert.assertArrayEquals(expectedString3, actualString3);
        Assert.assertArrayEquals(expectedString4, actualString4);
    }

    @Test(expected = ArgumentException.class)
    public void shouldGetArgumentSearchThenThrowArgumentException() throws ArgumentException {
        String[] ARGS = {"-search", "type", "newspaper", "-as"};
        getArgsSearch(ARGS);
    }

    @Test
    public void shouldParseArguments() throws ArgumentException, ReflectionException {
        String[] argsInput = {"-input", "referenceInput.xml"};
        String[] argsOutput = {"-output", "referenceOutput.xml"};
        String[] argsSearch = {"-search", "type", "magazine", "-ds"};

        ArgumentManager manager = new ArgumentManager(argsInput);
        manager.parseArgs();

        Receiver receiver = manager.getReceiver();
        Queue<Command> commands = receiver.getCommands();
        int sizeQueue = commands.size();

        Assert.assertNotNull(commands);
        Assert.assertThat(sizeQueue, is(1));

        manager = new ArgumentManager(argsOutput);
        manager.parseArgs();

        receiver = manager.getReceiver();
        commands = receiver.getCommands();
        sizeQueue = commands.size();

        Assert.assertNotNull(commands);
        Assert.assertThat(sizeQueue, is(1));

        manager = new ArgumentManager(argsSearch);
        manager.parseArgs();

        receiver = manager.getReceiver();
        commands = receiver.getCommands();
        sizeQueue = commands.size();

        Assert.assertNotNull(commands);
        Assert.assertThat(sizeQueue, is(1));
    }

    @Test
    public void shouldParseArgumentsThenThrowArgumentExceptionDueToIncorrectNameCommand() throws ArgumentException, ReflectionException {
        String[] argsInput = {"-input2", "referenceInput.xml"};

        ArgumentManager manager = new ArgumentManager(argsInput);
        manager.parseArgs();
        int sizeQueue = manager.getReceiver().getCommands().size();

        Assert.assertThat(sizeQueue, is(0));
    }

    @Test(expected = ArgumentException.class)
    public void shouldParseArgumentsThenThrowArgumentExceptionDueToIncorrectNameFile() throws ArgumentException, ReflectionException {
        String[] argsInput = {"-input", "input.xmldddd"};

        ArgumentManager manager = new ArgumentManager(argsInput);
        manager.parseArgs();
    }

    @Test(expected = ArgumentException.class)
    public void shouldParseArgumentsThenThrowArgumentExceptionDueToIncorrectOutputNameFile() throws ArgumentException, ReflectionException {
        String[] argsInput = {"-output", "output.xmldddd"};

        ArgumentManager manager = new ArgumentManager(argsInput);
        manager.parseArgs();
    }

    @Test
    public void shouldExecuteAllCommand() throws ArgumentException, ParserException, ReflectionException {
        String f1 = "-input";
        String f2 = PATH_TO_INPUT_VALID_FILE;

        String[] tempArgs = new String[] {f1, f2};
        ArgumentManager manager = new ArgumentManager(tempArgs);
        manager.parseArgs();
        manager.executeAllCommands();
    }
}

