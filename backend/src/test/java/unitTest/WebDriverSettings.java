package unitTest;

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.Objects;

public class WebDriverSettings {
    protected ChromeDriver driver;
    private static final String CHROME_DRIVER_PATH= "/chromedriver/chromedriver.exe";

    @BeforeClass
    public static void setProperty(){
        System.setProperty("webdriver.chrome.driver", CHROME_DRIVER_PATH);
    }

    @Before
    public void setUp(){
        driver = new ChromeDriver();
        driver.get("http://localhost:3000/");
    }

    @After
    public void close(){
//        driver.quit();
    }
}
