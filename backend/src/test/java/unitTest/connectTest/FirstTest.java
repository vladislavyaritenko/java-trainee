package unitTest.connectTest;

import org.junit.Test;
import org.openqa.selenium.WebElement;
import unitTest.WebDriverSettings;

import java.util.List;

public class FirstTest extends WebDriverSettings {
    private static final String HOSTNAME = "localhost";
    private static final String PORT = "3306";
    private static final String USERNAME = "root";
    private static final String PASSWORD = "pass";
    private static final String DB_NAME = "sakila";
    private static final String TYPE_DB = "MySQL";

    @Test
    public void firstTest() {
        WebElement element;

        element = driver.findElementByName("Hostname:");
        element.sendKeys(HOSTNAME);

        element = driver.findElementByName("Port:");
        element.sendKeys(PORT);

        element = driver.findElementByName("Username:");
        element.sendKeys(USERNAME);

        element = driver.findElementByName("Password:");
        element.sendKeys(PASSWORD);

        element = driver.findElementByName("Database Name:");
        element.sendKeys(DB_NAME);

        WebElement dropDownListBox = driver.findElementByClassName("Dropdown-control");
        dropDownListBox.click();
        List<WebElement> dropDownListBoxValues = driver.findElementsByClassName("Dropdown-option");
        for (WebElement elementSelect : dropDownListBoxValues) {
            if (TYPE_DB.equals(elementSelect.getText())) {
                elementSelect.click();
                break;
            }
        }

        WebElement submitButton = driver.findElementById("connectButton");
        submitButton.click();
    }

}
