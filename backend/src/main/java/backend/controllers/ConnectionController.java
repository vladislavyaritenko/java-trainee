package backend.controllers;

import backend.services.ConnectionService;
import core.exceptions.DBException;
import core.exceptions.ReflectionException;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.WebApplicationContext;

/**
 * Controller for working with database connection.
 */
@RestController
@Scope(WebApplicationContext.SCOPE_REQUEST)
public class ConnectionController {
    private static final Logger LOGGER = LogManager.getLogger(ConnectionController.class);

    private ConnectionService service;

    /**
     * ConnectionController class constructor.
     *
     * @param service connection service class.
     */
    @Autowired
    public ConnectionController(ConnectionService service) {
        this.service = service;
    }

    /**
     * Method using for first connection to database.
     *
     * @param paramsForConnect parameters for connecting to the database.
     * @return root tag.
     * @throws DBException         when an error occurs in a core.db request.
     * @throws ReflectionException
     */
    @PostMapping(value = "connect")
    @ResponseStatus(HttpStatus.CREATED)
    public JSONObject initConnection(@RequestBody Map<String, String> paramsForConnect) throws DBException, ReflectionException {
        LOGGER.info("Start connection to database.");
        return service.initConnection(paramsForConnect);
    }

    /**
     * Method using for closing database connection.
     *
     * @throws DBException when an error occurs in a core.db request.
     */
    @PostMapping("disconnect")
    @ResponseStatus(HttpStatus.OK)
    public void destroyConnection() throws DBException {
         service.destroyConnection();
    }

    @PostMapping("isConnection")
    @ResponseStatus(HttpStatus.OK)
    public boolean isConnection(){
        return service.isConnection();
    }


}
