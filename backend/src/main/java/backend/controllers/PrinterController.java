package backend.controllers;

import backend.services.PrinterService;
import core.exceptions.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.WebApplicationContext;

/**
 * Controller for printing DDL (Data Definition Language).
 */
@RestController
@Scope(WebApplicationContext.SCOPE_REQUEST)
public class PrinterController {
    private PrinterService service;

    @Autowired
    public PrinterController(PrinterService service) {
        this.service = service;
    }

    /**
     * Method using for printing text that contains a sql query for creating an entity.
     *
     * @param printParams string of printing arguments.
     * @return text that contains a sql query for creating an entity.
     * @throws TagNotFoundException            when tag not found.
     * @throws PrinterNotFoundException        when printer is not found.
     * @throws PrinterManagerNotFoundException when PrinterManager is not found.
     * @throws ArgumentException               when parameters for printing is empty.
     * @throws PrintCannotException            when tag was loaded by detail or lazy load.
     */
    @GetMapping("/print/{printParams}")
    public String print(@PathVariable String printParams) throws TagNotFoundException, PrinterNotFoundException, PrinterManagerNotFoundException, ArgumentException, PrintCannotException {
        return service.print(printParams);
    }
}
