package backend.controllers;

import backend.services.LoadService;
import core.exceptions.ArgumentException;
import core.exceptions.DBException;
import core.exceptions.LoaderManagerNotFoundException;
import core.exceptions.TagNotFoundException;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.WebApplicationContext;

/**
 * Controller for loading database meta information.
 */
@RestController
@Scope(WebApplicationContext.SCOPE_REQUEST)
public class LoadController {

    private LoadService service;

    /**
     * LoadingController class constructor.
     *
     * @param service loading service class.
     */
    @Autowired
    public LoadController(LoadService service) {
        this.service = service;
    }

    /**
     * Method using for filling the empty tag by lazy loading way.
     *
     * @param loadParams string of load arguments.
     * @return filled tag.
     * @throws DBException                    when an error occurs in a core.db request.
     * @throws TagNotFoundException           when tag is not found.
     * @throws LoaderManagerNotFoundException when LoaderManager is not found.
     * @throws ArgumentException              when parameters for loading is empty.
     */
    @GetMapping("/lazy/{loadParams}")
    @ResponseStatus(HttpStatus.OK)
    public JSONObject lazyLoad(@PathVariable String loadParams) throws DBException, TagNotFoundException, LoaderManagerNotFoundException, ArgumentException {
        return service.lazyLoad(loadParams);
    }

    /**
     * Method using for filling the empty tag by lazy loading way.
     *
     * @param loadParams string of load arguments.
     * @return filled tag.
     * @throws DBException                    when an error occurs in a core.db request.
     * @throws TagNotFoundException           when tag is not found.
     * @throws LoaderManagerNotFoundException when LoaderManager is not found.
     * @throws ArgumentException              when parameters for loading is empty.
     */
    @GetMapping("/full/{loadParams}")
    @ResponseStatus(HttpStatus.OK)
    public JSONObject fullLoad(@PathVariable String loadParams) throws DBException, TagNotFoundException, LoaderManagerNotFoundException, ArgumentException {
        return service.fullLoad(loadParams);
    }

    /**
     * Method using for filling the empty tag by lazy loading way.
     *
     * @param loadParams string of load arguments.
     * @return filled tag.
     * @throws DBException                    when an error occurs in a core.db request.
     * @throws TagNotFoundException           when tag is not found.
     * @throws LoaderManagerNotFoundException when LoaderManager is not found.
     * @throws ArgumentException              when parameters for loading is empty.
     */
    @GetMapping("/detail/{loadParams}")
    @ResponseStatus(HttpStatus.OK)
    public JSONObject detailLoad(@PathVariable String loadParams) throws DBException, TagNotFoundException, LoaderManagerNotFoundException, ArgumentException {
        return service.detailLoad(loadParams);
    }
}
