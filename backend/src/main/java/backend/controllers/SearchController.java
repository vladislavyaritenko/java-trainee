package backend.controllers;

import backend.services.SearchService;
import core.exceptions.DBException;
import core.exceptions.SearchException;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.WebApplicationContext;

import java.util.Map;

/**
 * Controller for searching database meta information.
 */
@RestController
@Scope(WebApplicationContext.SCOPE_REQUEST)
public class
SearchController {
    private SearchService service;

    /**
     * SearchController class constructor.
     *
     * @param service loading service class.
     */
    @Autowired
    public SearchController(SearchService service) {
        this.service = service;
    }

    /**
     * Method using for searching tag.
     *
     * @param paramsForSearch
     * @return object of found tags.
     */
    @PostMapping("search")
    @ResponseStatus(HttpStatus.OK)
    public JSONObject search(@RequestBody Map<String, String> paramsForSearch) throws DBException, SearchException {
    return service.search(paramsForSearch);
    }
}
