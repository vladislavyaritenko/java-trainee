package backend.context;

import core.db.loaders.LoaderManager;
import core.db.printers.PrinterManager;
import core.structure.Tag;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.web.context.WebApplicationContext;

/**
 * Class using as an application context.
 */
@Component("context")
@Scope(WebApplicationContext.SCOPE_SESSION)
public final class ContextBackend {
    private Tag tag;
    private LoaderManager loaderManager;
    private PrinterManager printerManager;

    public Tag getTag() {
        return tag;
    }

    public void setTag(Tag tag) {
        this.tag = tag;
    }

    public LoaderManager getLoaderManager() {
        return loaderManager;
    }

    public void setLoaderManager(LoaderManager loaderManager) {
        this.loaderManager = loaderManager;
    }

    public PrinterManager getPrinterManager() {
        return printerManager;
    }

    public void setPrinterManager(PrinterManager printerManager) {
        this.printerManager = printerManager;
    }
}

