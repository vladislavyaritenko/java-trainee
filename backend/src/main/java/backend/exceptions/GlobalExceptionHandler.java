package backend.exceptions;

import core.exceptions.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
public class GlobalExceptionHandler {
    private static final Logger LOGGER = LogManager.getLogger(GlobalExceptionHandler.class);

    @ResponseStatus(value = HttpStatus.SERVICE_UNAVAILABLE, reason = "Error while working with database.")
    @ExceptionHandler(DBException.class)
    public void handleDBException(Exception e) {
        LOGGER.error("Error while working with database: " + e);
    }

    @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR, reason = "Tag is not found.")
    @ExceptionHandler(TagNotFoundException.class)
    public void handleTagNotFoundException(Exception e) {
        LOGGER.error("Tag is not found: " + e);
    }

    @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR, reason = "Reflection Exception.")
    @ExceptionHandler(ReflectionException.class)
    public void handleReflectionException(Exception e) {
        LOGGER.error("Reflection Exception: " + e);
    }

    @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR, reason = "Printer is not found.")
    @ExceptionHandler(PrinterNotFoundException.class)
    public void handleNoSuchPrinterException(Exception e) {
        LOGGER.error("Printer is not found: " + e);
    }

    @ResponseStatus(value = HttpStatus.SERVICE_UNAVAILABLE, reason = "LoaderManager is not found.")
    @ExceptionHandler(LoaderManagerNotFoundException.class)
    public void handleLoaderManagerNotFoundException(Exception e) {
        LOGGER.error("LoaderManager is not found: " + e);
    }

    @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR, reason = "PrinterManager is not found.")
    @ExceptionHandler(PrinterManagerNotFoundException.class)
    public void handlePrinterManagerNotFoundException(Exception e) {
        LOGGER.error("PrinterManager is not found: " + e);
    }

    @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR, reason = "Tag was loaded by detail or lazy load. Please, load the tag by full load!")
    @ExceptionHandler(PrintCannotException.class)
    public void handlePrintCannotException(Exception e) {
        LOGGER.error("Tag was loaded by detail or lazy load. Please, load the tag by full load! " + e);
    }

    @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR, reason = "Tag is absent!")
    @ExceptionHandler(SearchException.class)
    public void handleSearchException(Exception e) {
        LOGGER.error("Tag is absent! " + e);
    }
}
