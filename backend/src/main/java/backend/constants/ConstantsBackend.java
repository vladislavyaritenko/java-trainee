package backend.constants;

/**
 * Backend's constants.
 */
public final class ConstantsBackend {
    private ConstantsBackend() {
    }

    public static final String HOSTNAME = "hostname";
    public static final String PORT = "port";
    public static final String USERNAME = "username";
    public static final String PASSWORD = "password";
    public static final String DB_NAME = "dbName";
    public static final String DB_TYPE = "dbType";
}
