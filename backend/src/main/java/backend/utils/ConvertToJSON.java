package backend.utils;

import core.db.constants.CommonConst;
import core.structure.Tag;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.util.List;

/**
 * Convert Tag to JSON object.
 */
public final class ConvertToJSON {
    /**
     * Method using for converting tag to JSON.
     *
     * @param currentTag tag for convert.
     * @param url        list found url when searching. May be null when tag loading.
     * @return JSON structure.
     */
    public static JSONObject convertTreeToJson(Tag currentTag, List<String> url) {
        JSONObject jsonObject = null;
        String path = currentTag.getURL();

        if (containsAndRemoveIfTrue(url, path)) {
            jsonObject = new JSONObject();

            String name = currentTag.getName().equals(CommonConst.SCHEMA) ? currentTag.getAttributeByKey(CommonConst.DB_NAME) : currentTag.getAttributeByKey(CommonConst.NAME);
            name = currentTag.getName().equals(CommonConst.CATEGORY) ? name.substring(0, 1) + name.substring(1).toLowerCase() : name;

            jsonObject.put("label", name);
            jsonObject.put("value", path);
            jsonObject.put("showCheckbox", false);

            JSONObject attributes = new JSONObject();
            currentTag.getAttributes().forEach((key, value) -> attributes.put(key, value));
            jsonObject.put("attributes", attributes);

            JSONArray children = new JSONArray();
            currentTag.getChildrenList().stream().filter(child -> !child.getAttributeByKey(CommonConst.NAME).equals("")).forEach(child -> {
                JSONObject childObject = convertTreeToJson(child, url);
                if (childObject != null) {
                    children.add(childObject);
                }
            });
            jsonObject.put("children", children);
        }

        return jsonObject;
    }

    private static boolean containsAndRemoveIfTrue(List<String> urlList, String pathList) {
        if (urlList == null) {
            return true;
        }

        String[] pathListArray = pathList.split("\\.");

        for (String url : urlList) {
            String[] urlArray = url.split("\\.");
            if (urlArray.length >= pathListArray.length) {
                for (int i = 0; i < pathListArray.length; i++) {
                    if (!urlArray[i].equals(pathListArray[i])) {
                        return false;
                    }
                }

                urlList.remove(pathList);
                return true;
            }
        }
        return false;
    }
}
