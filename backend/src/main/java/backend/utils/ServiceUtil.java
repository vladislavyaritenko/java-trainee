package backend.utils;

import core.db.constants.CommonConst;
import core.exceptions.DBException;
import core.exceptions.TagNotFoundException;
import core.structure.Tag;

import java.util.List;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.web.context.WebApplicationContext;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.Deque;
import java.util.LinkedList;
import java.util.Queue;
import java.util.function.Predicate;

/**
 * Utility class.
 */
@Component
@Scope(WebApplicationContext.SCOPE_REQUEST)
public final class ServiceUtil {
    /**
     * Method using for finding tag.
     *
     * @param tagPredicate search predicate.
     * @param tag          current tag.
     * @return found tag.
     * @throws TagNotFoundException if tag is not found.
     */
    public Tag findTag(Predicate<Tag> tagPredicate, Tag tag, Deque<String> paramsQueue) throws TagNotFoundException {
        Queue<Tag> queue = new LinkedList<>();
        queue.add(tag);

        while (!queue.isEmpty()) {
            Tag temp = queue.remove();
            if (tagPredicate.test(temp) && paramsQueue.size() == 0) {
                return temp;
            }

            String name = temp.getAttributeByKey(CommonConst.NAME) != null ? temp.getAttributeByKey(CommonConst.NAME) : temp.getAttributeByKey(CommonConst.DB_NAME);

            if ((!paramsQueue.isEmpty()) && (paramsQueue.getFirst().equals(name.toLowerCase()))) {
                queue = new LinkedList<>(temp.getChildrenList());
                paramsQueue.removeFirst();
            }
        }

        throw new TagNotFoundException("Tag is not found");
    }

    /**
     * Method using for selecting of search predicate.
     *
     * @param name name of loading entity.
     * @return search predicate.
     */
    public Predicate<Tag> selectPredicate(String name) {
        return tag1 -> {
            String nameTag = tag1.getAttributeByKey(CommonConst.NAME) != null ? tag1.getAttributeByKey(CommonConst.NAME) : tag1.getAttributeByKey(CommonConst.DB_NAME);
            return name.equals(nameTag.toLowerCase());
        };
    }

    /**
     * Method using for parsing arguments.
     *
     * @param params string of arguments.
     * @return queue from a sequence of parameters to the required entity.
     */
    public Deque<String> getQueueOfParameters(String params) {
        return new LinkedList<>(Arrays.asList(params.split("\\.")));
    }
}
