package backend.services;

import backend.context.ContextBackend;
import backend.utils.ServiceUtil;
import core.db.printers.Printer;
import core.db.printers.PrinterManager;
import core.exceptions.*;
import core.structure.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import org.springframework.web.context.WebApplicationContext;

import javax.validation.constraints.NotNull;
import java.util.Deque;

/**
 * Printer service class.
 */
@Service
@Scope(WebApplicationContext.SCOPE_REQUEST)
public class PrinterService {
    private ContextBackend context;
    private ServiceUtil serviceUtil;

    @Autowired
    public PrinterService(ContextBackend context, ServiceUtil serviceUtil) {
        this.context = context;
        this.serviceUtil = serviceUtil;
    }

    /**
     * Method using for printing text that contains a sql query for creating an entity.
     *
     * @param printParams string of printing arguments.
     * @return text that contains a sql query for creating an entity.
     * @throws TagNotFoundException            when tag not found.
     * @throws PrinterNotFoundException        when printer is not found.
     * @throws PrinterManagerNotFoundException when PrinterManager is not found.
     * @throws ArgumentException               when parameters for printing is empty.
     * @throws PrintCannotException            when tag was loaded by detail or lazy load.
     */
    public String print(@NotNull String printParams) throws TagNotFoundException, PrinterNotFoundException, PrinterManagerNotFoundException, ArgumentException, PrintCannotException {
        if (printParams.isEmpty()) {
            throw new ArgumentException("Parameters for printing is empty.");
        }

        Deque<String> printParamsQueue = serviceUtil.getQueueOfParameters(printParams);
        PrinterManager printerManager = context.getPrinterManager();

        if (printerManager == null) {
            throw new PrinterManagerNotFoundException("PrinterManager is not found.");
        }

        Tag tag = context.getTag();
        String name = printParamsQueue.removeLast();
        Tag foundTag = serviceUtil.findTag(serviceUtil.selectPredicate(name), tag, printParamsQueue);

        Printer printer = printerManager.getPrintersMap().get(foundTag.getName());

        if (printer == null) {
            throw new PrinterNotFoundException("No such printer exception: " + foundTag.getName());
        }

        return printer.print(foundTag);
    }
}
