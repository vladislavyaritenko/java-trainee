package backend.services;

import backend.context.ContextBackend;
import backend.utils.ConvertToJSON;
import backend.utils.ServiceUtil;
import core.db.loaders.LoaderManager;
import core.exceptions.ArgumentException;
import core.exceptions.DBException;
import core.exceptions.LoaderManagerNotFoundException;
import core.exceptions.TagNotFoundException;
import core.structure.Tag;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import org.springframework.web.context.WebApplicationContext;

import javax.validation.constraints.NotNull;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Deque;

/**
 * Loading service class.
 */
@Service
@Scope(WebApplicationContext.SCOPE_REQUEST)
public class LoadService {
    private ContextBackend context;
    private ServiceUtil serviceUtil;

    /**
     * LoadingService class constructor.
     *
     * @param context bean context class.
     */
    @Autowired
    public LoadService(ContextBackend context, ServiceUtil serviceUtil) {
        this.context = context;
        this.serviceUtil = serviceUtil;
    }

    /**
     * Method using for filling the empty tag by lazy loading way.
     *
     * @param loadParams string of load arguments.
     * @return filled tag.
     * @throws DBException                    when an error occurs in a core.db request.
     * @throws TagNotFoundException           when tag is not found.
     * @throws LoaderManagerNotFoundException when LoaderManager is not found.
     * @throws ArgumentException              when parameters for loading is empty.
     */
    public JSONObject lazyLoad(@NotNull String loadParams) throws TagNotFoundException, DBException, LoaderManagerNotFoundException, ArgumentException {
        if (loadParams.isEmpty()) {
            throw new ArgumentException("Parameters for loading is empty.");
        }

        Deque<String> loadParamsQueue = serviceUtil.getQueueOfParameters(loadParams);
        LoaderManager loaderManager = context.getLoaderManager();

        if (loaderManager == null) {
            throw new LoaderManagerNotFoundException("LoaderManager is not found.");
        }

        if (isCloseConnection(loaderManager.getConnection())) {
            throw new DBException("You haven't got connection to database!");
        }

        Tag tag = context.getTag();
        String name = loadParamsQueue.removeLast();
        Tag foundTag = serviceUtil.findTag(serviceUtil.selectPredicate(name), tag, loadParamsQueue);

        context.getLoaderManager().lazyLoad(foundTag);

        context.setTag(tag);

        return ConvertToJSON.convertTreeToJson(context.getTag(), null);
    }

    /**
     * Method using for filling the empty tag by full loading way.
     *
     * @param loadParams string of load arguments.
     * @return filled tag.
     * @throws DBException                    when an error occurs in a core.db request.
     * @throws TagNotFoundException           when tag not found.
     * @throws LoaderManagerNotFoundException when LoaderManager is not found.
     * @throws ArgumentException              when parameters for loading is empty.
     */
    public JSONObject fullLoad(@NotNull String loadParams) throws TagNotFoundException, DBException, LoaderManagerNotFoundException, ArgumentException {
        if (loadParams.isEmpty()) {
            throw new ArgumentException("Parameters for loading is empty.");
        }

        Deque<String> loadParamsQueue = serviceUtil.getQueueOfParameters(loadParams);
        LoaderManager loaderManager = context.getLoaderManager();

        if (loaderManager == null) {
            throw new LoaderManagerNotFoundException("LoaderManager is not found.");
        }

        if (isCloseConnection(loaderManager.getConnection())) {
            throw new DBException("You haven't got connection to database!");
        }

        Tag tag = context.getTag();
        String name = loadParamsQueue.removeLast();
        Tag foundTag = serviceUtil.findTag(serviceUtil.selectPredicate(name), tag, loadParamsQueue);

        context.getLoaderManager().fullLoad(foundTag);

        context.setTag(tag);
        return ConvertToJSON.convertTreeToJson(context.getTag(), null);
    }

    /**
     * Method using for filling the empty tag by detail loading way.
     *
     * @param loadParams string of load arguments.
     * @return filled tag.
     * @throws DBException                    when an error occurs in a core.db request.
     * @throws TagNotFoundException           when tag not found.
     * @throws LoaderManagerNotFoundException when LoaderManager is not found.
     * @throws ArgumentException              when parameters for loading is empty.
     */
    public JSONObject detailLoad(@NotNull String loadParams) throws TagNotFoundException, DBException, LoaderManagerNotFoundException, ArgumentException {
        if (loadParams.isEmpty()) {
            throw new ArgumentException("Parameters for loading is empty.");
        }

        Deque<String> loadParamsQueue = serviceUtil.getQueueOfParameters(loadParams);
        LoaderManager loaderManager = context.getLoaderManager();

        if (loaderManager == null) {
            throw new LoaderManagerNotFoundException("LoaderManager is not found.");
        }

        if (isCloseConnection(loaderManager.getConnection())) {
            throw new DBException("You haven't got connection to database!");
        }

        Tag tag = context.getTag();
        String name = loadParamsQueue.removeLast();
        Tag foundTag = serviceUtil.findTag(serviceUtil.selectPredicate(name), tag, loadParamsQueue);

        context.getLoaderManager().detailLoad(foundTag);

        context.setTag(tag);
        return ConvertToJSON.convertTreeToJson(context.getTag(), null);
    }

    private boolean isCloseConnection(Connection connection) throws DBException {
        try {
            if ((connection == null) || (connection.isClosed())) {
                return true;
            }
            return false;
        } catch (SQLException e) {
            throw new DBException(e);
        }
    }
}
