package backend.services;

import backend.constants.ConstantsBackend;
import backend.context.ContextBackend;
import backend.utils.ConvertToJSON;
import backend.utils.ServiceUtil;
import core.db.connection.ConnectionManager;
import core.db.constants.CommonConst;
import core.db.constants.DBType;
import core.db.loaders.LoaderManager;
import core.db.printers.PrinterManager;
import core.exceptions.DBException;
import core.exceptions.ReflectionException;
import core.structure.Tag;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import org.springframework.web.context.WebApplicationContext;

/**
 * Connection service class.
 */
@Service
@Scope(WebApplicationContext.SCOPE_REQUEST)
public class ConnectionService {
    private static final Logger LOGGER = LogManager.getLogger(ConnectionService.class);

    private ContextBackend context;
    private ServiceUtil serviceUtil;

    /**
     * TagService class constructor.
     *
     * @param context bean context class.
     * @param serviceUtil   Utils class.
     */
    @Autowired
    public ConnectionService(ContextBackend context, ServiceUtil serviceUtil) {
        this.context = context;
        this.serviceUtil = serviceUtil;
    }

    /**
     * Method using for first connection to database.
     *
     * @param paramsForConnect parameters for connecting to the database.
     * @return root tag.
     * @throws DBException         when an error occurs in a core.db request.
     * @throws ReflectionException
     */
    public JSONObject initConnection(Map<String, String> paramsForConnect) throws DBException, ReflectionException {
        String hostname = paramsForConnect.get(ConstantsBackend.HOSTNAME);
        int port = Integer.parseInt(paramsForConnect.get(ConstantsBackend.PORT));
        String username = paramsForConnect.get(ConstantsBackend.USERNAME);
        String password = paramsForConnect.get(ConstantsBackend.PASSWORD);
        String dbName = paramsForConnect.get(ConstantsBackend.DB_NAME);
        DBType dbType = DBType.getDBType(paramsForConnect.get(ConstantsBackend.DB_TYPE));

        String url = getUrl(hostname, port, dbName);

        execute(dbType, dbName, url, username, password);

        return ConvertToJSON.convertTreeToJson(context.getTag(), null);
    }

    /**
     * Method using for closing database connection.
     *
     * @throws DBException when an error occurs in a core.db request.
     */
    public void destroyConnection() throws DBException {
        LoaderManager loaderManager = context.getLoaderManager();

        if (loaderManager != null) {
            Connection connection = close(loaderManager.getConnection());

            loaderManager.setConnection(connection);
            context.setLoaderManager(loaderManager);
        }
    }

    private void execute(DBType dbType, String dbName, String url, String user, String password) throws DBException, ReflectionException {
        LOGGER.info("Command connectToDB starting...");

        Connection connection = new ConnectionManager().initConnection(dbType, url, user, password);

        Tag tag = new Tag(CommonConst.SCHEMA);
        tag.setAttribute(CommonConst.DB_NAME, dbName);
        context.setTag(tag);

        context.setLoaderManager(new LoaderManager(connection, dbType));
        context.setPrinterManager(new PrinterManager(dbType));

        LOGGER.info("LoaderManager and PrinterManager are created...");
    }

    public boolean isConnection() {
        LoaderManager loaderManager = context.getLoaderManager();
        return loaderManager != null;
    }

    private String getUrl(String hostname, int port, String dbName) {
        return hostname + port + "/" + dbName;
    }

    private Connection close(Connection connection) throws DBException {
        try {
            if (connection != null) {
                connection.close();
            }
        } catch (SQLException e) {
            throw new DBException(e);
        }
        return connection;
    }

}
