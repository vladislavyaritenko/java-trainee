package backend.services;

import backend.context.ContextBackend;
import backend.utils.ConvertToJSON;
import backend.utils.ServiceUtil;
import core.exceptions.DBException;
import core.exceptions.SearchException;
import core.structure.Tag;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import org.springframework.web.context.WebApplicationContext;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;

/**
 * Search service class.
 */
@Service
@Scope(WebApplicationContext.SCOPE_REQUEST)
public class SearchService {
    private ContextBackend context;
    private ServiceUtil serviceUtil;

    /**
     * SearchService class constructor.
     *
     * @param context bean context class.
     * @param serviceUtil   Utils class.
     */
    @Autowired
    public SearchService(ContextBackend context, ServiceUtil serviceUtil) {
        this.context = context;
        this.serviceUtil = serviceUtil;
    }

    /**
     * Method using for searching tag.
     *
     * @param paramsForSearch
     * @return object of found tags.
     */
    public JSONObject search(Map<String, String> paramsForSearch) throws DBException, SearchException {
        Tag tag = context.getTag();

        if (tag == null) {
            throw new DBException("Tag is absent!");
        }

        String keyParam = paramsForSearch.get("key").toUpperCase();
        String valueParam = paramsForSearch.get("value").equals("null") ? "" : paramsForSearch.get("value").toLowerCase();

        Predicate<Tag> tagPredicate = selectSearchPredicate(keyParam, valueParam);

        List<Tag> foundTags = tag.breadthSearch(tagPredicate, tag, new ArrayList<>());

        List<String> urlList = new ArrayList<>();
        foundTags.forEach(searchTag -> urlList.add(searchTag.getURL()));

        if (urlList.size() > 0) {
            return ConvertToJSON.convertTreeToJson(context.getTag(), urlList);
        }
        throw new SearchException("Search is fail!");
    }

    private Predicate<Tag> selectSearchPredicate(String keyParam, String valueParam) {
        return tag1 -> {
            if (!tag1.getAttributes().isEmpty()) {
                String key = tag1.getAttributes().get(keyParam);
                return key != null && key.toLowerCase().equals(valueParam);
            }
            return false;
        };
    }
}


